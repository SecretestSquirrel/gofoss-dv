[![Netlify Status](https://api.netlify.com/api/v1/badges/d510ab06-d1d4-437b-a057-d53cc3388c2d/deploy-status)](https://app.netlify.com/sites/gofossnet/deploys)

<img src="https://img.shields.io/liberapay/goal/GeorgJerska.svg?logo=liberapay">

# GoFOSS

<img src="https://gofoss.net/assets/img/gofoss_favicon.png" alt="GoFOSS" width="150px"></img>

This is the source code of [https://gofoss.net](https://gofoss.net/), the ultimate, free and open source guide to online privacy, data ownership and durable tech.

The aim of this website is to make privacy respecting, free and open source software accessible to all.

## Learn how to...

* ... [safely browse the Internet](https://gofoss.net/intro-browse-freely)
* ... [keep your conversations private](https://gofoss.net/intro-speak-freely)
* ... [protect your data](https://gofoss.net/intro-store-safely)
* ... [unlock your computer's full potential](https://gofoss.net/intro-free-your-computer)
* ... [stay mobile and free](https://gofoss.net/intro-free-your-phone)
* ... [own your cloud](https://gofoss.net/intro-free-your-cloud)
* ... [avoid filter bubbles, surveillance & censorship](https://gofoss.net/nothing-to-hide)

## Why should you bother?

* Large tech firms harvest user data to [maximize their profits](https://en.wikipedia.org/wiki/Surveillance_capitalism/)
* Irrespective of fundamental rights, such as freedom & privacy
* This gives rise to mass surveillance & polarizes the political debate
* It also drives uniformity of thought & facilitates censorship
* Free and open source software can help addressing those issues

## How can you contribute?

* This is a volunteer-run & non-profit project
* It is truly free and open source
* No ads, no tracking, no sponsors, no affiliates, no paywall
* Help us to improve the content, add new features & translate
* Read the [contribution guidelines](https://gofoss.net/contributing/) to learn about all the ways to get involved
* Or click on the button below to help us funding operating & development costs

<center>

<a href="https://liberapay.com/GeorgJerska/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

</center>

## Screenshots

![GoFOSS screenshots](docs/assets/img/screenshot_gofoss.png)

## License

Released with ❤️under [aGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt).

Please also refer to the [privacy statement, legal disclaimer and license terms](https://gofoss.net/en/disclaimer/).