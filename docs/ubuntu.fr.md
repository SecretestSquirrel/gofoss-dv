---
template: main.html
title: Comment installer Ubuntu
description: Qu'est-ce qu'Ubuntu ? Ubuntu est meilleur que Windows ? Quelle est la signification d'Ubuntu ? Qu'est-ce qu'Ubuntu LTS ? Comment installer Ubuntu ?
---

# Ubuntu, une distribution Linux accessible

!!! level "Destiné aux débutants et utilisateurs expérimentés. Certaines compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/ubuntu_desktop.png" alt="Comment installer Ubuntu" width="600px"></img>
</div>


## Découvrez Ubuntu

La façon la plus simple de découvrir [Ubuntu](https://ubuntu.com/) est de l'installer aux côtés de votre système d'exploitation existant, par exemple en utilisant une clé USB « Live » ou un programme comme VirtualBox. S'il devait s'avérer qu'Ubuntu n'est pas à votre goût, vous pourrez ainsi facilement revenir à Windows ou macOS.

=== "USB ou DVD « Live »"

    Pour essayer Ubuntu rapidement, lancez-le à partir d'un DVD ou d'une clé USB « Live ». Plus de détails ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Téléchargez Ubuntu |Téléchargez la dernière [version d'Ubuntu bénéficiant d'un support à long terme](https://ubuntu.com/download/desktop) (en anglais, « long-term support ou LTS »). Au moment d'écrire ces lignes, la dernière version LTS était Ubuntu 20.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
        | Gravez un DVD ou préparez une clé USB |Utilisez un ordinateur [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) ou [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) pour graver le fichier `.iso` téléchargé sur un DVD. Vous pouvez également utiliser un ordinateur [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) ou [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) pour créer une clé USB « Live ». |
        | Redémarrez |Insérez le DVD ou la clé USB « Live » dans l'ordinateur sur lequel vous souhaitez tester Ubuntu, puis redémarrez-le. La plupart des ordinateurs démarrent alors automatiquement depuis le DVD ou la clé USB. Si ce n'est pas le cas, essayez d'appuyer plusieurs fois sur `F12`, `ESC`, `F2` ou `F10` lorsque l'ordinateur démarre. Cela devrait vous donner accès au menu de démarrage, où vous pourrez alors sélectionner le DVD ou le lecteur USB comme périphérique de démarrage. |
        | Testez Ubuntu |Lorsque l'écran de démarrage s'affiche, choisissez l'entrée `Essayer Ubuntu`. |

        </center>


=== "VirtualBox"

    [VirtualBox](https://www.virtualbox.org/) est une autre solution pour découvrir Ubuntu tout en conservant votre système d'exploitation existant. Ce programme crée des machines virtuelles qui se lancent sur votre ordinateur Windows ou macOS. Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        ### Installer VirtualBox

        === "Windows"

            Assurez-vous que votre ordinateur répond aux exigences minimales : Processeur double cœur de 2 GHz ou plus, 2 Go de mémoire vive ou plus, 25 Go d'espace disque disponible. Téléchargez et exécutez le dernier [paquet VirtualBox pour les hôtes Windows](https://www.virtualbox.org/wiki/Downloads). Ouvrez le fichier `.exe` téléchargé et suivez le guide d'installation.

        === "macOS"

            Assurez-vous que votre ordinateur répond aux exigences minimales : Processeur double cœur de 2 GHz ou plus, 2 Go de mémoire vive ou plus, 25 Go d'espace disque disponible. Téléchargez le [paquet VirtualBox pour les hôtes OS X](https://www.virtualbox.org/wiki/Downloads). Ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône VirtualBox vers le dossier d'application. Pour y accéder facilement, ouvrez le dossier d'applications et faites glisser l'icône VirtualBox vers le dock.


        ### Configurer VirtualBox

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Téléchargez Ubuntu | Téléchargez la dernière [version d'Ubuntu Desktop bénéficiant d'un support à long terme](https://ubuntu.com/download/desktop) (en anglais, « long-term support ou LTS »). Au moment d'écrire ces lignes, la dernière version LTS était Ubuntu 20.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
        | Créez une machine virtuelle | Lancez VirtualBox et cliquez sur le bouton `Nouvelle`. |
        | Nom et système d'exploitation | Donnez un nom à votre machine virtuelle (en anglais, « virtual machine ou VM »), par exemple `Ubuntu`. Sélectionnez également le système d'exploitation (`Linux`) et la version, par exemple `Ubuntu (64-bit)`. |
        | Taille de la mémoire | Choisissez combien de mémoire vive sera allouée à Ubuntu. 2 Go sont recommandés, 3-4 Go sont encore mieux. |
        | Disque dur |  Sélectionnez `Créer un disque dur virtuel maintenant` pour ajouter un disque dur virtuel. |
        | Type de fichier de disque dur | Choisissez le format `VDI` comme type de fichier. |
        | Stockage sur disque dur physique | Choisissez `Dynamiquement alloué` comme taille du disque dur virtuel. |
        | Emplacement du fichier et taille | Choisissez l'endroit où créer le disque dur virtuel. Décidez également combien d'espace disque doit être alloué à Ubuntu. 10 Go sont recommandés, plus est encore mieux. |
        | Sélecteur de disque optique |De retour sur l'écran principal, cliquez sur `Démarrer` pour lancer la machine virtuelle `Ubuntu`. Dans la fenêtre qui s'ouvre, cliquez sur l'icône pour choisir un fichier de disque optique virtuel. Cliquez sur `Ajouter` et cherchez le fichier `.iso' Ubuntu LTS téléchargé précédemment. Cliquez sur `Ouvrir`, `Choisir` et `Démarrer`. L'assistant d'installation d'Ubuntu apparaîtra après la phase de démarrage. |

        </center>


        ### Installer Ubuntu dans VirtualBox

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Bienvenue |Sélectionnez une langue et cliquez sur `Installer Ubuntu`. |
        |Disposition du clavier |Sélectionnez une disposition de clavier. |
        |Mises à jour et autres logiciels |Choisissez entre une installation `normale` ou `minimale`, en fonction du nombre d'applications que vous souhaitez installer dès le départ. En option, cochez la case `Télécharger les mises à jour pendant l'installation de Ubuntu` pour accélérer la configuration après l'installation, et la case `Installer des logiciels tiers` pour bénéficier de pilotes (propriétaires) pour les graphismes, le WiFi, les fichiers multimédias, etc. Cliquez ensuite sur `Continuer`. |
        |Type d'installation & chiffrage |Cet écran permet de choisir si l'on veut supprimer le système d'exploitation existant et le remplacer par Ubuntu, ou si l'on veut installer Ubuntu en parallèle du système d'exploitation existant (ce que l'on appelle le « dual boot »). <br><br> Comme nous installons Ubuntu dans VirtualBox, aucun autre système d'exploitation n'est présent. Choisissez simplement `Effacer le disque et installer Ubuntu`, cliquez sur `Fonctions avancées` et sélectionnez `Utiliser LVM pour la nouvelle installation de Ubuntu` ainsi que `Chiffrer la nouvelle installation de Ubuntu`. Cliquez ensuite sur `OK` et `Installer maintenant`. |
        |Choisir une clé de sécurité |Choisissez une [clé de sécurité forte et unique](https://gofoss.net/fr/passwords/). Elle sera nécessaire pour déchiffrer le disque à chaque démarrage de votre ordinateur. Cochez également la case `Écraser l'espace disque vide` pour plus de sécurité. Cliquez sur `Installer maintenant`. Confirmez la fenêtre de dialogue en cliquant sur `Continuer`. <br><br> *Attention*: Si vous perdez la clé de sécurité, toutes vos données seront perdues. Conservez-la en lieu sûr ! |
        |Où êtes-vous ? |Choisissez un fuseau horaire et un emplacement. Cliquez sur `Continuer`. |
        |Qui êtes-vous ? |Saisissez des informations de connexion, telles qu'un nom d'utilisateur et un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Sélectionnez `Demander mon mot de passe pour ouvrir une session`. Cliquez sur `Continuer`.|
        |Redémarrez et connectez-vous pour la première fois |Cliquez sur `Redémarrer maintenant` après l'installation. La machine virtuelle va redémarrer. Vous pouvez maintenant vous connecter à votre première session Ubuntu en saisissant la clé de sécurité et le mot de passe corrects. |
        |Découvrez Ubuntu| Parcourez la configuration initiale et commencez à explorer Ubuntu. Fermez la session une fois que vous avez terminé. A partir de maintenant, vous pouvez lancer Ubuntu en cliquant sur le bouton `Démarrer` sur l'écran principal de VirtualBox. |

        </center>


    ??? tip "Montrez-moi une vidéo récapitulative (5min)"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ef6ada2-116b-416d-8d53-10a817d95827" frameborder="0" allowfullscreen></iframe>
        </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Ubuntu desktop" width="150px"></img> </center>

## Le bureau d'Ubuntu

Une fois connecté à votre première session Ubuntu, vous verrez apparaître le [« GNOME Desktop »](https://www.gnome.org/) :

<center>

| Éléments du « Gnome Desktop » | Description |
| ------ | ------ |
| Barre supérieure | Affiche la date, l'heure, les notifications, l'autonomie de la batterie, les connexions réseau, le volume, etc. |
| Le « dock » | Situé à gauche, le « dock » affiche des raccourcis vers vos applications préférées et les applications actuellement ouvertes. L'emplacement peut être modifié. |
| Tiroir d'applications | Située en bas à gauche du « dock », ce tiroir donne accès à toutes les applications installées. |
| Écran d'aperçu | Accessible via le bouton `Activités` situé sur la barre supérieure, affiche toutes les fenêtres ouvertes. |
| Barre de recherche | Accessible via le bouton `Activités`, permet de rechercher des applications, des fichiers, des paramètres, des commandes, etc. |

</center>


<br>

<center> <img src="../../assets/img/separator_php.svg" alt="Le terminal Ubuntu" width="150px"></img> </center>

## Le terminal

Le terminal est une interface permettant d'exécuter des commandes en mode texte. De nombreux nouveaux utilisateurs sont rebutés par le terminal, qui est souvent associé à d'obscurs activités de piratage ou de codage. En réalité, l'utilisation du terminal n'est pas si compliquée et s'avère souvent beaucoup plus rapide que la navigation dans l'interface graphique. Ouvrez le terminal avec le raccourci clavier `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` situé sur la barre supérieure et cherchez `Terminal`.

??? info "Dites-m'en plus sur le terminal"

    Le terminal est parfois appelé shell, console, ligne de commande ou « prompt ». Il faut un certain temps pour s'y habituer, mais il s'avère être plutôt efficace quand on a compris quelques astuces. Par exemple, mettre `sudo` devant une commande signifie que celle-ci est exécutée avec des privilèges d'administrateur. Un peu comme les fenêtres contextuelles de sécurité de Windows et macOS, qui demandent un mot de passe administrateur avant d'installer un programme ou de modifier un paramètre. Voici quelques lectures supplémentaires pour devenir un ninja du terminal :

    * [Aide-mémoire avec les commandes de terminal les plus utilisées](https://valise.chapril.org/s/Log4SeZmGb9xtKb)
    * [Autre aide-mémoire avec les commandes de terminal les plus utilisées](https://valise.chapril.org/s/GqojwkXGrAHgESt)
    * [Encore un autre aide-mémoire avec les commandes de terminal les plus utilisées](https://valise.chapril.org/s/T3M6XbAMHPTXGWR)
    * [Quelques commandes utiles pour le terminal Ubuntu](https://valise.chapril.org/s/MHsRdpXznZSLFyg)
    * [Livre complet sur la ligne de commande Linux](https://valise.chapril.org/s/QXXNcMj874tJreY)


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Installation d'Ubuntu" width="150px"></img> </center>

## Installez Ubuntu

Vous commencez à apprécier Ubuntu ? Vous voulez l'installer définitivement sur votre ordinateur ? Génial ! Il vous suffit d'effectuer quelques vérifications préliminaires et de suivre les instructions d'installation ci-dessous.


??? tip "Montrez-moi la liste des contrôles préliminaires"

    <center>

    | Vérifications | Description |
    | ------ | ------ |
    | Mon ordinateur est-il compatible avec Linux ? |• [Testez Ubuntu](https://gofoss.net/fr/ubuntu/) en utilisant une clé USB « Live » ou VirtualBox <br>• Consultez la [base de données de compatibilité](https://ubuntu.com/certified) <br>• [Demandez](https://search.disroot.org/) sur Internet <br>• Achetez un [ordinateur compatible avec Linux](https://linuxpréinstallé.com/) |
    | Mon ordinateur répond-il aux exigences minimales ? |• Processeur double cœur 2 GHz <br>• 4 Go de mémoire vive (RAM) <br>• 25 Go d'espace de stockage libre (Ubuntu occupe environ 5 Go, prévoyez au moins 20 Go pour vos données) |
    | Mon ordinateur est-il branché ? | Si vous installez Ubuntu sur un appareil mobile tel qu'un ordinateur portable, assurez-vous qu'il est bien branché. |
    | Le support d'installation est-il accessible ? | Vérifiez si votre ordinateur dispose d'un lecteur de DVD ou d'un port USB libre. |
    | Mon ordinateur est-il connecté à l'Internet ? | Vérifiez si la connexion Internet est opérationnelle. |
    | Ai-je sauvegardé mes données ? | [Sauvegardez vos données](https://gofoss.net/fr/backups/), car il existe un risque (faible mais réel) de perte de données pendant le processus d'installation ! |
    | Ai-je téléchargé la dernière version d'Ubuntu ? | Téléchargez la dernière [version d'Ubuntu Desktop bénéficiant d'un support à long terme](https://ubuntu.com/download/desktop) (en anglais, « long-term support ou LTS »), qui est pris en charge pendant 5 ans, y compris les mises à jour de sécurité et de maintenance. Au moment d'écrire ces lignes, la dernière version LTS était Ubuntu 20.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
    | Ai-je préparé un périphérique de démarrage ? | Utilisez un ordinateur [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) ou [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) pour graver le fichier `.iso` téléchargé sur un DVD. Vous pouvez également utiliser un ordinateur [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) ou [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) pour créer une clé USB « Live ». |

    </center>


??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Instruction | Description |
    | ------ | ------ |
    |Démarrage |Insérez le DVD ou la clé USB « Live » dans l'ordinateur, puis redémarrez-le. La plupart des ordinateurs démarrent alors automatiquement depuis le DVD ou la clé USB. Si ce n'est pas le cas, essayez d'appuyer plusieurs fois sur `F12`, `ESC`, `F2` ou `F10` lorsque l'ordinateur démarre. Cela devrait vous donner accès au menu de démarrage, où vous pourrez alors sélectionner le DVD ou le lecteur USB comme périphérique de démarrage. |
    |Bienvenue |Sélectionnez une langue et cliquez sur `Installer Ubuntu`. |
    |Disposition du clavier |Sélectionnez une disposition de clavier. |
    |Mises à jour et autres logiciels |Choisissez entre une installation `normale` ou `minimale`, en fonction du nombre d'applications que vous souhaitez installer dès le départ. En option, cochez la case `Télécharger les mises à jour pendant l'installation de Ubuntu` pour accélérer la configuration après l'installation, et la case `Installer des logiciels tiers` pour bénéficier de pilotes (propriétaires) pour les graphismes, le WiFi, les fichiers multimédias, etc. Cliquez ensuite sur `Continuer`. |
    |Type d'installation & chiffrage |Choisissez si vous voulez : <br>• Supprimer le système d'exploitation existant et le remplacez par Ubuntu. *Attention!* : ceci effacera toutes les données de votre disque dur ! Assurez-vous d'avoir [sauvegardé vos données](https://gofoss.net/fr/backups/) ! <br>• Ou installer Ubuntu en parallèle du système d'exploitation existant (ce que l'on appelle le « dual boot »). Cela ne devrait avoir aucune incidence sur la configuration existante de votre ordinateur. Veillez tout de même à [sauvegarder vos données](https://gofoss.net/fr/backups/), on ne sait jamais... <br><br> Pour chiffrer Ubuntu, cliquez sur `Fonctions avancées` et sélectionnez : <br>• `Utiliser LVM pour la nouvelle installation de Ubuntu` <br>• `Chiffrer la nouvelle installation de Ubuntu` <br><br>Puis, cliquez sur `OK` et `Installer maintenant`. |
    |Choisir une clé de sécurité |Choisissez une [clé de sécurité forte et unique](https://gofoss.net/fr/passwords/). Elle sera nécessaire pour déchiffrer le disque à chaque démarrage de votre ordinateur. Puis, cliquez sur `Continuer`. <br><br> *Attention*: Si vous perdez la clé de sécurité, toutes vos données seront perdues. Conservez-la en lieu sûr ! |
    |Où êtes-vous ? |Choisissez un fuseau horaire et un emplacement. Cliquez sur `Continuer`. |
    |Qui êtes-vous ? |Saisissez des informations de connexion, telles qu'un nom d'utilisateur et un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Sélectionnez `Demander mon mot de passe pour ouvrir une session`. Cliquez sur `Continuer`.|
    |Redémarrez et connectez-vous pour la première fois |Et voilà. Attendez que l'installation se termine, retirez la clé USB et cliquez sur `Redémarrer maintenant` lorsque vous y êtes invité. Après le redémarrage, connectez-vous à Ubuntu avec votre clé de sécurité et votre mot de passe. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252dab-255f-42c7-885e-711d9a24da85" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Mises à jour du système Ubuntu" width="150px"></img> </center>

## Mises à jour système

Maintenez Ubuntu à jour et installez régulièrement les derniers correctifs de sécurité, les corrections d'erreurs et les mises à niveau des applications. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Puis lancez la commande suivante :

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    La première partie de la commande `sudo apt update` vérifie si de nouvelles versions de logiciels sont disponibles. La seconde partie de la commande `sudo apt upgrade` installe les dernières mises à jour. Le `-y` à la fin de la commande autorise l'installation des nouveaux paquets.

    Si vous préférez ne pas utiliser le terminal, cliquez sur le bouton `Activités` en haut à gauche de l'écran, et recherchez `Gestionnaire de mises à jour`. Il vérifiera si des mises à jour sont disponibles et vous proposera de les installer.

    Une fois le système est à jour, supprimez les anciens paquets inutiles à l'aide des commandes suivantes :

    ```bash
    sudo apt autoremove
    sudo apt autoclean
    sudo apt clean
    ```

<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Rapports de bogues sur Ubuntu" width="150px"></img> </center>

## Rapports de bogues

[Apport](https://wiki.ubuntu.com/Apport/) est le système de rapport de bogues d'Ubuntu. Il intercepte les plantages et archive les rapports de bogues. Vous pouvez le désactiver pour des raisons de confidentialité en suivant les instructions détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes pour supprimer complètement la fonctionnalité de rapport de bogue :

    ```bash
    sudo apt purge apport
    sudo rm /etc/cron.daily/apport
    ```

    Si vous préférez ne pas utiliser le terminal, ouvrez le gestionnaire de logiciels, cherchez `Apport` et cliquez sur `Supprimer`. Pour simplement désactiver le rapport automatique de bogues tout en conservant la fonctionnalité, ouvrez un terminal et tapez `sudo systemctl disable apport.service`. Ouvrez ensuite le fichier de configuration avec la commande `sudo gedit /etc/default/apport` et mettez la valeur `enabled` à zéro, c'est-à-dire `enabled=0`.


<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Codecs pour Ubuntu" width="150px"></img> </center>

## Codecs

Les codecs indiquent à votre ordinateur comment lire les fichiers vidéo ou audio. Pour des raisons légales et éthiques, certaines distributions Linux n'incluent pas tous les codecs multimédia. Il est donc parfois nécessaire d'installer des codecs supplémentaires pour visualiser des formats de fichiers particuliers. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez les commandes suivantes pour profiter pleinement de votre expérience multimédia :

    ```bash
    sudo add-apt-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -cs) partner"
    sudo apt update
    sudo apt install ubuntu-restricted-extras adobe-flashplugin browser-plugin-freshplayer-pepperflash libavcodec-extra libdvd-pkg
    ```

<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Ubuntu software center" width="150px"></img> </center>

## Gestionnaire de logiciels

Depuis Ubuntu 20.04, le gestionnaire de logiciels classique a été remplacé par « Snap », une nouvelle technologie permettant de fournir des applications regroupées dans un seul fichier. Si vous préférez revenir au gestionnaire de logiciels classique, suivez les instructions détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Puis exécutez les commandes suivantes :

    ```bash
    sudo apt remove snap-store
    sudo apt install gnome-software
    ```

    Si vous souhaitez que le gestionnaire de logiciels prenne en charge les paquets « Snap » ainsi que les paquets « Flatpak », exécutez les commandes suivantes :

    ```bash
    sudo apt install gnome-software-plugin-snap
    sudo apt install gnome-software-plugin-flatpak
    ```

<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Ubuntu Gnome Tweaks" width="150px"></img> </center>

## Apparence et ergonomie

Ubuntu permet de personnaliser pratiquement tous les éléments visuels du système d'exploitation, tels que les polices, les styles de fenêtres, les animations, les thèmes, les icônes, etc. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez la commande suivante pour installer [Gnome Tweaks](https://wiki.gnome.org/Apps/Tweaks/) et accéder ainsi aux contrôles des éléments visuels d'Ubuntu :

    ```bash
    sudo apt install gnome-tweaks
    ```

    Vous pouvez également cliquer sur le bouton `Activités` en haut à gauche et cherchez `Logiciels`. Recherchez maintenant `Gnome Tweaks` et cliquez sur `Installer`.


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Pilotes graphiques Ubuntu" width="150px"></img> </center>

## Pilotes graphiques

La grande majorité des distributions Linux sont livrées avec des pilotes graphiques « open source ». Bien que cela soit suffisant pour faire tourner des applications standard, ce n'est souvent pas assez pour les jeux. Pour installer des pilotes graphiques propriétaires, cliquez sur le bouton `Activités` en haut à gauche de l'écran et tapez `Pilotes additionnels`. Choisissez le bon pilote graphique (la plupart du temps, c'est l'option par défaut) et redémarrez votre système.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Ubuntu" width="150px"></img> </center>

## Assistance

Pour davantage de précisions ou en cas de questions, consultez la [documentation d'Ubuntu](https://help.ubuntu.com/), les [tutoriels d'Ubuntu](https://ubuntu.com/tutorials), le [wiki d'Ubuntu](https://wiki.ubuntu.com/) ou demandez de l'aide à la [communauté conviviale d'Ubuntu](https://forum.ubuntu-fr.org/).

<div align="center">
<img src="https://imgs.xkcd.com/comics/sandwich.png" alt="Ubuntu Desktop"></img>
</div>

<br>
