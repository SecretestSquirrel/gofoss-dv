---
template: main.html
title: So installiert Ihr FOSS-Apps
description: Degooglen. Was ist F-Droid? Wie kann ich F-Droid installieren? Ist F-Droid sicher? Die besten F-Droid Apps. Was ist Aurora Store? Wie kann ich Aurora Store installieren?
---

# Top 50 Tracker-freie FOSS-Apps

!!! level "Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/mobile_3.png" alt="F-Droid" width="500px"></img>
</div>


## F-Droid

Falls Ihr stolze BesitzerIn eines Android-Smartphones seid,solltet Ihr [F-Droid](https://f-droid.org/de/) einen Besuch abstatten! Es handelt sich um einen App-Store, der ausschließlich freie und quelloffene Apps anbietet. Die Installation und Instandhaltung von F-Droid ist relativ einfach. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Schritte | Anweisungen |
    | ------ | ------ |
    | Herunterladen |Öffnet den Browser auf Eurem Smartphone, besucht [F-Droids Webseite](https://f-droid.org/de/) und ladet die `.apk`-Datei herunter. |
    | Installation aus unbekannten Quellen erlauben |Öffnet die heruntergeladene `.apk`-Datei. Ein Warnhinweis sollte angezeigt werden: *"Aus Sicherheitsgründen kannst du auf dem Smartphone keine unbekannten Apps aus dieser Quelle installieren"*. Um die Installation aus unbekannten Quellen vorübergehend zu erlauben, klickt auf `Einstellungen ‣ Dieser Quelle vertrauen`. |
    | F-Droid installieren | Verlasst die Einstellungsseite und klickt auf `Installieren`. |
    | Installation aus unbekannten Quellen verbieten |Begebt Euch in die `Einstellungen` Eures Smartphones und deaktiviert die Installation aus unbekannten Quellen. Je nach Telefon befindet sich diese Option unter `Einstellungen ‣ Apps & Benachrichtigungen ‣ Erweitert ‣ Spezieller App-Zugriff ‣ Unbek. Apps installieren`. Wählt Euren Browser oder Dateimanager und deaktiviert den Eintrag `Dieser Quelle vertrauen`. |
    | F-Droid starten |Nach dem ersten Start aktualisiert F-Droid das Software-Paketquelle. Es handelt sich um die Liste aller auf F-Droid verfügbaren Softwarepakete. Das kann eine Weile dauern, ein wenig Geduld ist gefordert. |
    | Automatische Updates aktivieren |Öffnet F-Droids `Einstellungen` und aktiviert die Optionen `Automatisch installieren` sowie `Aktualisierungsbenachrichtigungen`. |
    | Paketquellen manuell aktualisieren |Wischt auf dem Startbildschirm von F-Droid nach unten, um alle Paketquellen zu aktualisieren. |

    </center>


??? question "Können F-Droid bzw. FOSS-Apps auch auf dem iPhone installiert werden?"

    Besitzer von iOS-Geräten haben in dieser Hinsicht leider Pech. Apples abgeschirmtes Ökosystem macht es [praktisch inkompatibel mit mobilen FOSS-Apps](https://www.fsf.org/blogs/community/why-free-software-and-apples-iphone-dont-mix/). Unter dem Vorwand der Sicherheit schließt Apple mit [Copyright-Sperren](https://www.law.cornell.edu/uscode/text/17/1201) und [Zugriffskontrollen](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32001L0029:DE:HTML) jegliche Bastelei, Reparatur oder Diagnose an iOS-Geräten aus. Apple — nicht die NutzerInnen — darf entscheiden, auf welche Inhalte zugegriffen werden kann, welche Apps installiert werden dürfen und wann das Gerät weggeworfen werden muss.

<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Aurora Store" width="150px"></img> </center>

## Aurora Store

Nicht alle sinnvollen Android-Apps sind frei, quelloffen, trackerfrei oder auf F-Droid verfügbar. Das bedeutet nicht zwangsläufig, dass sie datenschutzschädlich sind. Es bedeutet auch nicht, dass Ihr Googles Play Store verwenden müsst, um solche Apps herunterzuladen. Installiert stattdessen [Aurora Store](https://f-droid.org/de/packages/com.aurora.store/), einen alternativen App-Store, in dem Ihr ohne Google-Konto Apps suchen, herunterladen und aktualisieren könnt.

<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Tracker-freie Apps" width="150px"></img> </center>

## Tracker-freie Apps (F-Droid)

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/fossbrowser.svg" alt="FOSS Browser" width="50px"></img> | [FOSS browser](https://f-droid.org/de/packages/de.baumann.browser/) | Einfacher und ressourcenarmer Browser. |
| <img src="../../assets/img/webapps.svg" alt="Webapps" width="50px"></img> | [Webapps](https://f-droid.org/de/packages/com.tobykurien.webapps/) | Verwandelt Eure Lieblingswebseiten in sichere Anwendungen. |
| <img src="../../assets/img/tutanota.svg" alt="Tutanota" width="50px"></img> | [Tutanota](https://f-droid.org/de/packages/de.tutao.tutanota/) | Quelloffene E-Mail-App. Inklusive dunklem Thema, Verschlüsselung, Push-Benachrichtigungen, automatischer Synchronisierung, Volltextsuche, Wischgesten usw. |
| <img src="../../assets/img/libremmail.svg" alt="Librem Mail" width="50px"></img> | [Librem mail](https://f-droid.org/de/packages/one.librem.mail/) | Quelloffene E-Mail-App, Fork von K-9 Mail. Inklusive Verschlüsselung und Multi-Konten-Unterstützung. |
| <img src="../../assets/img/simpleemail.svg" alt="Simple Email" width="50px"></img> | [Simple email](https://f-droid.org/de/packages/org.dystopia.email/) | Quelloffene E-Mail-App. Datenschutzfreundlich, inklusive Verschlüsselung, Multi-Konten-Unterstützung, Zwei-Wege-Synchronisation, Offline-Speicher, dunklem Thema, serverseitiger Suche und einfachem Design. |
| <img src="../../assets/img/k9mail.svg" alt="K-9 mail" width="50px"></img> | [K-9 mail](https://f-droid.org/de/packages/com.fsck.k9/) |Quelloffenene E-Mail-App. Unterstützt POP3 und IMAP, Push-Mail wird allerdings nur für IMAP unterstützt. |
| <img src="../../assets/img/silence.svg" alt="Silence" width="50px"></img> | [Silence](https://f-droid.org/de/packages/org.smssecure.smssecure/) | Verschlüsselte SMS- und MMS-App zum Schutz Eurer Privatsphäre. |
| <img src="../../assets/img/signal.svg" alt="Element" width="50px"></img> | [Element](https://f-droid.org/de/packages/im.vector.app/) | Quelloffener Client für das Matrix-Protokoll. Matrix ist ein Netzwerk für sichere, durchgängig verschlüsselte und dezentrale Kommunikation. Kann selbst gehostet werden. |
| <img src="../../assets/img/silence.svg" alt="Conversations" width="50px"></img> | [Conversations](https://f-droid.org/de/packages/eu.siacs.conversations/) | Quelloffener Client für das XMPP-Protokoll. Ermöglicht sichere und dezentrale Kommunikation. Kann selbst gehostet werden. |
| <img src="../../assets/img/signal.svg" alt="Briar" width="50px"></img> | [Briar](https://f-droid.org/de/packages/org.briarproject.briar.android/) | Quelloffener Messenger. Peer-to-Peer (stützt sich nicht auf zentrale Server), durchgängig verschlüsselt, legt minimale Informationen offen. Nutzt Tor. |
| <img src="../../assets/img/tusky.svg" alt="Tusky" width="50px"></img> | [Tusky](https://f-droid.org/de/packages/com.keylesspalace.tusky/) | Ressourcenarmer Client für Mastodon, ein freies und quelloffenes soziales Netzwerk als Alternative zu Twitter oder Facebook. |
| <img src="../../assets/img/redreader.svg" alt="RedReader" width="50px"></img> | [RedReader](https://f-droid.org/de/packages/org.quantumbadger.redreader/) | Freier und quelloffener Client für reddit.com. Keine Tracker, keine Reklame. |
| <img src="../../assets/img/redreader.svg" alt="Infinity" width="50px"></img> | [Infinity für Reddit](https://f-droid.org/de/packages/ml.docilealligator.infinityforreddit/) | Freier und quelloffener Client für reddit.com. Keine Tracker, keine Reklame. |
| <img src="../../assets/img/feeder.svg" alt="Feeder" width="50px"></img> | [Feeder](https://f-droid.org/de/packages/com.nononsenseapps.feeder/) | Quelloffener RSS-Feed-Reader. Kein Tracking, keine Kontoerstellung, Offline-Modus, Hintergrundsynchronisation und Benachrichtigungen. |
| <img src="../../assets/img/feeder.svg" alt="Flym" width="50px"></img> | [Flym](https://f-droid.org/packages/net.frju.flym/) |Quelloffener RSS-Feed-Reader. |
| <img src="../../assets/img/maps.svg" alt="Osmand" width="50px"></img> | [Osmand](https://f-droid.org/de/packages/net.osmand.plus/) |Quelloffene App für Online- sowie Offline-Karten und Navigation. |
| <img src="../../assets/img/maps.svg" alt="Organic Maps" width="50px"></img> | [Organic Maps](https://f-droid.org/de/packages/app.organicmaps/) |Quelloffene App für Online- sowie Offline-Karten und Navigation. Fork von Maps.me, aber ohne Tracker. |
| <img src="../../assets/img/opencamera.svg" alt="Open Camera" width="50px"></img> | [Open camera](https://f-droid.org/de/packages/net.sourceforge.opencamera/) |Funktionsreiche Kamera-App mit Autostabilisierung, Multizoom-Touch, Blitz, Gesichtserkennung, Timer, Serienbildmodus, stummschaltbarem Auslöser und vielem mehr. |
| <img src="../../assets/img/simplegallery.svg" alt="Simple Gallery" width="50px"></img> | [Simple gallery](https://f-droid.org/de/packages/com.simplemobiletools.gallery.pro/) |Hochgradig anpassbare Offline-Foto-Galerie. Kommt ohne Internetzugang aus, zum Wohle Eurer Privatsphäre. Erlaubt es Fotos zu organisieren und zu bearbeiten, gelöschte Dateien wiederherzustellen, Dateien zu schützen oder verstecken, sowie eine Vielzahl verschiedener Foto- und Videoformate zu betrachten (einschließlich RAW, SVG, usw.). |
| <img src="../../assets/img/simplemusic.svg" alt="Simple Music Player" width="50px"></img> | [Simple music player](https://f-droid.org/de/packages/com.simplemobiletools.musicplayer/) |Leicht zu steuernder Musikplayer, sei es über die Statusleiste, das Home-Screen-Widget oder die Kopfhörer. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. |
| <img src="../../assets/img/simplecalculator.svg" alt="Simple Calculator" width="50px"></img> | [Simple calculator](https://f-droid.org/de/packages/com.simplemobiletools.calculator/) |Quelloffener Rechner, werbefrei und zugriffsarm. Anpassungsfähiges Design. |
| <img src="../../assets/img/editor.svg" alt="Simple Notes" width="50px"></img> | [Simple notes](https://f-droid.org/de/packages/com.simplemobiletools.notes.pro/) |Quelloffene Notiz-App, werbefrei und zugriffsarm. Anpassungsfähiges Design und Widgets. |
| <img src="../../assets/img/editor.svg" alt="Notepad" width="50px"></img> | [Notepad](https://f-droid.org/de/packages/com.farmerbb.notepad/) |Einfache, quelloffene Notiz-App. |
| <img src="../../assets/img/editor.svg" alt="Carnet" width="50px"></img> | [Carnet](https://f-droid.org/de/packages/com.spisoft.quicknote/) |Funktionsreiche, quelloffene Notiz-App, inklusive Synchronisationsmöglichkeiten (z.B. NextCloud) und Online-Editor. |
| <img src="../../assets/img/editor.svg" alt="Markor" width="50px"></img> | [Markor](https://f-droid.org/de/packages/net.gsantner.markor/) |Quelloffener Texteditor mit Markdown-Unterstützung. |
| <img src="../../assets/img/simpleclock.svg" alt="Simple Clock" width="50px"></img> | [Simple clock](https://f-droid.org/de/packages/com.simplemobiletools.clock/) |Uhr, Alarm, Stoppuhr, Timer. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. |
| <img src="../../assets/img/simplecontacts.svg" alt="Simple Contacts" width="50px"></img> | [Simple contacts](https://f-droid.org/de/packages/com.simplemobiletools.contacts.pro/) |Einfache App zur Verwaltung Eurer Kontakte. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. Kontakte können lokal gespeichert oder mit der Cloud synchronisiert werden. Wenn Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/) könnt Ihr diese App zur [Verwaltung und Synchronisierung Eurer Kontakte](https://gofoss.net/de/contacts-calendars-tasks/) verwenden. |
| <img src="../../assets/img/simplecontacts.svg" alt="Open Contacts" width="50px"></img> | [Open contacts](https://f-droid.org/de/packages/opencontacts.open.com.opencontacts/) |Quelloffene Kontakt-App. |
| <img src="../../assets/img/simplecalendar.svg" alt="Simple Calendar" width="50px"></img> | [Simple calendar](https://f-droid.org/de/packages/com.simplemobiletools.calendar.pro/) |Anpassungsfähiger Offline-Kalender zur Organisation von einmaligen oder wiederkehrenden Ereignissen, Geburtstagen, Jahrestagen, Geschäftsterminen, und so weiter. Tages-, Wochen- und Monatsansichten verfügbar. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. Wenn Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/) könnt Ihr diese App zur [Verwaltung und Synchronisierung Eurer Kalender](https://gofoss.net/de/contacts-calendars-tasks/) verwenden. |
| <img src="../../assets/img/doodle.svg" alt="Etar" width="50px"></img> | [Etar](https://f-droid.org/de/packages/ws.xsoh.etar/) |Quelloffener Kalender im Materialdesign. Funktioniert auch mit Online-Kalendern. Frei, quelloffen und werbefrei. |
| <img src="../../assets/img/tasks.svg" alt="OpenTasks" width="50px"></img> | [OpenTasks](https://f-droid.org/de/packages/org.dmfs.tasks/) |Quelloffener Task-Manager. Wenn Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/) könnt Ihr diese App zur [Verwaltung und Synchronisierung Eurer Aufgabenlisten](https://gofoss.net/de/contacts-calendars-tasks/) verwenden. |
| <img src="../../assets/img/davx5.svg" alt="Davx5" width="50px"></img> | [Davx5](https://f-droid.org/de/packages/at.bitfire.davdroid/) | Quelloffener Client zum Synchronisieren von Kontakten, Kalendern und Aufgabenlisten. Kann [selbst gehostet](https://gofoss.net/de/contacts-calendars-tasks/), oder mit einem vertrauenswürdigen Hoster verwendet werden. |
| <img src="../../assets/img/antennapod.svg" alt="AntennaPod" width="50px"></img> | [AntennaPod](https://f-droid.org/de/packages/de.danoeh.antennapod/) |Funktionsreicher Podcast-Manager und -Player. Bietet sofortigen Zugriff auf Millionen von kostenlosen oder -pflichtigen Programmen, von unabhängigen Podcastern bis hin zu großen Verlagshäusern wie der BBC, NPR oder CNN. |
| <img src="../../assets/img/radiodroid.svg" alt="RadioDroid" width="50px"></img> | [RadioDroid](https://f-droid.org/de/packages/net.programmierecke.radiodroid2/) |Eine App zum Hören von Online-Radiosendern. |
| <img src="../../assets/img/documentviewer.svg" alt="Document Viewer" width="50px"></img> | [Document viewer](https://f-droid.org/de/packages/org.sufficientlysecure.viewer/) |Zeigt verschiedene Dateiformate an, inklusive pdf, djvu, epub, xps und Comics (cbz, fb2). |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Libre Office Viewer" width="50px"></img> | [Libre Office viewer](https://f-droid.org/de/packages/org.documentfoundation.libreoffice/) |Zeigt docx, doc, xlsx, xls, pptx, ppt, odt, ods und odp Dateien an. |
| <img src="../../assets/img/https.svg" alt="Keepass DX" width="50px"></img> | [Keepass DX](https://f-droid.org/de/packages/com.kunzisoft.keepass.libre/) |Sicherer und quelloffener Passwortmanager. |
| <img src="../../assets/img/letsencrypt.svg" alt="andOTP" width="50px"></img> | [andOTP](https://f-droid.org/de/packages/org.shadowice.flocke.andotp/) |Freie und quelloffene Anwendung zur Zwei-Faktor-Authentifizierung. |
| <img src="../../assets/img/letsencrypt.svg" alt="Free OTP" width="50px"></img> | [Free OTP](https://f-droid.org/de/packages/org.liberty.android.freeotpplus/) |Quelloffener Zwei-Faktor-Authentifikator. |
| <img src="../../assets/img/letsencrypt.svg" alt="Aegis" width="50px"></img> | [Aegis](https://f-droid.org/de/packages/com.beemdevelopment.aegis/) |Freier, sicherer und quelloffener Zwei-Faktor-Authentifikator. |
| <img src="../../assets/img/openvpn.svg" alt="Open VPN" width="50px"></img> | [Open VPN](https://f-droid.org/de/packages/de.blinkt.openvpn/) |Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/), erlaubt Euch diese App eine VPN-Verbindung herzustellen. |
| <img src="../../assets/img/openvpn.svg" alt="Proton VPN" width="50px"></img> | [Proton VPN](https://f-droid.org/de/packages/ch.protonvpn.android/) |Sicherer und (teilweise) kostenloser VPN-Anbieter. Gibt an, keine Benutzeraktivitäten zu erfassen. Bietet Verschlüsselung, Schweizer Datenschutzgesetze, DNS-Leckschutz, Kill-Switch und mehr. |
| <img src="../../assets/img/simplefilemanager.svg" alt="Simple File Manager" width="50px"></img> | [Simple file manager](https://f-droid.org/de/packages/com.simplemobiletools.filemanager.pro/) |Durchsucht und bearbeitet Dateien auf Eurem Telefon. Quelloffen, werbefrei und mit anpassungsfähigem Design. |
| <img src="../../assets/img/simplefilemanager.svg" alt="DroidFS" width="50px"></img> | [DroidFS](https://f-droid.org/de/packages/sushi.hardcore.droidfs/) |Quelloffene App zum sicheren Speichern und Zugreifen auf Eure verschlüsselten Dateien. |
| <img src="../../assets/img/seafile.svg" alt="Seafile" width="50px"></img> | [Seafile](https://f-droid.org/de/packages/com.seafile.seadroid2/) |Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/), erlaubt Euch diese App [Eure Cloud-Dateien zu verwalten und zu synchronisieren](https://gofoss.net/de/cloud-storage/). |
| <img src="../../assets/img/anysoft.svg" alt="Anysoft Keyboard" width="50px"></img> | [Anysoft keyboard](https://f-droid.org/de/packages/com.menny.android.anysoftkeyboard/) |Quelloffene und datenschutzfreudliche Tastatur, unterstützt mehrere Sprachen, Spracheingabe, Gesten, Nachtmodus und verschiedene Themen. |
| <img src="../../assets/img/anysoft.svg" alt="Simple Keyboard" width="50px"></img> | [Simple keyboard](https://f-droid.org/de/packages/rkr.simplekeyboard.inputmethod/) |Quelloffene Tastatur. |
| <img src="../../assets/img/anysoft.svg" alt="Open Board" width="50px"></img> | [Open board](https://f-droid.org/de/packages/org.dslul.openboard.inputmethod.latin/) |Quelloffene Tastatur. |
| <img src="../../assets/img/anysoft.svg" alt="Hacker's Keyboard" width="50px"></img> | [Hacker's keyboard](https://f-droid.org/de/packages/org.pocketworkstation.pckeyboard/) |Quelloffene Tastatur mit separaten Zifferntasten, Interpunktionszeichen sowie Pfeiltasten. |
| <img src="../../assets/img/totem.svg" alt="Newpipe" width="50px"></img> | [Newpipe](https://f-droid.org/de/packages/org.schabi.newpipe/) | Ressourcenarme YouTube-App, ohne proprietäre API oder Googles Play-Dienste. Unterstützt ebenfalls PeerTube. |
| <img src="../../assets/img/totem.svg" alt="Thorium" width="50px"></img> | [Thorium](https://f-droid.org/de/packages/net.schueller.peertube/) |PeerTube ist eine freie, quelloffene und dezentrale Video-Hosting-Plattform. |
| <img src="../../assets/img/gimp.svg" alt="Quick Dic" width="50px"></img> | [Quick dic](https://f-droid.org/de/packages/de.reimardoeffinger.quickdic/) |Offline-Wörterbuch. |
| <img src="../../assets/img/exodus.svg" alt="Exodus" width="50px"></img> | [Exodus](https://f-droid.org/de/packages/org.eu.exodus_privacy.exodusprivacy/) |Findet heraus, welche Tracker und Berechtigungen Eure Apps enthalten. |
| <img src="../../assets/img/lawnchair.svg" alt="Zim launcher" width="50px"></img> | [Zim launcher](https://f-droid.org/de/packages/org.zimmob.zimlx/) |Werbefreie und quelloffene Launcher-App. |

</center>


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Aurora Store" width="150px"></img> </center>

## Datenschutzfreundliche Apps (Aurora)

<center>

| App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/firefox.svg" alt="Firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/de/firefox/browsers/mobile/) | Schneller, sicherer und privater Browser. *Hinweis*: [Enthält 3 Tracker (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/de/reports/org.mozilla.firefox/latest/). |
| <img src="../../assets/img/tor.svg" alt="Tor Browser" width="50px"></img> | [Tor Browser](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=de_DE/) |Tor Browser für Android. Blockiert Tracker, schützt vor Überwachung, widersteht digitalen Fingerabdrücken und verschlüsselt den Internetverkehr. *Hinweis*: [Enthält 3 Tracker (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/de/reports/org.torproject.torbrowser/latest/). |
| <img src="../../assets/img/protonmail.svg" alt="Protonmail" width="50px"></img> | [Protonmail](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=de_DE/) |Der weltweit größte sichere E-Mail-Dienst, entwickelt von Wissenschaftlern des CERN und des MIT. Quelloffen und durch Schweizer Datenschutzgesetze geschützt. Enthält keine Tracker. |
| <img src="../../assets/img/signal.svg" alt="Signal" width="50px"></img> | [Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=de_DE/) |Schneller, einfacher und sicherer Nachrichtenaustausch. Durchgängig verschlüsselte Text-, Sprach- und Videonachrichten, sowie Übermittlung von Dokumenten und Bildern. Quelloffen, werbefrei, Tracker-frei. *Hinweis*: benötigt die Angabe Eurer Telefonnummer. |
| <img src="../../assets/img/lawnchair.svg" alt="Lawnchair" width="50px"></img> | [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah&hl=de) |Freier und quelloffener Launcher. Symbolgröße, Schriftart, Symbole, Benachrichtigungen usw. können angepasst werden. Enthält keine Tracker. |
| <img src="../../assets/img/alarm.svg" alt="Simple Alarm Clock" width="50px"></img> | [Simple Alarm clock](https://play.google.com/store/apps/details?id=com.better.alarm&hl=de) |Quelloffener und funktionsreicher Wecker mit einer übersichtlichen Oberfläche. |

</center>


<br>

<center> <img src="../../assets/img/separator_unzip.svg" alt="FOSS apps" width="150px"></img> </center>

## Andere FOSS-Apps

<center>

| FOSS-Apps | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/fossbrowser.svg" alt="Fennec" width="50px"></img> | [Fennec F-Droid](https://f-droid.org/de/packages/org.mozilla.fennec_fdroid/) |Datenschutzfreundliche Version von Firefox. Ist darauf ausgelegt, alle in den offiziellen Mozilla-Builds enthaltenen proprietären Elemente zu entfernen. *Hinweis*: [Enthält 2 Tracker (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/de/reports/org.mozilla.fennec_fdroid/latest/). |
| <img src="../../assets/img/firefox.svg" alt="Mull Browser" width="50px"></img> | [Mull](https://f-droid.org/de/packages/us.spotco.fennec_dos/) | Firefox-Fork für Android. Von der DivestOS-Mannschaft entwickelt, basiert auf den Tor Uplift und arkenfox-user.js Projekten. *Hinweis*: [Enthält 2 Tracker (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/de/reports/us.spotco.fennec_dos/latest/).|
| <img src="../../assets/img/fossbrowser.svg" alt="Bromite" width="50px"></img> | [Bromite](https://www.bromite.org/) |Chromium-basierter Browser, inklusive Werbeblocker und erhöhten Datenschutz. Nicht über F-Droid erhältlich. |
| <img src="../../assets/img/alarm.svg" alt="Insane Alarm" width="50px"></img> | [Insane alarm](https://github.com/RIAEvangelist/insane-alarm/releases/) |Quelloffener Alarm, weckt Euch garantiert auf! Nicht über F-Droid erhältlich. |
| <img src="../../assets/img/tasks.svg" alt="Tasks.org" width="50px"></img> | [Tasks.org](https://f-droid.org/de/packages/org.tasks/) |Quelloffener Task-Manager. Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/), erlaubt Euch diese App [Eure Aufgabenlisten zu verwalten und zu synchronisieren](https://gofoss.net/de/contacts-calendars-tasks/). *Hinweis*: [Enthält 2 Tracker (Google CrashLytics, Google Analytics).](https://reports.exodus-privacy.eu.org/de/reports/org.tasks/latest/) |
| <img src="../../assets/img/editor.svg" alt="Standard Notes" width="50px"></img> | [Standard notes](https://f-droid.org/de/packages/com.standardnotes/) |Freie, quelloffene und verschlüsselte Notiz-App. *Hinweis*: [Enthält 1 Tracker (bugsnag)](https://reports.exodus-privacy.eu.org/de/reports/com.standardnotes/latest/) |
| <img src="../../assets/img/maps.svg" alt="Magic earth" width="50px"></img> | [Magic earth](https://play.google.com/store/apps/details?id=com.generalmagic.magicearth&hl=de) |Verwendet OpenStreetMap-Daten, nicht vollständig quelloffen, nicht über F-Droid erhältlich. Behauptet, keine Benutzerdaten zu erfassen. Inklusive Dashcam, Navigation, Verkehrsinformationen, Fahrpläne für öffentliche Verkehrsmittel und Wetterangaben. |
| <img src="../../assets/img/doodle.svg" alt="Transportr" width="50px"></img> | [Transportr](https://f-droid.org/de/packages/de.grobox.liberario/) |Quelloffene App für Fahrpläne des öffentlichen Nahverkehrs in Europa und anderen Regionen. *Hinweis*: [Enthält 1 Tracker (mapbox)](https://reports.exodus-privacy.eu.org/de/reports/de.grobox.liberario/latest/)|
| <img src="../../assets/img/totem.svg" alt="Free tube" width="50px"></img> | [Free Tube](https://github.com/FreeTubeApp/FreeTube/) |Quelloffenen und datenschutzfreundliche Youtube-App. Nicht über F-Droid erhältlich. |
| <img src="../../assets/img/aurora.svg" alt="Fossdroid" width="50px"></img> | [Fossdroid](https://fossdroid.com/) |App-Store ähnlich wie F-Droid, bietet freie und quelloffene Android-Apps an. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten finden Ihr:

* in der [F-Droid Dokumentation](https://f-droid.org/de/docs/)
* Ihr könnt auch gerne die [F-Droid Gemeinschaft](https://forum.f-droid.org/) um Unterstützung bitten
* oder aber die [Reddit Gemeinschaft](https://teddit.net/r/fossdroid/)


<br>