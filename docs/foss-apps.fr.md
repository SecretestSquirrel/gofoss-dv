---
template: main.html
title: Installez des applications libres et open source
description: Degooglisez. Qu'est ce que F-Droid ? Comment installer F-Droid ? Est ce que F-Droid est sûr ? Les meilleures applications de F-Droid. Qu'est ce que Aurora Store ? Comment installer Aurora Store ?
---

# 50 applications libres sans traqueurs

!!! level "Destiné aux utilisateurs expérimentés. Certaines compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/mobile_3.png" alt="F-Droid" width="500px"></img>
</div>


## F-Droid

Si vous détenez un smartphone Android, jetez un œil à [F-Droid](https://f-droid.org/fr/) ! C'est un magasin d'applications exclusivement dédié aux applications libres et open source. Installer et maintenir F-Droid à jour est très simple. Plus d'instructions sont données ci-après.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Instructions |
    | ------ | ------ |
    |Téléchargez F-Droid |Ouvrez votre navigateur, allez sur [le site de F-Droid](https://f-droid.org/fr/) et téléchargez le fichier `.apk`.|
    |Activez l'installation depuis des sources inconnues |Ouvrez le fichier `.apk` téléchargé. Votre téléphone devrait afficher un message du type *« À des fins de sécurité, l'installation d'applications inconnues provenant de cette source n'est pas autorisée sur ce téléphone »*. Pour autoriser temporairement l'opération, cliquez sur `Paramètres ‣ Autoriser cette source`. |
    |Installez F-Droid |Quittez la page des paramètres et appuyez sur `Installer`.|
    |Désactivez l'installation depuis des sources inconnues |Retournez dans les `Paramètres` de votre téléphone pour désactiver l'installation depuis des sources inconnues. Selon votre appareil, cette option pourra être située dans : `Paramètres ‣ Applis et notifications ‣ Options avancées ‣ Accès spécifiques des applications ‣ Installation d'applis inconnues`. Sélectionnez votre navigateur ou gestionnaire de fichier et décochez l'option `Autoriser cette source`. |
    |Lancez F-Droid |Après le premier lancement, F-Droid mettra à jour le dépôt, c'est-à-dire la liste de tous les logiciels disponibles sur F-Droid. Cette mise à jour peut prendre du temps, soyez patient. |
    |Activez les mises à jour automatiques |Allez dans les `Paramètres` de F-Droid et activez les options `Installer automatiquement les mises à jour` et `Afficher les mises à jour disponibles`. |
    |Mettez à jour manuellement les dépôts | Retournez sur l'écran d'accueil de F-Droid et balayez vers le bas pour mettre à jour tous les dépôts. |


    </center>


??? question "Puis-je installer F-Droid ou des applications libres sur mon iPhone ?"

    Les propriétaires d'appareils iOS n'ont pas ce privilège. Le système clos d'Apple les rend de fait [incompatibles avec les applications mobiles libres](https://www.fsf.org/blogs/community/why-free-software-and-apples-iphone-dont-mix/). Sous prétexte de sécurité, Apple a recours au [verrouillage du droit d'auteur](https://www.law.cornell.edu/uscode/text/17/1201) et au [contrôle d'accès](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32001L0029:FR:HTML) pour empêcher tout bricolage, tout déverrouillage, toute réparation ou tout diagnostic sur les appareils iOS. C'est Apple, et non l'utilisateur, qui décide du contenu auquel il est possible d'accéder, des applications qui peuvent être installées et du moment à partir duquel l'appareil devient obsolète.

<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Aurora Store" width="150px"></img> </center>

## Aurora Store

Toutes les applications Android utiles ne sont pas libres, sans traqueurs ou disponibles sur F-Droid. Cela ne signifie pas nécessairement qu'elles soient préjudiciables à votre vie privée. Cela ne signifie pas non plus que vous êtes obligé d'utiliser le Play Store de Google. Installez plutôt [Aurora Store](https://f-droid.org/fr/packages/com.aurora.store/), un magasin d'applications alternatif où vous pouvez rechercher, télécharger et mettre à jour vos applications sans avoir besoin d'un compte Google.


<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Applis sans traqueurs" width="150px"></img> </center>

## Applis sans traqueurs (F-Droid)

<center>

| Applis FOSS | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/fossbrowser.svg" alt="FOSS browser" width="50px"></img> | [FOSS browser](https://f-droid.org/fr/packages/de.baumann.browser/) | Navigateur simple et léger. |
| <img src="../../assets/img/webapps.svg" alt="Webapps" width="50px"></img> | [Webapps](https://f-droid.org/fr/packages/com.tobykurien.webapps/) | Transformez vos sites web mobiles préférés en applications sécurisées. |
| <img src="../../assets/img/tutanota.svg" alt="Tutanota" width="50px"></img> | [Tutanota](https://f-droid.org/fr/packages/de.tutao.tutanota/) | Appli de messagerie électronique open source. Inclut un thème sombre, le cryptage, les notifications instantanées, la synchronisation automatique, la recherche plein texte, les gestes de balayage, etc. |
| <img src="../../assets/img/libremmail.svg" alt="Librem mail" width="50px"></img> | [Librem mail](https://f-droid.org/fr/packages/one.librem.mail/) | Appli de messagerie électronique, open source et dérivée de l'appli K-9 Mail. Inclut le cryptage et les comptes multiples. |
| <img src="../../assets/img/simpleemail.svg" alt="Simple email" width="50px"></img> | [Simple email](https://f-droid.org/fr/packages/org.dystopia.email/) | Appli de messagerie électronique open source. Inclut le respect de la vie privée, le cryptage, les comptes multiples, la synchronisation bidirectionnelle, le stockage hors ligne, un thème sombre, la possibilité de rechercher des messages sur le serveur, et un design sobre. |
| <img src="../../assets/img/k9mail.svg" alt="K-9 mail" width="50px"></img> | [K-9 mail](https://f-droid.org/fr/packages/com.fsck.k9/) |Appli de messagerie électronique open source. Prend en charge les protocoles POP3 et IMAP (uniquement le « push mail » pour IMAP. |
| <img src="../../assets/img/silence.svg" alt="Silence" width="50px"></img> | [Silence](https://f-droid.org/fr/packages/org.smssecure.smssecure/) | Appli de textos et de messages multimédias cryptée, qui protége votre vie privée. |
| <img src="../../assets/img/signal.svg" alt="Element" width="50px"></img> | [Element](https://f-droid.org/fr/packages/im.vector.app/) | Client open source pour le protocole Matrix. C'est un réseau de communication décentralisé, sécurisé et crypté de bout en bout, dont les serveurs peuvent être auto-hébergés. |
| <img src="../../assets/img/silence.svg" alt="Conversations" width="50px"></img> | [Conversations](https://f-droid.org/fr/packages/eu.siacs.conversations/) | Client open source pour le protocole XMPP. C'est un réseau de communication décentralisé et sécurisé, dont les serveurs peuvent être auto-hébergés. |
| <img src="../../assets/img/signal.svg" alt="Briar" width="50px"></img> | [Briar](https://f-droid.org/fr/packages/org.briarproject.briar.android/) | Appli de messagerie open source. Basée sur une architecture de pair à pair (ne repose pas sur des serveurs centraux), cryptée de bout en bout et avec une faible exposition d'informations. Utilise Tor. |
| <img src="../../assets/img/tusky.svg" alt="Tusky" width="50px"></img> | [Tusky](https://f-droid.org/fr/packages/com.keylesspalace.tusky/) | Client léger pour Mastodon, un réseau social libre et open source destiné à remplacer Twitter ou Facebook. |
| <img src="../../assets/img/redreader.svg" alt="RedReader" width="50px"></img> | [RedReader](https://f-droid.org/fr/packages/org.quantumbadger.redreader/) | Client FOSS pour reddit.com. Pas de traqueur, pas de publicité. |
| <img src="../../assets/img/redreader.svg" alt="Infinity" width="50px"></img> | [Infinity for Reddit](https://f-droid.org/fr/packages/ml.docilealligator.infinityforreddit/) | Client FOSS pour reddit.com. Pas de traqueur, pas de publicité. |
| <img src="../../assets/img/feeder.svg" alt="Feeder" width="50px"></img> | [Feeder](https://f-droid.org/fr/packages/com.nononsenseapps.feeder/) | Lecteur de flux RSS open source. Pas de suivi, pas besoin de compte. Prend en charge la lecture hors ligne, la synchronisation en arrière-plan et les notifications. |
| <img src="../../assets/img/feeder.svg" alt="Flym" width="50px"></img> | [Flym](https://f-droid.org/fr/packages/net.frju.flym/) |Lecteur de flux RSS open source.  |
| <img src="../../assets/img/maps.svg" alt="Osmand" width="50px"></img> | [Osmand](https://f-droid.org/fr/packages/net.osmand.plus/) |Appli open source pour les cartes et la navigation. Fonctionne également en mode hors connexion. |
| <img src="../../assets/img/maps.svg" alt="Organic Maps" width="50px"></img> | [Organic Maps](https://f-droid.org/fr/packages/app.organicmaps/) |Appli open source pour les cartes et la navigation. Fonctionne également en mode hors connexion. Dérivé de l'appli Maps.me. Sans traqueurs. |
| <img src="../../assets/img/opencamera.svg" alt="Open camera" width="50px"></img> | [Open camera](https://f-droid.org/fr/packages/net.sourceforge.opencamera/) |Appli appareil photo riche en fonctionnalités, comprenant l'auto-stabilisation, le multizoom tactile, le flash, la détection des visages, un minuteur, un mode rafale, l'obturateur silencieux, etc. |
| <img src="../../assets/img/simplegallery.svg" alt="Simple gallery" width="50px"></img> | [Simple gallery](https://f-droid.org/fr/packages/com.simplemobiletools.gallery.pro/) |Galerie de photo hors ligne hautement personnalisable. Elle ne nécessite aucun accès à Internet pour protéger votre vie privée. Organisez et modifiez vos photos, récupérez des fichiers supprimés, protégez ou cachez des fichiers et visualisez une grande variété de formats de photos et de vidéos, y compris RAW, SVG, etc. |
| <img src="../../assets/img/simplemusic.svg" alt="Simple music player" width="50px"></img> | [Simple music player](https://f-droid.org/fr/packages/com.simplemobiletools.musicplayer/) |Lecteur de musique, facilement contrôlable depuis la barre d'état, le widget sur l'écran d'accueil ou par les boutons de votre casque. Entièrement open source, ne contient pas de publicités ni de autorisations inutiles. Couleurs personnalisables. |
| <img src="../../assets/img/simplecalculator.svg" alt="Simple calculator" width="50px"></img> | [Simple calculator](https://f-droid.org/fr/packages/com.simplemobiletools.calculator/) |Calculatrice entièrement open source, sans publicités ni autorisations inutiles. Couleurs personnalisables. |
| <img src="../../assets/img/editor.svg" alt="Simple notes" width="50px"></img> | [Simple notes](https://f-droid.org/fr/packages/com.simplemobiletools.notes.pro/) |Appli de prise de notes entièrement open source, sans publicités ni autorisations inutiles. Couleurs et widget personnalisables. |
| <img src="../../assets/img/editor.svg" alt="Notepad" width="50px"></img> | [Notepad](https://f-droid.org/fr/packages/com.farmerbb.notepad/) |Application simple et open source de prise de notes. |
| <img src="../../assets/img/editor.svg" alt="Carnet" width="50px"></img> | [Carnet](https://f-droid.org/fr/packages/com.spisoft.quicknote/) |Puissante appli open source de prise de notes, avec des capacités de synchronisation (y compris NextCloud) et un éditeur en ligne. |
| <img src="../../assets/img/editor.svg" alt="Markor" width="50px"></img> | [Markor](https://f-droid.org/fr/packages/net.gsantner.markor/) |Éditeur de texte open source qui prend notamment en charge le format « markdown ». |
| <img src="../../assets/img/simpleclock.svg" alt="Simple clock" width="50px"></img> | [Simple clock](https://f-droid.org/fr/packages/com.simplemobiletools.clock/) |Horloge, alarme, chronomètre, minuteur. Entièrement open source, sans publicités ni autorisations inutiles. Couleurs personnalisables. |
| <img src="../../assets/img/simplecontacts.svg" alt="Simple contacts" width="50px"></img> | [Simple contacts](https://f-droid.org/fr/packages/com.simplemobiletools.contacts.pro/) |Appli simple pour créer et gérer vos contacts. Entièrement open source, sans publicités ni autorisations inutiles. Couleurs personnalisables. Les contacts peuvent être stockés localement sur vos appareils, ou synchronisés avec le nuage. Si vous [hébergez votre propre serveur](https://gofoss.net/fr/intro-free-your-cloud/), cette appli peut être utilisée pour [gérer et synchroniser vos contacts](https://gofoss.net/fr/contacts-calendars-tasks/). |
| <img src="../../assets/img/simplecontacts.svg" alt="Open contacts" width="50px"></img> | [Open contacts](https://f-droid.org/fr/packages/opencontacts.open.com.opencontacts/) |Appli de contact open source. |
| <img src="../../assets/img/simplecalendar.svg" alt="Simple calendar" width="50px"></img> | [Simple calendar](https://f-droid.org/fr/packages/com.simplemobiletools.calendar.pro/) |Calendrier hors ligne entièrement personnalisable pour organiser vos événements (récurrents), anniversaires, réunions professionnelles, rendez-vous, etc. Prend en charge l'affichage quotidien, hebdomadaire et mensuel. Entièrement open source, sans publicités ni autorisations inutiles. Couleurs personnalisables. Si vous [hébergez votre propre serveur](https://gofoss.net/fr/intro-free-your-cloud/), cette appli peut être utilisée pour [gérer et synchroniser vos calendriers](https://gofoss.net/fr/contacts-calendars-tasks/). |
| <img src="../../assets/img/doodle.svg" alt="Etar" width="50px"></img> | [Etar](https://f-droid.org/fr/packages/ws.xsoh.etar/) |Calendrier libre et open source, suivant le design « material ». Fonctionne avec les calendriers en ligne. Sans publicité. |
| <img src="../../assets/img/tasks.svg" alt="OpenTasks" width="50px"></img> | [OpenTasks](https://f-droid.org/fr/packages/org.dmfs.tasks/) |Gestionnaire de tâches open source. Si vous [hébergez votre propre serveur](https://gofoss.net/fr/intro-free-your-cloud/), cette appli peut être utilisée pour [gérer et synchroniser vos listes de tâches](https://gofoss.net/fr/contacts-calendars-tasks/). |
| <img src="../../assets/img/davx5.svg" alt="Davx5" width="50px"></img> | [Davx5](https://f-droid.org/fr/packages/at.bitfire.davdroid/) | Client open source pour synchroniser vos contacts, calendriers et listes de tâches. Peut être utilisé avec un [serveur auto-hébergé](https://gofoss.net/fr/contacts-calendars-tasks/) ou un hébergeur de confiance. |
| <img src="../../assets/img/antennapod.svg" alt="AntennaPod" width="50px"></img> | [AntennaPod](https://f-droid.org/fr/packages/de.danoeh.antennapod/) |Gestionnaire et lecteur de podcasts. Fournit un accès instantané à des millions de podcasts gratuits et payants, allant des « podcasters » indépendants aux grandes maisons d'édition telles que la BBC, NPR et CNN. |
| <img src="../../assets/img/radiodroid.svg" alt="RadioDroid" width="50px"></img> | [RadioDroid](https://f-droid.org/fr/packages/net.programmierecke.radiodroid2/) |Écoutez les stations de radio en ligne. |
| <img src="../../assets/img/documentviewer.svg" alt="Document viewer" width="50px"></img> | [Document viewer](https://f-droid.org/fr/packages/org.sufficientlysecure.viewer/) |Affichez différents formats de fichiers, notamment pdf, djvu, epub, xps et bandes dessinées (cbz, fb2). |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Libre Office viewer" width="50px"></img> | [Libre Office viewer](https://f-droid.org/fr/packages/org.documentfoundation.libreoffice/) |Affichez des fichiers du format docx, doc, xlsx, xls, pptx, ppt, odt, ods et odp. |
| <img src="../../assets/img/https.svg" alt="Keepass DX" width="50px"></img> | [Keepass DX](https://f-droid.org/fr/packages/com.kunzisoft.keepass.libre/) |Gestionnaire de mots de passe sécurisé et open source. |
| <img src="../../assets/img/letsencrypt.svg" alt="andOTP" width="50px"></img> | [andOTP](https://f-droid.org/fr/packages/org.shadowice.flocke.andotp/) |Appli libre et open-source pour l'authentification à deux facteurs. |
| <img src="../../assets/img/letsencrypt.svg" alt="Free OTP" width="50px"></img> | [Free OTP](https://f-droid.org/fr/packages/org.liberty.android.freeotpplus/) | Appli open-source pour l'authentification à deux facteurs. |
| <img src="../../assets/img/letsencrypt.svg" alt="Aegis" width="50px"></img> | [Aegis](https://f-droid.org/fr/packages/com.beemdevelopment.aegis/) |Appli libre et open-source pour l'authentification à deux facteurs. |
| <img src="../../assets/img/openvpn.svg" alt="Open VPN" width="50px"></img> | [Open VPN](https://f-droid.org/fr/packages/de.blinkt.openvpn/) |Si vous [hébergez votre propre serveur](https://gofoss.net/fr/intro-free-your-cloud/), cette appli peut être utilisée pour établir une connexion VPN. |
| <img src="../../assets/img/openvpn.svg" alt="Proton VPN" width="50px"></img> | [Proton VPN](https://f-droid.org/fr/packages/ch.protonvpn.android/) |VPN sécurisé et (partiellement) gratuit. Déclare ne pas enregistrer l'activité de ses utilisatrices et utilisateurs. Prend en charge le cryptage, la protection contre les fuites DNS, le « kill switch » etc. Soumis aux lois suisses sur la protection de la vie privée. |
| <img src="../../assets/img/simplefilemanager.svg" alt="Simple file manager" width="50px"></img> | [Simple file manager](https://f-droid.org/en/packages/com.simplemobiletools.filemanager.pro/) |Parcourez et modifiez vos fichiers. Open source, pas de publicité, couleurs personnalisables. |
| <img src="../../assets/img/simplefilemanager.svg" alt="DroidFS" width="50px"></img> | [DroidFS](https://f-droid.org/fr/packages/sushi.hardcore.droidfs/) |Stockez et accédez à vos fichiers en toute sécurité. Outil open source pour stocker des fichiers dans des volumes cryptés. |
| <img src="../../assets/img/seafile.svg" alt="Seafile" width="50px"></img> | [Seafile](https://f-droid.org/fr/packages/com.seafile.seadroid2/) |Si vous [hébergez votre propre serveur](https://gofoss.net/fr/intro-free-your-cloud/), cette appli peut être utilisée pour [gérer et synchroniser les fichiers en nuage](https://gofoss.net/fr/cloud-storage/). |
| <img src="../../assets/img/anysoft.svg" alt="Anysoft keyboard" width="50px"></img> | [Anysoft keyboard](https://f-droid.org/fr/packages/com.menny.android.anysoftkeyboard/) |Clavier open source. Prend en charge plusieurs langues, la saisie vocale, les commandes gestuelle, un mode nuit et des thèmes. Respecte votre vie privée. |
| <img src="../../assets/img/anysoft.svg" alt="Simple keyboard" width="50px"></img> | [Simple keyboard](https://f-droid.org/fr/packages/rkr.simplekeyboard.inputmethod/) |Clavier open source. |
| <img src="../../assets/img/anysoft.svg" alt="Open board" width="50px"></img> | [Open board](https://f-droid.org/fr/packages/org.dslul.openboard.inputmethod.latin/) |Clavier open source. |
| <img src="../../assets/img/anysoft.svg" alt="Hacker's keyboard" width="50px"></img> | [Hacker's keyboard](https://f-droid.org/fr/packages/org.pocketworkstation.pckeyboard/) |Clavier open source avec des touches numériques distinctes, la ponctuation aux endroits habituels et des touches fléchées. |
| <img src="../../assets/img/totem.svg" alt="Newpipe" width="50px"></img> | [Newpipe](https://f-droid.org/fr/packages/org.schabi.newpipe/) | Appli YouTube légère, sans API propriétaire ou services Google Play. Prend également en charge PeerTube. |
| <img src="../../assets/img/totem.svg" alt="Thorium" width="50px"></img> | [Thorium](https://f-droid.org/fr/packages/net.schueller.peertube/) |PeerTube est un réseau d'hébergement vidéo décentralisé, basé sur des logiciels libres. |
| <img src="../../assets/img/gimp.svg" alt="Quick dic" width="50px"></img> | [Quick dic](https://f-droid.org/fr/packages/de.reimardoeffinger.quickdic/) |Dictionnaire hors ligne. |
| <img src="../../assets/img/exodus.svg" alt="Exodus" width="50px"></img> | [Exodus](https://f-droid.org/fr/packages/org.eu.exodus_privacy.exodusprivacy/) |Découvrez quels traqueurs et quelles autorisations sont intégrés dans vos applis mobiles. |
| <img src="../../assets/img/lawnchair.svg" alt="Zim launcher" width="50px"></img> | [Zim launcher](https://f-droid.org/fr/packages/org.zimmob.zimlx/) |Appli de lancement libre et open source. Sans publicité. |

</center>


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Aurora Store" width="150px"></img> </center>

## Applis respectueuses de la vie privée (Aurora)

<center>

| Applis | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/firefox.svg" alt="Firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/fr/firefox/browsers/mobile/) | Navigateur rapide, sécurisé et privé. *Attention* : [3 traqueurs (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/fr/reports/org.mozilla.firefox/latest/). |
| <img src="../../assets/img/tor.svg" alt="Tor browser" width="50px"></img> | [Tor browser](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=fr/) |Navigateur Tor pour Android. Bloque les traqueurs, se défend contre la surveillance, résiste aux empreintes digitales, crypte le trafic internet. *Attention* : [3 traqueurs (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/fr/reports/org.torproject.torbrowser/latest/). |
| <img src="../../assets/img/protonmail.svg" alt="Protonmail" width="50px"></img> | [Protonmail](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=fr/) |Le plus grand service de courrier électronique sécurisé au monde, développé par des scientifiques du CERN et du MIT. Open source et protégé par la loi suisse sur la vie privée. Pas de traqueurs. |
| <img src="../../assets/img/signal.svg" alt="Signal" width="50px"></img> | [Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=fr/) |Messagerie rapide, simple et sécurisée. Texte, voix, vidéos, documents et images sont cryptés de bout en bout. Open source, sans publicité. Pas de traqueurs. *Attention* : accède à votre numéro de téléphone. |
| <img src="../../assets/img/lawnchair.svg" alt="Lawnchair" width="50px"></img> | [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah&hl=fr) |Lanceur libre et open source. Personnalisez les icônes, les étiquettes, le nombre de rangées et de colonnes, les notifications, etc. Pas de traqueurs. |
| <img src="../../assets/img/alarm.svg" alt="Simple Alarm clock" width="50px"></img> | [Simple Alarm clock](https://play.google.com/store/apps/details?id=com.better.alarm&hl=fr) |Réveil open source doté de fonctionnalités performantes et d'une interface épurée. |

</center>


<br>

<center> <img src="../../assets/img/separator_unzip.svg" alt="Applis FOSS" width="150px"></img> </center>

## Autres applis FOSS

<center>

| Applis FOSS | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/fossbrowser.svg" alt="Fennec" width="50px"></img> | [Fennec F-Droid](https://f-droid.org/fr/packages/org.mozilla.fennec_fdroid/) |Version de Firefox axée sur la confidentialité. Se concentre sur la suppression de tous les éléments propriétaires trouvés dans les versions officielles de Mozilla. *Attention* : [2 traqueurs (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/fr/reports/org.mozilla.fennec_fdroid/latest/). |
| <img src="../../assets/img/firefox.svg" alt="Mull browser" width="50px"></img> | [Mull](https://f-droid.org/fr/packages/us.spotco.fennec_dos/) | Dérivé sécurisé de Firefox pour Android, développé par l'équipe DivestOS et basé sur les projets Tor Uplift et arkenfox-user.js. *Attention* : [2 traqueurs (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/fr/reports/us.spotco.fennec_dos/latest/).|
| <img src="../../assets/img/fossbrowser.svg" alt="Bromite" width="50px"></img> | [Bromite](https://www.bromite.org/) |Navigateur Chromium, incluant blocage des publicités et confidentialité renforcée. Non disponible sur F-Droid. |
| <img src="../../assets/img/alarm.svg" alt="Insane alarm" width="50px"></img> | [Insane alarm](https://github.com/RIAEvangelist/insane-alarm/releases/) |Alarme open source, qui vous réveillera quoi qu'il arrive ! Non disponible sur F-Droid. |
| <img src="../../assets/img/tasks.svg" alt="Tasks.org" width="50px"></img> | [Tasks.org](https://f-droid.org/fr/packages/org.tasks/) |Gestionnaire de tâches open source. Si vous [hébergez votre propre serveur](https://gofoss.net/fr/intro-free-your-cloud/), cette appli peut être utilisée pour [gérer et synchroniser vos tâches](https://gofoss.net/fr/contacts-calendars-tasks/). *Attention* : [2 traqueurs (Google CrashLytics, Google Analytics).](https://reports.exodus-privacy.eu.org/fr/reports/org.tasks/latest/) |
| <img src="../../assets/img/editor.svg" alt="Standard notes" width="50px"></img> | [Standard notes](https://f-droid.org/fr/packages/com.standardnotes/) |Appli de prise de notes libre, open source et entièrement cryptée. *Attention*: [1 traqueur (bugsnag)](https://reports.exodus-privacy.eu.org/fr/reports/com.standardnotes/latest/) |
| <img src="../../assets/img/maps.svg" alt="Magic earth" width="50px"></img> | [Magic earth](https://play.google.com/store/apps/details?id=com.generalmagic.magicearth&hl=fr) |Utilise les données OpenStreetMap, mais l'appli en elle-même n'est pas vraiment open source. Non disponible sur F-Droid. Déclare ne pas espionner ses utilisateurs. Prend en charge la fonction « Dashcam », la navigation, les informations sur le trafic, les horaires des transports publics et la météo. |
| <img src="../../assets/img/doodle.svg" alt="Transportr" width="50px"></img> | [Transportr](https://f-droid.org/fr/packages/de.grobox.liberario/) |Appli open source pour les horaires des transports publics en Europe et outre-mer. *Attention*: [1 traqueur (mapbox)](https://reports.exodus-privacy.eu.org/fr/reports/de.grobox.liberario/latest/)|
| <img src="../../assets/img/totem.svg" alt="Free tube" width="50px"></img> | [Free Tube](https://github.com/FreeTubeApp/FreeTube/) |Appli Youtube open source pour protéger votre vie privée. Non disponible sur F-Droid. |
| <img src="../../assets/img/aurora.svg" alt="Fossdroid" width="50px"></img> | [Fossdroid](https://fossdroid.com/) |Magasin d'applis similaire à F-Droid, promouvant les applis Android libres et open source. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance" width="150px"></img> </center>

## Assistance

Si vous avez des questions ou désirez davantage de détails, consultez:

* la [documentation de F-Droid](https://f-droid.org/fr/docs/)
* la [communauté F-Droid](https://forum.f-droid.org/)
* la [communauté Reddit](https://teddit.net/r/fossdroid/)


<br>
