---
template: main.html
title: How FOSS protects your privacy to safely browse the Internet
description: How to avoid privacy breaches ? How to configure privacy and security settings ? How to use encryption ?
---

# What's in it for you?

<center><img src="../../assets/img/foss_ecosystem.png" alt="Free and open source software" width="500px"></img></center>

There is no one-click solution, switching to free and open source software is a process. This guide lays out some steps which will benefit your digital freedom:

<center>

| Benefits | Steps |
| :------: | ------ |
|[Safely browse the Internet](https://gofoss.net/intro-browse-freely/) | Switch to Firefox. Block trackers, cookies and ads. Use privacy-respecting search engines. Possibly encrypt your traffic with Tor or VPN. |
|[Keep your conversations private](https://gofoss.net/intro-speak-freely/) |Use end-to-end encryption to secure your emails, messages and calls. |
|[Protect your data](https://gofoss.net/intro-store-safely/) |Use long and unique passphrases. Choose a different one for each of your accounts and devices. Keep them safe in an encrypted password manager. Consider using two-factor authentication. Create a regular backup routine. And encrypt sensitive data. |
|[Unlock your computer's potential](https://gofoss.net/intro-free-your-computer/) |Switch to GNU/Linux and favor free and open source apps. Depending on your needs, choose a beginner friendly distribution like Linux Mint or Ubuntu. For more experienced users, pick Debian, Manjaro, openSUSE, Fedora or Gentoo Linux. And for privacy buffs, have a look at Qubes OS, Whonix or Tails. |
|[Stay mobile and free](https://gofoss.net/intro-free-your-phone/) |Switch to a custom mobile operating system like LineageOS, CalyxOS, GrapheneOS or /e/. Favor tracker-free open source apps from community maintained app stores. |
|[Own your cloud](https://gofoss.net/intro-free-your-cloud/) |Your files, pictures, videos, contacts, calendars, tasks belong to you, not Big Tech. Ditch commercial social media and discover the Fediverse, a federated family of various free and open source online services. Choose privacy-respecting cloud providers. Or set up your own secure server and self host services. |
|[Learn, connect & get involved](https://gofoss.net/nothing-to-hide/) |There is much to be said and learned about online privacy, data exploitation, filter bubbles, surveillance, censorship and planned obsolescence. Learn more about the project, get involved and spread the word. |

</center>

This guide allows you to take a look under the hood and adopt, reject or customise any software. We want you to feel comfortable, stay in control and understand what software you are using. Some key principles we apply:

<center>

| Principle | Description |
| :------: | ------ |
| #1 |Take one step at a time. |
| #2 |Start with the basics before diving into more advanced stuff. |
| #3 |Illustrate each step with concrete, hands-on examples. |
| #4 |Favour free and open source software. |
| #5 |Pick quality software which excels at one specific job over all-in-one bloatware. |
| #6 |Provide customisable solutions which leave the user in control. |
| #7 |Stay close to the source code, and away from one-click deployments or containerised solutions. |

</center>

<br>