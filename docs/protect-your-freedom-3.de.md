---
template: main.html
title: FOSS schützt Eure Daten
description: Diese Webseite ist frei und quelloffen. Keine Werbung, kein Ausspionieren, keine gesponsorten oder kostenpflichtigen Inhalte, keine Affiliates.
---

# Ist dies der richtige Leitfaden für mich?

Diese Webseite ist für alle gedacht. Sie soll Spaß machen und dabei lehrreich, personalisierbar und zugänglich sein. Der gesamte Inhalt ist 100% frei und quelloffen – keine Werbung, kein Tracking, keine gesponserten Inhalte. Jedes Kapitel weist darauf hin, welche Zielgruppe angesprochen wird:

<center>

| Publikum | Beschreibung |
| :------: | ------ |
|Normale BenutzerInnen <br>(alias Anfänger)<br><img src="../../assets/img/users.png" alt="Anfänger" width="30px"></img> |Diese Kapitel richten sich an BenutzerInnen, die keine besonderen technische Kenntnisse, oder einfach wenig Zeit und Lust zum Herumbasteln haben. |
|Fortgeschrittene BenutzerInnen <br>(alias BastlerInnen)<br><img src="../../assets/img/geeks.png" alt="BastlerInnen" width="30px"></img> |Diese Kapitel richten sich an BenutzerInnen, die bereits über einige technische Kenntnisse verfügen und mehr über Datenschutz und Technologie erfahren möchten. |
|ExpertInnen <br>(alias Technikbegeisterte)<br><img src="../../assets/img/techies.png" alt="Technikbegeisterte" width="75px"></img> |Diese Kapitel richten sich an BenutzerInnen, die mit den Tricks und Kniffen der Technik vertraut sind. |

</center>

<br>