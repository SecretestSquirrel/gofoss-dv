---
template: main.html
title: So verschlüsselt Ihr Eure E-Mails
description: Datenschutzfreundliche E-Mail-Anbieter. Ist Protonmail sicherer als Gmail? Protonmail vs. Gmail? Tutanota vs. Protonmail? Ist Tutanota sicher?
---


# Verschlüsselt Eure E-Mails

!!! level "Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/https5.png" alt="E-Mail-Verschlüsselung" width="250px"></img>
</div>

Wählt einen vertrauenswürdigen E-Mail-Anbieter. Prüft sorgfältig welche Funktionen verfügbar sind, welche Verschlüsselungstechnologien genutzt werden oder an welchen Standorten sich die Server befinden, denn Datenschutzgesetze ändern sich von Land zu Land. Dieses Kapitel bietet einen kurzen Überblick über beliebte, datenschutzfreundliche E-Mail-Anbieter.


<br>


<center> <img src="../../assets/img/separator_protonmail.svg" alt="Protonmail" width="150px"></img> </center>

## Protonmail

[Protonmail](https://protonmail.com/) gibt an, der weltweit größte sichere E-Mail-Dienst zu sein, der darüber hinaus durch Schweizer Datenschutzgesetze abgesichert ist. Protonmail wird unter anderem von US-Investoren (Charles River Ventures) und der Europäischen Union finanziert. Der Quellcode ist [hier verfügbar](https://github.com/ProtonMail/).

Zum Zeitpunkt der Abfassung dieses Textes umfasst das Einzelbenutzerkonto 500 MB kostenlosen Speicherplatz. Für 4 bis 24 EUR/Monat erhaltet Ihr Zugang zu weiteren BenutzerInnen und Speicherplatz sowie einer Fülle von Funktionen: Kalender, Kontakt- und E-Mail-Import, Bitcoin-Zahlungen, VPN und mehr.

??? warning "Einige Anmerkungen zur Verschlüsselung"

    <center>

    | Emails | Verschlüsselung |
    | ------ | ------ |
    | **Email-Versand zwischen Protonmail-NutzerInnen** |Nachrichtentext und Anhänge sind durchgängig verschlüsselt. Betreffzeilen und Empfänger-/Absenderadressen sind hingegen unverschlüsselt. |
    | **Email-Versand von Protonmail-NutzerInnen an andere Anbieter** |Nachrichtentext und Anhänge werden nur dann durchgängig verschlüsselt, wenn der Protonmail-Nutzer die Option `Für Außenstehende verschlüsseln` auswählt. Andernfalls wird nur TLS-Verschlüsselung angewendet, soweit der empfangende Mailserver dies unterstützt (dies bedeutet auch, dass der empfangende Anbieter die Nachricht mitlesen kann). In jedem Fall bleiben Betreffzeilen und Empfänger-/Absenderadressen unverschlüsselt.|
    | **Email-Empfang durch Protonmail-NutzerInnen von anderen Anbietern** |Nachrichtentext und Anhänge werden nur mit TLS verschlüsselt, wenn der Mailserver des Absenders dies unterstützt. Betreffzeilen und Empfänger-/Absenderadressen bleiben unverschlüsselt. |

    </center>


### Protonmail Clients

Neben dem [Webmail-Zugang](https://account.protonmail.com/login/) bietet Protonmail mobile Apps für Android und iOS an. In Desktop-Umgebungen unterstützt Protonmail [Thunderbird](https://www.thunderbird.net/de/), über eine sogenannte Bridge-Anwendung. Diese Funktion ist jedoch nur für kostenpflichtige Abonnements verfügbar. Alternativ dazu ist [ElectronMail](https://github.com/vladimiry/ElectronMail) ein freier und quelloffener Desktop-Client für Protonmail. Beachtet jedoch, dass ElectronMail eine inoffizielle Anwendung ist. Untenstehend findet Ihr eine ausführliche Anleitung

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

         Ladet einfach die Protonmail-App von [Googles Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=de&gl=DE) oder [Aurora Store](https://auroraoss.com/de/) herunter. Die App [enthält 0 Tracker und benötigt 14 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/ch.protonmail.android/latest/). Zum Vergleich: für Gmail sind es 1 Tracker und 55 Berechtigungen, für Outlook sind es 13 Tracker und 49 Berechtigungen und für Hotmail sind es 4 Tracker und 31 Berechtigungen.


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

         Ladet einfach die Protonmail-App vom [App Store](https://apps.apple.com/de/app/protonmail-encrypted-email/id979659905/) herunter.


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für ElectronMail in Windows (kein kostenpflichtiges Abonnement erforderlich)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | ElectronMail herunterladen |Ladet das [ElectronMail-Installationsprogramm für Windows](https://github.com/vladimiry/ElectronMail/releases) herunter und führt es aus. |
        | Master-Kennwort erstellen |Öffnet ElectronMail und gebt ein [sicheres, individuelles Master-Kennwort](https://gofoss.net/de/passwords/) ein, um Eure E-Mails zu schützen. |
        | Anmelden |Meldet Euch mit Eurem Protonmail-Konto an. Falls aktiviert, führt ebenfalls die Zwei-Faktor-Authentifizierung durch. |
        | Domäne |Wählt eine Domäne aus der Liste. Es gibt sogar eine `Onion`-Option, mit der Ihr Tor verwenden könnt. Klickt anschließend auf `Schließen`. |

        </center>


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Thunderbird in Windows (kostenpflichtiges Abonnement erforderlich)"

        ### Thunderbird in Windows installieren

        Ruft die [Thunderbird-Download-Seite](https://www.thunderbird.net/de/) auf und klickt auf die Schaltfläche `Kostenloser Download`. Sobald das Installationsprogramm heruntergeladen ist, klickt auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten.

        ### Protonmail Bridge in Windows installieren

        Thunderbird lässt sich gut mit Protonmail integrieren und stellt sicher, dass eingehende und versandte E-Mails verschlüsselt bleiben. Dies wird von der sogenannten Bridge-Anwendung gehandhabt, die nur für zahlende NutzerInnen verfügbar ist. Ladet hierzu [Protonmail Bridge für Windows](https://protonmail.com/bridge/download) herunter. Sobald das Installationsprogramm heruntergeladen ist, klickt auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten.

        ### Protonmail Bridge in Windows konfigurieren

        Öffnet die soeben installierte Protonmail Bridge-Anwendung und folgt dem Einrichtungsassistenten:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Meldet Euch mit Eurem Protonmail-Konto an. |
        | 2 | Klickt auf Euren Kontonamen und dann auf die Schaltfläche `Mailbox Configuration`. |
        | 3 | Ein Fenster mit dem Titel `Protonmail Bridge Mailbox Configuration` sollte sich einblenden. Hier werden die IMAP- und SMTP-Einstellungen angezeigt, einschließlich eines Passworts, das später für die Konfiguration von Thunderbird benötigt wird. |

        </center>

        ### Thunderbird in Windows konfigurieren

        Startet nun Thunderbird, ruft den Menüeintrag `Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Einrichtungsassistenten:

        <center>

        | Einstellung | Beschreibung |
        | ------ | ------ |
        | Euer Name | Gebt den Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure Protonmail E-Mail-Adresse ein. |
        | Passwort | Kopiert das Passwort aus dem Fenster `Protonmail Bridge Mailbox Configuration` und fügt es hier ein (*Gebt nicht Euer Protonmail-Passwort ein*, das wird nicht funktionieren). |
        | Passwort speichern | Setzt ein Häkchen bei `Kennwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Manuell einrichten | Klickt auf die Schaltfläche `Manuell einrichten`, und fügt die IMAP- und SMTP-Einstellungen ein, die im Fenster `Protonmail Bridge Mailbox Configuration` angezeigt werden (wählt für die Authentifizierung `Normales Passwort`). |
        | Erneut testen | Klickt auf die Schaltfläche `Erneut testen`, um Eure Verbindungseinstellungen zu überprüfen. |
        | Erweiterte Einstellungen | Klickt auf die Schaltfläche `Erweiterte Einstellungen`. Ein neues Fenster wird aufgerufen. Klickt einfach auf die Schaltfläche `OK`, ändert keine Einstellungen in diesem Fenster. |
        | Sicherheitsausnahme hinzufügen | Klickt im Pop-up-Fenster auf die Schaltfläche `Sicherheitsausnahme hinzufügen'. Damit wird bestätigt, dass Euer Computer (`127.0.0.1`) die Bridge-Anwendung ausführen kann. Möglicherweise müsst Ihr eine zweite Sicherheitsausnahme bestätigen, sobald Ihr Eure erste E-Mail versenden möchtet. |

        </center>

    ??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo für Thunderbird (kostenpflichtiges Abonnement erforderlich)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Quelle: Protonmail. Anweisungen sollten in ähnlicher Weise für macOS oder Linux gelten.*

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für ElectronMail in macOS (kein kostenpflichtiges Abonnement erforderlich)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | ElectronMail herunterladen |Ladet das [ElectronMail Disk Image](https://github.com/vladimiry/ElectronMail/releases) herunter, öffnet es und zieht das ElectronMail-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das ElectronMail-Symbol in das Andockmenü. |
        | Master-Kennwort erstellen |Öffnet ElectronMail und gebt ein [sicheres, individuelles Master-Kennwort](https://gofoss.net/de/passwords/) ein, um Eure E-Mails zu schützen. |
        | Anmelden |Meldet Euch mit Eurem Protonmail-Konto an. Falls aktiviert, führt ebenfalls die Zwei-Faktor-Authentifizierung durch. |
        | Domäne |Wählt eine Domäne aus der Liste. Es gibt sogar eine `Onion`-Option, mit der Ihr Tor verwenden könnt. Klickt anschließend auf `Schließen`. |

        </center>


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Thunderbird in macOS (kostenpflichtiges Abonnement erforderlich)"

        ### Thunderbird in macOS installieren

        Ruft die [Thunderbird-Download-Seite](https://www.thunderbird.net/de/) auf und klickt auf die Schaltfläche `Kostenloser Download`. Sobald das Installationsprogramm heruntergeladen ist, sollte es sich von selbst öffnen und ein neues Laufwerk einbinden, das die Thunderbird-Anwendung enthält. Sollte dies nicht der Fall sein, öffnet die heruntergeladene Thunderbird `dmg`-Datei und verschiebt das erscheinende Thunderbird-Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Thunderbird-Symbol in das Andockmenü.

        ### Protonmail Bridge in macOS installieren

        Thunderbird lässt sich gut mit Protonmail integrieren und stellt sicher, dass eingehende und versandte E-Mails verschlüsselt bleiben. Dies wird von der sogenannten Bridge-Anwendung gehandhabt, die nur für zahlende NutzerInnen verfügbar ist. Ladet hierzu [Protonmail Bridge für macOS](https://protonmail.com/bridge/download) herunter. Sobald das Installationsprogramm heruntergeladen ist, sollte es sich von selbst öffnen und ein neues Laufwerk einbinden, das die Protonmail-Anwendung enthält. Sollte dies nicht der Fall sein, öffnet die heruntergeladene Protonmail-Bridge `dmg`-Datei und verschiebt das erscheinende Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Protonmail-Symbol in das Andockmenü.

        ### Protonmail Bridge in macOS konfigurieren

        Öffnet die soeben installierte Protonmail Bridge-Anwendung und folgt dem Einrichtungsassistenten:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Meldet Euch mit Eurem Protonmail-Konto an. |
        | 2 | Klickt auf Euren Kontonamen und dann auf die Schaltfläche `Mailbox Configuration`. |
        | 3 | Ein Fenster mit dem Titel `Protonmail Bridge Mailbox Configuration` sollte sich einblenden. Hier werden die IMAP- und SMTP-Einstellungen angezeigt, einschließlich eines Passworts, das später für die Konfiguration von Thunderbird benötigt wird. |

        </center>

        ### Thunderbird in macOS konfigurieren

        Startet nun Thunderbird, ruft den Menüeintrag `Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Einrichtungsassistenten:

        <center>

        | Einstellung | Beschreibung |
        | ------ | ------ |
        | Euer Name | Gebt den Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure Protonmail E-Mail-Adresse ein. |
        | Passwort | Kopiert das Passwort aus dem Fenster `Protonmail Bridge Mailbox Configuration` und fügt es hier ein (*Gebt nicht Euer Protonmail-Passwort ein*, das wird nicht funktionieren). |
        | Passwort speichern | Setzt ein Häkchen bei `Kennwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Manuell einrichten | Klickt auf die Schaltfläche `Manuell einrichten`, und fügt die IMAP- und SMTP-Einstellungen ein, die im Fenster `Protonmail Bridge Mailbox Configuration` angezeigt werden (wählt für die Authentifizierung `Normales Passwort`). |
        | Erneut testen | Klickt auf die Schaltfläche `Erneut testen`, um Eure Verbindungseinstellungen zu überprüfen. |
        | Erweiterte Einstellungen | Klickt auf die Schaltfläche `Erweiterte Einstellungen`. Ein neues Fenster wird aufgerufen. Klickt einfach auf die Schaltfläche `OK`, ändert keine Einstellungen in diesem Fenster. |
        | Sicherheitsausnahme hinzufügen | Klickt im nun erscheinenden Fenster auf die Schaltfläche `Sicherheitsausnahme hinzufügen'. Damit wird bestätigt, dass Euer Computer (`127.0.0.1`) die Bridge-Anwendung ausführen kann. Möglicherweise müsst Ihr eine zweite Sicherheitsausnahme bestätigen, sobald Ihr Eure erste E-Mail versenden möchtet. |

        </center>

    ??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo für Thunderbird (kostenpflichtiges Abonnement erforderlich)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Quelle: Protonmail. Anweisungen sollten in ähnlicher Weise für macOS oder Linux gelten.*

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für ElectronMail in Ubuntu Linux (kein kostenpflichtiges Abonnement erforderlich)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | ElectronMail herunterladen |Ladet das aktuelle [ElectronMail .deb-Paket](https://github.com/vladimiry/ElectronMail/releases) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `electron-mail-X-XX-X-linux-amd64.deb`. Für den Zweck dieses Tutorials nehmen wir an, dass die Datei in den Ordner `/home/gofoss/Downloads` heruntergeladen wurde. Passt den Dateipfad an Eure eigenen Einstellungen an. Öffnet nun das Terminal mit der Tastenkombination `STRG+ALT+T` oder klickt auf die Schaltfläche `Anwendungen` oben links und sucht nach `Terminal`. Führt schließlich die folgenden Befehle aus:<br><br> `cd /home/gofoss/Downloads` <br>`sudo dpkg -i electron-mail-X-XX-X-linux-amd64.deb`
        |
        | Master-Kennwort erstellen |Öffnet ElectronMail und gebt ein [sicheres, individuelles Master-Kennwort](https://gofoss.net/de/passwords/) ein, um Eure E-Mails zu schützen. |
        | Anmelden |Meldet Euch mit Eurem Protonmail-Konto an. Falls aktiviert, führt ebenfalls die Zwei-Faktor-Authentifizierung durch.  |
        | Domäne |Wählt eine Domäne aus der Liste. Es gibt sogar eine `Onion`-Option, mit der Ihr Tor verwenden könnt. Klickt anschließend auf `Schließen`. |

        </center>


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Thunderbird in Ubuntu Linux (kostenpflichtiges Abonnement erforderlich)"

        ### Thunderbird in Linux installieren

        Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt oben links auf die Schaltfläche `Aktivitäten` und sucht nach `Terminal`. Führt den folgenden Befehl aus, um Thunderbird zu installieren:

        ```bash
        sudo apt install thunderbird
        ```

        ### Protonmail Bridge in Linux installieren

        Thunderbird lässt sich gut mit Protonmail integrieren und stellt sicher, dass eingehende und versandte E-Mails verschlüsselt bleiben. Dies wird von der sogenannten Bridge-Anwendung gehandhabt, die nur für zahlende NutzerInnen verfügbar ist. Ladet hierzu [Protonmail Bridge für Linux](https://protonmail.com/bridge/download) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `Protonmail-Bridge_X.X.X-X_amd64.deb`. Nehmen wir an, diese Datei wurde in den Ordner `/home/gofoss/Downloads` heruntergeladen. Öffnet das Terminal mit dem Tastaturkürzel `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal`. Führt dann die folgenden Befehle aus (vergesst nicht, den Dateinamen sowie den Pfad des Download-Ordners entsprechend anzupassen):

        ```bash
        sudo apt install gdebi
        cd /home/gofoss/Downloads
        sudo gdebi protonmail-bridge_X.X.X-X_amd64.deb
        ```

        ### Protonmail Bridge in Linux konfigurieren

        Öffnet die Bridge-Anwendung mit dem Terminalbefehl `protonmail-bridge`, oder klickt auf die Schaltfläche `Aktivitäten` oben links, und sucht nach `ProtonMail Bridge`. Folgt anschließend dem Einrichtungsassistenten:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Meldet Euch mit Eurem Protonmail-Konto an. |
        | 2 | Klickt auf Euren Kontonamen und dann auf die Schaltfläche `Mailbox Configuration`. |
        | 3 | Ein Fenster mit dem Titel `Protonmail Bridge Mailbox Configuration` sollte sich einblenden. Hier werden die IMAP- und SMTP-Einstellungen angezeigt, einschließlich eines Passworts, das später für die Konfiguration von Thunderbird benötigt wird. |

        </center>

        ### Thunderbird in Linux konfigurieren

        Startet nun Thunderbird, ruft den Menüeintrag `Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Einrichtungsassistenten:

        <center>

        | Einstellung | Beschreibung |
        | ------ | ------ |
        | Euer Name | Gebt Euren Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure Protonmail E-Mail-Adresse ein. |
        | Passwort | Kopiert das Passwort aus dem Fenster `Protonmail Bridge Mailbox Configuration` und fügt es hier ein (*Gebt nicht Euer Protonmail-Passwort ein*, das wird nicht funktionieren). |
        | Passwort speichern | Setzt ein Häkchen bei `Kennwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Manuell einrichten | Klickt auf die Schaltfläche `Manuell einrichten`, und fügt die IMAP- und SMTP-Einstellungen ein, die im Fenster `Protonmail Bridge Mailbox Configuration` angezeigt werden (wählt für die Authentifizierung `Normales Passwort`). |
        | Erneut testen | Klickt auf die Schaltfläche `Erneut testen`, um Eure Verbindungseinstellungen zu überprüfen. |
        | Erweiterte Einstellungen | Klickt auf die Schaltfläche `Erweiterte Einstellungen`. Ein neues Fenster wird aufgerufen. Klickt einfach auf die Schaltfläche `OK`, ändert keine Einstellungen in diesem Fenster. |
        | Sicherheitsausnahme hinzufügen | Klickt im nun erscheinenden Fenster auf die Schaltfläche `Sicherheitsausnahme hinzufügen'. Damit wird bestätigt, dass Euer Computer (`127.0.0.1`) die Bridge-Anwendung ausführen kann. Möglicherweise müsst Ihr eine zweite Sicherheitsausnahme bestätigen, sobald Ihr Eure erste E-Mail versenden möchtet. |

        </center>


    ??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo für Thunderbird (kostenpflichtiges Abonnement erforderlich)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Quelle: Protonmail. Anweisungen sollten in ähnlicher Weise für macOS oder Linux gelten.*

        </center>


<br>


<center> <img src="../../assets/img/separator_tutanota.svg" alt="Tutanota" width="150px"></img> </center>

## Tutanota

[Tutanota](https://tutanota.com/de/) ist ein in Deutschland registrierter, Freemium-basierter und sicherer E-Mail-Dienst. Alle Daten sind durchgängig verschlüsselt und [quelloffen](https://github.com/tutao/tutanota/). Tutanota verwendet seinen eigenen Verschlüsselungsstandard und unterstützt nicht PGP.

Zum Zeitpunkt der Abfassung dieses Textes umfasst das Basiskonto 1 GB kostenlosen Speicherplatz. Für 1 bis 6 EUR/Monat erhaltet Ihr Zugang zu weiteren BenutzerInnen und Speicherplatz sowie einer Fülle von Funktionen: benutzerdefinierte Domains, unbegrenzte Suchfunktion, mehrere Kalender, Posteingangsregeln, *Whitelabel*, Kalenderfreigabe usw. E-Mail-Importe und anonyme Zahlung werden derzeit nicht unterstützt.


### Tutanota Clients

Neben dem [Webmail-Zugang](https://mail.tutanota.com/?r=/login/) bietet Tutanota mobile Apps für Android und iOS an. Für Desktop-Umgebungen hat Tutanota einen eigenen Client entwickelt. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

         Ladet einfach die Tutanota-App aus dem [Google Play Store](https://play.google.com/store/apps/details?id=de.tutao.tutanota&hl=de&gl=DE) oder [Aurora Store](https://auroraoss.com/de/) herunter. Tutanota ist auch auf [F-Droid](https://f-droid.org/de/packages/de.tutao.tutanota/) verfügbar. Alternativ könnt Ihr die [Download-Seite](https://tutanota.com/de/#download)) oder die [Github-Paketquelle](https://github.com/tutao/tutanota/releases/) von Tutanota besuchen, um die `.apk`-Datei herunterzuladen und zu installieren. Die App [enthält 0 Tracker und benötigt 9 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/de.tutao.tutanota/latest/). Zum Vergleich: für Gmail sind es 1 Tracker und 55 Berechtigungen; für Outlook sind es 13 Tracker und 49 Berechtigungen; und für Hotmail sind es 4 Tracker und 31 Berechtigungen.

=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

         Ladet einfach die Tutanota aus dem [App Store](https://apps.apple.com/de/app/tutanota/id922429609) herunter.


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        Ladet einfach [das Installationsprogramm](https://mail.tutanota.com/desktop/tutanota-desktop-win.exe) herunter, klickt dann auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten.


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        Ladet einfach [das Installationsprogramm](https://mail.tutanota.com/desktop/tutanota-desktop-mac.dmg) herunter, das sich von selbst öffnen und ein neues Laufwerk mit der Tutanota-Anwendung einbinden sollte. Falls dies nicht der Fall sein sollte, öffnet die heruntergeladene Tutanota `.dmg`-Datei und verschiebt das erscheinende Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Tutanota-Symbol in das Andockmenü.


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Ladet einfach [das Installationsprogramm](https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `tutanota-desktop-linux.AppImage`.. Nehmen wir an, diese Datei wurde in den Ordner `/home/gofoss/Downloads` heruntergeladen. Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal`. Führt dann die folgenden Befehle aus (vergesst nicht, den Dateinamen sowie den Pfad zum Download-Ordner entsprechend anzupassen):

        ```bash
        cd /home/gofoss/Downloads
        chmod +x tutanota-desktop-linux.AppImage
        ```

    ??? tip "Tipps um Tutanota im Ubuntu-Dock anzuzeigen"

        Es ist nicht ganz einfach, aber Tutanotas Launcher kann dem Ubuntu-Anwendungsmenü hinzugefügt und an das Dock angeheftet werden. Öffnet hierzu das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal`. Führt den folgenden Befehl aus:

        ```bash
        sudo gedit /usr/share/applications/tutanota.desktop
        ```

        Fügt den folgenden Inhalt in die neu erstellte Datei ein. Stellt sicher, dass der `Exec`-Pfad auf den Ordner verweist, der die heruntergeladene AppImage-Datei enthält:

        ```
        #!/usr/bin/env xdg-open
        [Desktop Entry]
        Version=1.0
        Type=Application
        Terminal=false
        Exec=/home/gofoss/Downloads/tutanota-desktop-linux.AppImage
        Name=Tutanota
        ```

        Macht die Datei ausführbar:

        ```bash
        sudo chmod +x /usr/share/applications/tutanota.desktop
        ```

        Meldet Euch von der Ubuntu-Sitzung ab und dann gleich wieder an. Ihr solltet nun in der Lage sein, Tutanota aus dem Anwendungsmenü zu starten und es an das Dock zu heften.


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Email providers" width="150px"></img> </center>

## Weitere Email-Anbieter

=== "Disroot"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[disroot.org](https://disroot.org/de/services/email/) |
    |Preisangaben | Das Basiskonto ist kostenlos (1 GB Speicherplatz); zusätzlichen Speicherplatz gibt's für 0,15 EUR pro GB pro Monat. |
    |Funktionalitäten |Disroot bietet Online-Dienste an, die auf den Prinzipien von Freiheit, Datenschutz, Gemeinschaft und Dezentralisierung basieren. Der Anbieter befindet sich in den Niederlanden. Bitcoin und Faircoin werden akzeptiert. Vollständige Festplatten- und E-Mail-Verschlüsselung. Mobile App. |
    |Schwachstellen | Kann potenziell Benutzerdaten entschlüsseln, da E-Mails Berichten zufolge im Klartext gespeichert werden. |


=== "Mailbox"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[mailbox.org](https://mailbox.org/de/) |
    |Preisangaben | 1 EUR/Monat, 2 GB Speicherplatz. |
    |Funktionalitäten |Deutscher, quelloffener E-Mail-Anbieter, dessen Server in Berlin stehen. Bietet Sicherheitsfunktionen wie Verschlüsselung im Ruhezustand, PGP, DANE, SPF und DKIM an. Bietet des weiteren Zwei-Faktor-Authentifizierung, Volltextsuche, Kalender, Adressbücher, Aufgabenlisten, CalDAV- und CardDAV-Synchronisation an. |
    |Schwachstellen | Keine eigene mobile App, Drittanbieter-Clients können genutzt werden. |


=== "Posteo"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[posteo.de](https://posteo.de/de/) |
    |Preisangaben | 1 EUR/Monat, 2 GB Speicherplatz. |
    |Funktionalitäten |Deutscher, quelloffener E-Mail-Anbieter, eigen-finanziert, Verschlüsselung im Ruhezustand, Zwei-Faktor-Authentifizierung, Kalender, Adressbücher, CalDAV- und CardDAV-Synchronisation. |
    |Schwachstellen | Kein Spam-Ordner, keine Test- oder Gratis-Version. |


=== "Kolab Now"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[kolabnow.com](https://kolabnow.com/) |
    |Preisangaben | 5 EUR/Monat, 2 GB Speicherplatz. |
    |Funktionalitäten |Schweizer, quelloffener E-Mail-Anbieter, Textsuche und Tagging, Filter, Adressbücher, Kalender, CalDAV- und CardDAV-Synchronisation. |
    |Schwachstellen | Keine durchgängige Verschlüsselung, keine Verschlüsselung im Ruhezustand. |


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="E-Mail Weiterleitung" width="150px"></img> </center>

## Übergangszeit

=== "E-Mails weiterleiten"

    Die Umstellung auf ein neues E-Mail-Konto kann einige Zeit in Anspruch nehmen. Vermutlich solltet Ihr Eure alten Konten noch eine Weile beibehalten, um sicherzustellen, dass Ihr nichts verpasst. Leitet einfach alle eingehenden Nachrichten an das neue Konto weiter. Weitere Anweisungen zur Weiterleitung von E-Mails findet Ihr auf den Dokumentationsseiten von [Gmail](https://support.google.com/mail/answer/10957?hl=de), [Outlook](https://support.microsoft.com/de-de/office/erstellen-beantworten-oder-weiterleiten-von-e-mail-nachrichten-in-outlook-im-web-ecafbc06-e812-4b9e-a7af-5074a9c7abd0?redirectsourcepath=%252fen-us%252farticle%252fforward-email-from-office-365-to-another-email-account-1ed4ee1e-74f8-4f53-a174-86b748ff6a0e), [iCloud](https://www.idownloadblog.com/2019/01/28/set-up-icloud-email-forwarding/), [Yahoo](https://www.wikihow.com/Forward-Yahoo-Mail), usw.


=== "Abonnements aktualisieren"

    Nutzt die Übergangszeit, um Eure alten E-Mail-Konten auf laufende Abonnements zu überprüfen und diesen Eure neue E-Mail-Adresse zu übermitteln!


=== "Kontakte informieren"

    Vergesst nicht, die neue E-Mail-Adresse Euren privaten und beruflichen Kontakten, Eurer Bank, Eurer Versicherung, dem Finanzamt und so weiter mitzuteilen. Möglicherweise solltet Ihr auch eine automatische Antwortnachricht auf Eurem alten Konto einrichten, um Eure Kontakte über die Adressänderung zu informieren.


=== "Alte E-Mail-Konten schließen"

    Mit der Zeit werden immer weniger E-Mails in Euren alten Posteingängen landen. Irgendwann werden diese inaktiv. Das ist der richtige Zeitpunkt, um Eure alten E-Mail-Konten zu schließen.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Protonmail & Tutanota Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten finden Ihr in:

* [Protonmails Hilfestellung](https://protonmail.com/support/) oder [Thunderbirds Handbuch](https://support.mozilla.org/de/products/thunderbird). Ihr könnt auch gerne die [Protonmail Gemeinschaft](https://teddit.net/r/ProtonMail) oder die [Thunderbird Gemeinschaft](https://support.mozilla.org/de/questions/new/thunderbird) um Hilfe bitten.

* [Tutanotas Handbuch](https://tutanota.com/howto/#install-desktop) oder auf Nachfrage bei der [Tutanota Gemeinschaft](https://teddit.net/r/tutanota).

<br>

<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/email_reply.png" width="450px" alt="Sichere E-Mails"></img>
</div>

<br>