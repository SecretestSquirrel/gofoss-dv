---
template: main.html
title: How to backup your server
description: Backup your server. Server backup software. What is rsnapshot? How to install rsnapshot? Rsnapshot Ubuntu. Rsnapshot restore.
---

# Schedule server backups with rsnapshot

!!! level "For advanced users. Solid tech skills required."

<center>
<img align="center" src="../../assets/img/server_2.png" alt="Server backups" width="600px"></img>
</center>


[rsnapshot](https://rsnapshot.org/) is a tool to schedule server backups. It's FOSS, reliable and saves disk space. This is due to the fact that rsnapshot performs incremental backups: after an initial full backup, only modified files are backed up.

<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="rsnapshot installation" width="150px"></img> </center>

## Installation

If something goes wrong, you want to be able to restore the server to a previous state. Simply install rsnapshot on your server by running the command `sudo apt install rsnapshot`.


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="rsnapshot configuration" width="150px"></img> </center>

## Configuration

After successfully installing rsnapshot, we're going to configure a number of settings such as the location of the backup directory, external dependencies, backup intervals, logging, which files to backup, and so on. More details below.

??? tip "Show me the step-by-step guide"

    ### Backup directory

    Open rsnapshot's configuration file:

    ```bash
    sudo vi /etc/rsnapshot.conf
    ```

    Add or modify the following lines to define where rsnapshot should store server backups. Replace `$PATH` with the path to the actual backup folder, which can be located on the server's hard drive, an external drive connected to the server or a remote location. Also set the flag `no_create_root` to avoid that rsnapshot automatically creates a backup folder:

    ```bash
    snapshot_root	$PATH
    no_create_root	1
    ```

    ### Dependencies

    Still in rsnapshot's configuration file `/etc/rsnapshot.conf`, remove the comments from the following lines to tell rsnapshot about external dependencies:

    ```bash
    cmd_rsync           /usr/bin/rsync
    cmd_du              /usr/bin/du
    cmd_rsnapshot_diff	/usr/bin/rsnapshot-diff
    ```

    To make sure rsnapshot is pointing towards the right binaries, run the following commands and double-check if all paths are correct:

    ```bash
    whereis cmd_rsync
    whereis cmd_du
    whereis_rsnapshot_diff
    ```

    ### Backup intervals

    Below settings in rsnapshot's configuration file `/etc/rsnapshot.conf` define the intervals at which backups are performed, as well as the number of backups to keep. Hourly, daily, weekly and monthly backups are supported. In this example, we'll keep the 3 last daily backups, 7 last weekly backups and 2 last monthly backups. Adjust according to your own backup strategy:

    ```bash
    retain	daily	3
    retain	weekly	6
    retain	monthly	2
    ```

    ### Logging

    You might want to enable logs for debugging purposes. Create a log file:

    ```bash
    sudo touch /var/log/rsnapshot.log
    ```

    Enable logging in rsnapshots configuration file `/etc/rsnapshot.conf`:

    ```bash
    verbose     5
    loglevel    5
    logfile     /var/log/rsnapshot.log
    ```


    ### Backup files

    Decide which server files to backup. You can specify one or several directories of your choice. You can also exclude certain sub-directories using the `exclude` function. Make sure to use one line for each directory, and to add a trailing `/` at the end of each directory path. In this example, we'll backup the entire root filesystem `/`, excluding the folders `tmp`, `proc`, `mnt` and `sys`:

    ```bash
    backup	/	.	exclude=tmp,exclude=proc,exclude=mnt,exclude=sys
    ```


    ### Test

    Verify the syntax of the configuration file (the terminal should prompt `Syntax OK`):

    ```bash
    sudo rsnapshot configtest
    ```

    Now test your hourly, daily, weekly or monthly backup strategies:

    ```bash
    sudo rsnapshot -t hourly
    sudo rsnapshot -t daily
    sudo rsnapshot -t weekly
    sudo rsnapshot -t monthly
    ```


<br>

<center> <img src="../../assets/img/separator_alarm.svg" alt="rsnapshot scheduled backups" width="150px"></img> </center>

## Scheduled backups

rsnapshot allows to automatically schedule backups at certain intervals. More details below.

??? tip "Show me the step-by-step guide"

    Run the shortest backup strategy to create a first full snapshot. In our example, that's the daily backup (adjust accordingly):

    ```bash
    sudo rsnapshot daily
    ```

    Open the `cron` configuration file to schedule backups at certain intervals:

    ```bash
    sudo vi /etc/cron.d/rsnapshot
    ```

    In this example, we'll schedule daily snapshots at 1:00am, weekly snapshots every Monday at 6:30pm and monthly snapshots on the 2nd day of every month at 7:00am. Define your own schedules as needed, the [crontab guru](https://crontab.guru/) can be of help:

    ```bash
    #0 */4 * * *    root    /usr/bin/rsnapshot hourly
    0 1  * * *      root    /usr/bin/rsnapshot daily
    30 18 * * 1     root    /usr/bin/rsnapshot weekly
    0 7 2 * *       root    /usr/bin/rsnapshot monthly
    ```


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="rsnapshot restoration" width="150px"></img> </center>

## Restoration

If something goes wrong, you might still be able to restore your server to a previous state by restoring one of rsnapshot's backups. More details below.

??? tip "Show me the step-by-step guide"

    The command below lists all existing snapshots, replace `$PATH` with the path to the backup folder defined earlier:

    ```bash
    ls -l $PATH
    ```

    Depending on your backup strategy, the output might look something like this:

    ```bash
    /path/to/rsnapshot/backup/folder/daily.0
    /path/to/rsnapshot/backup/folder/daily.1
    /path/to/rsnapshot/backup/folder/daily.2
    /path/to/rsnapshot/backup/folder/weekly.0
    /path/to/rsnapshot/backup/folder/weekly.1
    /path/to/rsnapshot/backup/folder/weekly.2
    /path/to/rsnapshot/backup/folder/weekly.3
    /path/to/rsnapshot/backup/folder/weekly.4
    /path/to/rsnapshot/backup/folder/weekly.5
    /path/to/rsnapshot/backup/folder/monthly.0
    /path/to/rsnapshot/backup/folder/monthly.1
    ```

    The following command restores the server to a previous state:

    ```bash
    sudo rsync -avr $SOURCE_PATH $DESTINATION_PATH
    ```

    Make sure to provide the correct `$SOURCE_PATH` (= the backup directory which is about to be restored) and `$DESTINATION_PATH` (= the target directory on the server to which the backup needs to be restored). For example, the following command will restore the weekly snapshot of the entire file system from 3 weeks ago to the root `/` of the server:

    ```bash
    sudo rsync -avr /path/to/rsnapshot/backup/folder/weekly.3/ /
    ```

!!! warning "Caution! Here be dragons."

    Exercise extreme caution when performing system restoration. It can irrevocably overwrite data and lead to data losses.

<br>