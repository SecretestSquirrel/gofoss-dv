---
template: main.html
title: How FOSS protects your privacy
description: This guide is free and open source. No ads, no tracking, no sponsors, no affiliates, no paywall.
---

# Is this the right guide for you?

This guide is for pretty much anyone. It's meant to be fun, educational, customisable and accessible. And it's 100% free and open source – no ads, no tracking, no sponsored content. On each page, we'll point out which audience is likely to be the most interested. Let's go FOSS!

<center>

| Audience | Description |
| :------: | ------ |
|Normal users <br>(aka Noobs)<br><img src="../../assets/img/users.png" alt="beginners" width="30px"></img> |These chapters are for users with no particular tech skills, limited time or no inclination to tinker. |
|Intermediate users <br>(aka Tinkerers)<br><img src="../../assets/img/geeks.png" alt="geeks" width="30px"></img> |These chapters are for users with some tech skills under the belt and keen to learn more about privacy and technology. |
|Advanced users <br>(aka Techies)<br><img src="../../assets/img/techies.png" alt="techies" width="75px"></img> |These chapters are for users which are familiar with the tricks of the trade and have solid tech skills. |

</center>

<br>