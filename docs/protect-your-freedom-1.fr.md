---
template: main.html
title: Comment les FOSS protègent votre vie privée du capitalisme de surveillance
description: Que sont les FOSS ? Avantages des logiciels libres et open source ? Qu'est-ce qu'un modèle de menace ? Différences entre la vie privée, l'anonymat et la sécurité ?
---

# Pourquoi s'inquiéter?

<div align="center">
<img src="../../assets/img/closed_ecosystem.png" alt="Jardin clos" width="500px"></img>
</div>

GoFOSS veut rendre accessible à tout.e.s les logiciels libres et open source respectant la vie privée. Décidez par vous-même si vous devez cela vaut le coup de poursuivre la lecture :

* Vous avez un iPhone, un appareil Android ou un ordinateur tournant avec Windows, macOS ou ChromeOS ?
* Vous naviguez sur le Web avec Chrome, Safari ou Edge?
* Vous allez sur les médias sociaux du type Facebook, Twitter, WhatsApp, Tiktok ou Snapchat?

Vous avez très probablement répondu par l'affirmative à au moins une de ces questions. Êtes-vous conscient que toutes les entreprises à l'origine de ces services font partie de ce que l'on appelle le [capitalisme de surveillance](https://fr.wikipedia.org/wiki/%C3%89conomie_de_la_surveillance) ? En deux mots, il s'agit d'un système économique centré autour de monopoles technologiques qui récoltent les données personnelles afin de maximiser les profits.

Vous n'y voyez pas d'inconvénient ? Et bien, le capitalisme de surveillance menace le cœur même de nos sociétés. Il donne lieu à une surveillance de masse, polarise le débat politique, interfère avec les processus électoraux, pousse à l'uniformisation de la pensée, facilite la censure et favorise l'obsolescence planifiée.

Pour affronter ces problèmes, il faut procéder à des changements complets de nos systèmes juridiques et nos conventions sociales. En attendant, vous pouvez déjà faire beaucoup pour vous approprier vos données, protéger votre vie privée et revendiquer votre droit à la réparation. Par exemple, commencez à utiliser des logiciels libres et open source, également appelés [FOSS](https://fsfe.org/freesoftware/freesoftware.fr.html). Vous pourrez ainsi naviguer sur Internet, discuter avec vos amis et vos proches, travailler en ligne sans être soumis à la collecte de données par les gouvernements et les entreprises qui enregistrent, monétisent ou censurent vos données. Ou obligé à racheter de nouveaux appareils tous les deux ans.

<br>