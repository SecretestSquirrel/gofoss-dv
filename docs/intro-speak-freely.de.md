---
template: main.html
title: So verschlüsselt Ihr Eure Nachrichten
description: Was ist Transportschichtverschlüsselung? Was ist eine durchgängige Verschlüsselung? Welche Verschlüsselungsalgorithmen gibt es? Was sind öffentliche und private Schlüssel?
---

# Führt vertrauliche Gespräche

Verschlüsselt Eure Nachrichten, um sie für andere unlesbar zu machen. So erfährt nur der Empfänger, was Ihr zu sagen habt.

Die **Transportschichtverschlüsselung (TLS)** stellt sicher, dass Eure Nachrichten während der Übertragung im Internet oder zwischen Mobilfunkmasten nicht entschlüsselt werden können. Zwischengeschaltete Dienste können jedoch auf Eure Daten zugreifen, während sie diese weiterleiten oder verarbeiten. Dazu gehören z.B. Messenger-Dienste, soziale Netzwerke, Suchmaschinen, Banken und so weiter.

<div align="center">
<img src="../../assets/img/tls.png" alt="Transportschichtverschlüsselung" width="550px"></img>
</div>

Die **durchgängige Verschlüsselung** schützt Eure Nachrichten während der gesamten Übertragung. Niemand außer dem Endempfänger kann sie entschlüsseln. Nicht einmal zwischengeschaltete Dienste, die die Daten weitergeben, wie z. B. ein durchgängig verschlüsselter Nachrichten-Dienst oder E-Mail-Anbieter.

<div align="center">
<img src="../../assets/img/e2ee.png" alt="Durchgängige Verschlüsselung" width="550px"></img>
</div>

??? tip "Hier erfahrt Ihr, wie Verschlüsselung helfen kann Überwachung zu vermeiden"

    <center>

    <iframe src="https://archive.org/embed/tips-to-help-avoid-online-surveillance" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Quelle: [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


## Verschlüsselte E-Mails

Wahrscheinlich ist Euch bereits bekannt, dass die meisten E-Mail-Anbieter:

* [private Nachrichten an Werbekunden verkaufen](https://www.wsj.com/articles/yahoo-bucking-industry-scans-emails-for-data-to-sell-advertisers-1535466959)
* [an Massenüberwachungsprogrammen teilnehmen](https://www.nytimes.com/2016/10/06/technology/yahoo-email-tech-companies-government-investigations.html)
* [schwerwiegende Datenpannen erleiden](https://en.wikipedia.org/wiki/Yahoo!_data_breaches)
* [das Verhalten ihrer Benutzer überwachen](https://thenextweb.com/news/google-tracks-nearly-all-your-online-and-some-of-your-offline-purchases-see-it-for-yourself-here)

Dennoch vertrauen die meisten NutzerInnen ihre E-Mails weiterhin etablierten Anbietern an. Hauptgrund? Ein Anbieterwechsel schreckt viele ab. Es wird oft argumentiert, dass die derzeitige E-Mail-Adresse doch "kostenlos", jedem bekannt und darüber hinaus an alle Konten und Abonnements gebunden sei. Dabei ist ein Anbieterwechsel eigentlich gar nicht so schwer. Dieses Kapitel beschreibt, wie Ihr Eure E-Mails schrittweise zu einem datenschutzfreundlichen Anbieter umsiedelt.

<center>
<html>
<embed src="../../assets/echarts/email_stats.html" style="width: 100%; height:420px">
</html>
</center>


## Verschlüsselte Nachrichten-Apps

In diesem Kapitel erfahrt Ihr ebenfalls, wie Ihr Eure Nachrichten und Anrufe verschlüsseln könnt. Heutzutage bieten mehrere mobile Apps eine durchgängige Verschlüsselung an, um Eure Kommunikation zu schützen. Dritte können Nachrichten, die an solche Apps gesendet oder von diesen empfangen werden, nicht mitlesen.

<br>
