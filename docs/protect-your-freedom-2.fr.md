---
template: main.html
title: Comment les FOSS protègent votre vie privée pour naviguer sur internet en toute sécurité
description: Comment éviter les atteintes à la vie privée ? Configurer les paramètres de confidentialité et de sécurité. Comment utiliser le cryptage ?

---

# Qu'est-ce que vous y gagnez ?

<center><img src="../../assets/img/foss_ecosystem.png" alt="Logiciel libre et open source" width="500px"></img></center>

Il n'existe pas de solution miracle. Le passage aux logiciels libres et open source est un processus. Ce guide présente quelques étapes qui contribueront à votre liberté numérique:

<center>

| Avantages | Étapes |
| :------: | ------ |
|[Naviguez sur internet en toute sécurité](https://gofoss.net/fr/intro-browse-freely/) | Passez à Firefox. Bloquez les traqueurs, les cookies et les publicités. Utilisez des moteurs de recherche respectueux de votre vie privée. Cryptez éventuellement votre trafic avec Tor ou VPN. |
|[Gardez vos conversations privées](https://gofoss.net/fr/intro-speak-freely/) |Cryptez vos courriels, messages et appels de bout en bout. |
|[Sécurisez vos données](https://gofoss.net/fr/intro-store-safely/) |Utilisez de mots de passe forts. Choisissez-en un différent pour chacun de vos comptes et appareils. Conservez-les en sécurité avec un gestionnaire de mots de passe chiffré. Envisagez d'activer l'authentification à deux facteurs. Créez une routine de sauvegarde régulière. Et cryptez vos données sensibles. |
|[Libérez le potentiel de vos ordinateurs](https://gofoss.net/fr/intro-free-your-computer/) |Passez à GNU/Linux et privilégiez les applications libres et open source. En fonction de vos besoins, choisissez une distribution adaptée aux débutant comme Linux Mint ou Ubuntu. Pour les utilisateurs plus expérimentés, choisissez Debian, Manjaro, openSUSE, Fedora ou Gentoo Linux. Et pour les amateurs de confidentialité, jetez un œil à Qubes OS, Whonix ou Tails. |
|[Restez mobiles et libres](https://gofoss.net/fr/intro-free-your-phone/) |Passez à un système d'exploitation mobile personnalisé comme LineageOS, CalyxOS, GrapheneOS ou /e/. Privilégiez les applications open source sans traqueur provenant de magasins d'applications gérés par la communauté. |
|[Appropriez-vous le nuage](https://gofoss.net/fr/intro-free-your-cloud/) |Vos fichiers, photos, vidéos, contacts, calendriers et tâches vous appartiennent, pas aux GAFAM. Abandonnez les médias sociaux commerciaux et découvrez le Fediverse, des services en ligne fédérés, libres et open source. Choisissez des fournisseurs de services en nuage respectueux de votre vie privée. Ou créez votre propre serveur sécurisé et des services d'auto-hébergement. |
|[Apprenez, réseautez et participez](https://gofoss.net/fr/nothing-to-hide/) |Il y a beaucoup à dire et à apprendre sur la vie privée en ligne, l'exploitation des données, les bulles de filtres, la surveillance en ligne, la censure et l'obsolescence planifiée. Apprenez-en davantage sur ce projet, impliquez-vous et faites passer le mot. |

</center>

Ce guide vous permet de jeter un coup d'œil dans les coulisses et d'adopter, de rejeter ou de personnaliser tout logiciel. Sentez-vous à l'aise, gardez le contrôle et comprenez ce que font les logiciels que vous utilisez. Voici quelques-uns des principes qui nous guident :

<center>

| Principe | Description |
| :------: | ------ |
| #1 | Franchir une étape à la fois. |
| #2 |Commencer avec les choses fondamentales avant de viser celles qui sont plus avancées. |
| #3 |Illustrer chaque étape avec des exemples concrets faciles à mettre en application. |
| #4 |Favoriser les logiciels libres et open source. |
| #5 |Choisir des logiciels de bonne qualité qui font très bien une seule chose plutôt que des **obésiciels** qui prétendent faire un tas de choses et le font mal. |
| #6 |Proposer des solutions personnalisables qui permettent à l'utilisateur de garder le contrôle. |
| #7 |Rester proche du code source et loin des solutions de déploiement en un clic ou en conteneurs. |

</center>

<br>