---
template: main.html
title: How FOSS protects your privacy from surveillance capitalism
description: What is FOSS? What's a threat model? And what's the difference between privacy, anonymity and security?
---

# Why should you bother?

<div align="center">
<img src="../../assets/img/closed_ecosystem.png" alt="Walled garden" width="500px"></img>
</div>

GoFOSS wants to make privacy respecting, free and open source software accessible to all. Decide for yourself if you should bother to continue reading:

* Do you own an iPhone, Android device or a computer running Windows, macOS or ChromeOS?
* Do you browse the web with Chrome, Safari or Edge?
* Do you socialise on Facebook, Twitter, WhatsApp, Tiktok or Snapchat?

Most likely you answered *yes* to at least one of those questions. Are you aware that all companies behind these services are part of the so-called [Surveillance Capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism)? In a nutshell, this term describes an economic system centred around technology monopolies which harvest private user data to maximize profit.

Don't see any issues with that? Well, Surveillance Capitalism actually threatens the very core of our societies. It gives rise to mass surveillance, polarizes the political debate, interferes with electoral processes, drives uniformity of thought, facilitates censorship and promotes planned obsolescence.

Tackling those issues requires comprehensive changes to our legal systems and social conventions. Meanwhile, there's a lot you can do to own your data, protect your privacy and claim your right to repair. For example, start using free and open source software, also referred to as [FOSS](https://fsfe.org/freesoftware/freesoftware.en.html). This empowers you to browse, chat and work online without anyone collecting, monetising or censoring your data. Or forcing you to buy new devices every other year.

<br>