---
template: main.html
title: So installiert Ihr Ubuntu
description: Was ist Ubuntu? Ist Ubuntu besser als Windows? Was bedeutet Ubuntu? Was ist Ubuntu LTS? Wie installiert man Ubuntu?
---

# Ubuntu, eine einsteigerfreundliche Linux-Version

!!! level "Für Beginner sowie erfahrenere BenutzerInnen. Grundlegende technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/ubuntu_desktop.png" alt="So installiert man Ubuntu" width="600px"></img>
</div>


## Entdeckt Ubuntu

[Ubuntu](https://ubuntu.com/) könnt Ihr am einfachsten entdecken, indem Ihr es neben Eurem aktuellen Betriebssystem ausführt, zum Beispiel mittels eines Live-USB-Laufwerks oder VirtualBox. Sollte Ubuntu Euch nicht gefallen, könnt Ihr ganz einfach wieder zu Windows oder macOS zurückkehren.

=== "Live-USB oder -DVD"

    Startet Ubuntu von einer Live-DVD oder einem USB-Laufwerk, um es schnell mal auszuprobieren. Mehr Details findet Ihr nachstehend.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Herunterladen |Ladet die aktuellste [Ubuntu Long-Term-Support (LTS)-Version](https://ubuntu.com/download/desktop) herunter. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-LTS-Version 20.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle).|
        | Brennen |Verwendet einen [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) oder [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) Rechner, um die heruntergeladene `.iso`-Datei auf eine DVD zu brennen. Alternativ könnt Ihr auch einen [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) oder [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) Rechner verwenden, um ein bootfähiges USB-Laufwerk zu erstellen. |
        | Neustarten |Legt die DVD oder das bootfähige USB-Laufwerk in den Rechner ein, auf dem Ihr Ubuntu testen möchtet, und startet den Rechner neu. Die meisten Rechner booten dann automatisch von der DVD oder dem USB-Laufwerk. Sollte dies nicht der Fall sein, drückt beim Hochfahren Eures Computers wiederholt auf die Tasten `F12`, `ESC`, `F2` oder `F10`. Dadurch solltet Ihr Zugang zum Boot-Menü Eures Rechners erhalten, in dem Ihr dann die DVD bzw. das USB-Laufwerk als Boot-Laufwerk auswählen könnt. |
        | Ubuntu testen |Sobald der Startbildschirm angezeigt wird, wählt den Eintrag `Ubuntu ausprobieren`. |

        </center>


=== "VirtualBox"

    [VirtualBox](https://www.virtualbox.org/) ist eine weitere Möglichkeit, Ubuntu zu entdecken ohne Euer bestehendes Betriebssystem zu beeinträchtigen. VirtualBox erstellt hierzu sogenannte virtuelle Maschinen, die auf Euren Windows- oder macOS-Rechnern laufen. Untenstehend findet Ihr eine ausführliche und einsteigerfreundliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        ### VirtualBox installieren

        === "Windows"

            Stellt erst einmal sicher, dass Euer Rechner die Mindestanforderungen erfüllt: 2 GHz Dual-Core-Prozessor, 2 GB RAM, 25 GB freier Speicherplatz. Ladet anschließend das aktuelle [VirtualBox Platform Package für Windows Hosts](https://www.virtualbox.org/wiki/Downloads) herunter. Öffnet schließlich die heruntergeladene `.exe`-Datei und folgt dem Installationsassistenten.

        === "macOS"

            Stellt erst einmal sicher, dass Euer Rechner die Mindestanforderungen erfüllt: 2 GHz Dual-Core-Prozessor, 2 GB RAM, 25 GB freier Speicherplatz. Ladet anschließend das aktuelle [VirtualBox Platform Package für OS X Hosts](https://www.virtualbox.org/wiki/Downloads) herunter. Öffnet die heruntergeladene `.dmg`-Datei und zieht das VirtualBox-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das VirtualBox-Symbol in das Andockmenü.

        ### VirtualBox konfigurieren

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Ubuntu herunterladen | Ladet die aktuellste [Ubuntu Long-Term-Support (LTS)-Version](https://ubuntu.com/download/desktop) herunter. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-LTS-Version 20.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle). |
        | Eine virtuelle Maschine erstellen | Startet VirtualBox und klickt auf die `Neu`-Schaltfläche. |
        | Name und Betriebssystem | Gebt Eurer virtuellen Maschine (VM) einen Namen, zum Beispiel `Ubuntu`. Wählt ebenfalls das Betriebssystem (`Linux`) sowie die Version aus, zum Beispiel `Ubuntu (64-bit)`. |
        | Speichergröße | Wählt aus, wie viel Arbeitsspeicher Ubuntu zugewiesen werden soll. 2 GB sind empfohlen, 3-4 GB wären noch besser. |
        | Festplatte |  Wählt den Eintrag `Festplatte erzeugen`, um eine virtuelle Festplatte hinzuzufügen. |
        | Dateityp der Festplatte | Wählt `VDI` als Dateityp für die Festplatte. |
        | Art der Speicherung | Wählt den Eintrag `dynamisch alloziert`, um die Größe der Festplatte festzulegen. |
        | Dateiname und Größe | Gebt an, wo die virtuelle Festplatte erstellt werden soll. Weist Ubuntu genügend Speicherplatz zu, mindestens 10 GB sind empfohlen. |
        | Wahl des optischen Mediums | Klickt vom Hauptbildschirm aus auf `Start`, um die virtuelle Ubuntu-Maschine zu starten. Klickt im erscheinenden Dialogfenster auf das Ordner-Symbol, um ein virtuelles optisches Medium auszuwählen. Ruft den Pfad auf, an dem sich die vorher heruntergeladene LTS Ubuntu `.iso`-Datei befindet. Klickt auf `Öffnen`, `Auswählen` und `Start`. Nach der Boot-Phase sollte der Ubuntu-Installationsassistent erscheinen. |

        </center>


        ### Ubuntu in VirtualBox installieren

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Willkommen |Wählt eine Sprache und klickt auf `Ubuntu installieren`. |
        |Tastaturbelegung |Wählt ein Tastaturlayout. |
        |Installation von Ubuntu wird vorbereitet |Wählt zwischen einer `normalen` oder `minimalen` Installation, je nachdem, wie viele Anwendungen Ihr von Anfang an installieren möchtet. Aktiviert optional das Kontrollkästchen `Während Ubuntu installiert wird Aktualisierungen herunterladen`, um die Einrichtung nach der Installation zu beschleunigen, sowie das Kontrollkästchen `Software von Drittanbietern installieren`, um von (proprietären) Treibern für die Grafik-Karte, das WiFi, Mediendateien usw. zu profitieren. Klickt dann auf `Weiter`. |
        |Installationsart & Verschlüsselung |Auf diesem Bildschirm könnt Ihr wählen, ob Ihr das vorhandene Betriebssystem löschen und durch Ubuntu ersetzen wollt, oder ob Ihr Ubuntu neben dem vorhandenen Betriebssystem installieren möchtet (sogenanntes "Dual-Booting"). <br><br> Nachdem wir Ubuntu in VirtualBox installieren, ist kein anderes Betriebssystem vorhanden. Wählt daher einfach `Festplatte löschen und Ubuntu installieren`, klickt auf `Fortgeschrittene Optionen` und wählt `LVM bei der neuen Ubuntu-Installation verwenden` sowie `Die neue Ubuntu-Installation verschlüsseln`. Klickt anschließend auf `OK` und `Jetzt installieren`. |
        |Wählt einen Sicherheitsschlüssel |Wählt einen [sicheren, individuellen Sicherheitsschlüssel](https://gofoss.net/de/passwords/). Dieser wird bei jedem Neustart Eures Computers zur Entschlüsselung der Festplatte benötigt. Aktiviert ebenfalls das Kontrollkästchen `Leeren Speicherplatz überschreiben`, das sorgt für noch mehr Sicherheit. Klickt dann auf `Jetzt installieren`. Klickt auf `Fortfahren`, um das Pop-up-Fenster zu schließen. <br><br> *Vorsicht*: Bei Verlust des Sicherheitsschlüssels gehen alle Daten verloren. Bewahrt ihn daher sicher auf! |
        |Wo befinden Sie sich? |Wählt eine Zeitzone und einen Ort. Klickt anschließend auf `Weiter`. |
        |Wer sind Sie? |Gebt Anmeldeinformationen an, z. B. einen Benutzernamen und ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/). Wählt `Passwort zum Anmelden abfragen` und klickt auf `Weiter`. |
        |Startet den Rechner neu & meldet Euch an |Klickt nach der erfolgreichen Installation auf `Jetzt neu starten`. Die virtuelle Maschine wird neu gestartet. Ihr könnt Euch nun bei Eurer ersten Ubuntu-Sitzung anmelden, indem Ihr den richtigen Sicherheitsschlüssel und das richtige Passwort eingebt. |
        |Ubuntu erforschen| Folgt den Einrichtungsoptionen und erforscht Ubuntu. Schließt die Sitzung, sobald Ihr fertig seid. Von nun an könnt Ihr Ubuntu starten, indem Ihr auf die `Start`-Schaltfläche auf dem Hauptbildschirm von VirtualBox klickt.|

        </center>


    ??? tip "Hier geht's zum 5-minütigen Zusammenfassungsvideo"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ef6ada2-116b-416d-8d53-10a817d95827" frameborder="0" allowfullscreen></iframe>
        </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Ubuntu Desktop" width="150px"></img> </center>

## Der Ubuntu-Desktop

Nachdem Ihr Euch in Eure erste Ubuntu-Sitzung eingeloggt habt, erscheint der sogenannte [GNOME-Desktop](https://www.gnome.org/):

<center>

| Bestandteile des Gnome-Desktops | Beschreibung |
| ------ | ------ |
| Obere Menüleiste | Zeigt Datum, Uhrzeit, Benachrichtigungen, Akkulaufzeit, Netzwerkverbindungen, Lautstärke usw. an. |
| Dock | Auf der linken Seite werden Verknüpfungen zu Euren Lieblingsanwendungen und den derzeit geöffneten Anwendungen angezeigt. Die Position des Docks kann angepasst werden. |
| App-Schublade | Befindet sich unten links im Dock und bietet Zugriff auf alle installierten Apps. |
| Übersichtsbildschirm | Über die `Aktivitäten`-Schaltfläche in der oberen Leiste erreichbar, zeigt alle offenen Fenster an. |
| Suchleiste | Über die `Aktivitäten`-Schaltfläche in der oberen Leiste erreichbar, ermöglicht es nach Anwendungen, Dateien, Einstellungen, Befehlen usw. zu suchen. |

</center>


<br>

<center> <img src="../../assets/img/separator_php.svg" alt="Ubuntu Terminal" width="150px"></img> </center>

## Das Terminal

Das Terminal ist eine Benutzeroberfläche zur Ausführung textbasierter Befehle. Viele neue BenutzerInnen schrecken vor dem Terminal zurück, da es oft mit obskurem Code-Hacking in Verbindung gebracht wird. In Wirklichkeit ist die Verwendung des Terminals gar nicht so schwer und kann deutlich schneller als die Nutzung einer graphischen Oberfläche sein. Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die `Aktivitäten`-Schaltfläche oben links und sucht nach `Terminal`.

??? info "Hier erfahrt Ihr mehr zum Terminal"

    Das Terminal wird manchmal auch Shell, Konsole, Befehlszeile oder Eingabeaufforderung genannt. Es dauert eine Weile, bis man sich daran gewöhnt hat. Aber hat man erst einmal ein paar Tricks gelernt, ist das Terminal äußerst praktisch. Wenn Ihr zum Beispiel `sudo` vor einen Befehl setzt, wird dieser mit Administratorrechten ausgeführt. Ähnlich wie die Sicherheitsmeldungen in Windows und macOS, die ein Administrator-Passwort verlangen, bevor ein Programm installiert oder eine Einstellung geändert wird. Hier noch etwas Lesestoff, um ein Terminal-Ninja zu werden:

    * [Spickzettel mit den gängigsten Terminalbefehlen](https://valise.chapril.org/s/Log4SeZmGb9xtKb)
    * [Ein weiterer Spickzettel mit den gängigsten Terminalbefehlen](https://valise.chapril.org/s/GqojwkXGrAHgESt)
    * [Und noch ein Spickzettel mit häufig genutzten Terminalbefehlen](https://valise.chapril.org/s/T3M6XbAMHPTXGWR)
    * [Ein paar nützliche Ubuntu-Terminalbefehle](https://valise.chapril.org/s/MHsRdpXznZSLFyg)
    * [Vollständiges Buch über die Linux-Kommandozeile](https://valise.chapril.org/s/QXXNcMj874tJreY)


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Ubuntu Installation" width="150px"></img> </center>

## Ubuntu installieren

Gefällt Euch Ubuntu? Möchtet ihr Ubuntu dauerhaft auf Eurem Rechner installieren? Prima! Führt einfach ein paar Vorabprüfungen durch und folgt den nachstehenden Installationsanweisungen.

??? tip "Ein paar Vorabprüfungen"

    <center>

    | Vorabprüfungen | Beschreibung |
    | ------ | ------ |
    | Ist mein Gerät Linux-kompatibel? |• [Testet Ubuntu](https://gofoss.net/de/ubuntu/) mit einem Live-USB-Laufwerk oder mit VirtualBox <br>• Überprüft die [Kompatibilitätsdatenbank](https://ubuntu.com/certified) <br>• [Fragt](https://search.disroot.org/) im Internet nach <br>• Kauft einen [Linux-kompatiblen Rechner](https://linuxpreloaded.com/) |
    | Erfüllt mein Gerät die Mindestanforderungen? |• 2 GHz Dual-Core-Prozessor <br>• 4 GB Arbeitsspeicher (RAM) <br>• 25 GB freier Festplattenspeicher (Ubuntu benötigt ca. 5 GB, seht mindestens 20 GB für Eure Daten vor) |
    | Ist mein Gerät eingestöpselt? | Falls Ihr Ubuntu auf einem mobilen Gerät wie z.B. einem Laptop installiert, sollte dieses an die Stromversorgung angeschlossen sein. |
    | Ist das Installationsmedium zugänglich? | Prüft, ob Euer Rechner über ein DVD-Laufwerk oder einen freien USB-Anschluss verfügt. |
    | Hat das Gerät Zugriff aufs Internet? | Prüft, ob die Internetverbindung funktioniert. |
    | Sind Eure Daten gesichert? | [Sichert Eure Daten](https://gofoss.net/de/backups/), da ein (geringes, aber reales) Risiko des Datenverlustes während des Installationsprozesses besteht! |
    | Ist die aktuelle Ubuntu Linux-Version heruntergeladen? | Ladet die aktuellste [Ubuntu Long-Term-Support (LTS)-Version](https://ubuntu.com/download/desktop) herunter. Diese wird 5 Jahre lang unterstützt, einschließlich Sicherheits- und Wartungsupdates. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-LTS-Version 20.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle). |
    | Ist ein bootfähiges Medium vorbereitet? | Verwendet einen [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) oder [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) Rechner, um die heruntergeladene `.iso`-Datei auf eine DVD zu brennen. Alternativ könnt Ihr auch einen [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) oder [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) Rechner verwenden, um ein bootfähiges USB-Laufwerk zu erstellen. |

    </center>


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Booten |Legt die DVD oder das bootfähige USB-Laufwerk ein und startet Euren Rechner neu. Die meisten Rechner booten automatisch von der DVD oder dem USB-Laufwerk. Sollte dies nicht der Fall sein, drückt beim Hochfahren Eures Computers wiederholt auf die Tasten `F12`, `ESC`, `F2` oder `F10`. Dadurch solltet Ihr Zugang zum Boot-Menü Eures Rechners erhalten, in dem Ihr dann die DVD bzw. das USB-Laufwerk auswählen könnt. |
    |Willkommen |Sobald der Rechner hochgefahren ist, wird der Installationsassistent angezeigt. Wählt eine Sprache und klickt auf `Ubuntu installieren`. |
    |Tastaturbelegung |Wählt ein Tastaturlayout. |
    |Installation von Ubuntu wird vorbereitet |Wählt zwischen einer `normalen` oder `minimalen` Installation, je nachdem, wie viele Anwendungen Ihr von Anfang an installieren möchtet. Optional: <br><br>• Aktiviert das Kontrollkästchen `Während Ubuntu installiert wird Aktualisierungen herunterladen`, um die Einrichtung nach der Installation zu beschleunigen <br>• Aktiviert das Kontrollkästchen `Software von Drittanbietern installieren`, um von (proprietären) Treibern für die Grafik-Karte, das WiFi, Mediendateien usw. zu profitieren <br><br> Klickt dann auf `Weiter` |
    |Installationsart & Verschlüsselung |Hier könnt Ihr wählen: <br>• Ob das vorhandene Betriebssystem gelöscht und durch Ubuntu ersetzt werden soll. *Vorsicht!*: dies löscht alle Daten auf Eurer Festplatte! Stellt daher sicher, dass Ihr [Eure Daten gesichert habt](https://gofoss.net/de/backups/)! <br>• Oder ob Ubuntu parallel zum vorhandenen Betriebssystem installiert werden soll (sogenanntes "Dual-Booting"). Dies sollte keine Auswirkungen auf die bestehende Konfiguration Eures Rechners haben. Stellt trotzdem sicher, dass Ihr [Eure Daten sichert](https://gofoss.net/de/backups/), man weiß ja nie... <br><br> Um Ubuntu zu verschlüsseln, klickt auf `Fortgeschrittene Optionen` und wählt: <br>• `LVM bei der neuen Ubuntu-Installation verwenden` <br>• `Die neue Ubuntu-Installation verschlüsseln` <br><br>Klickt dann auf`OK` und auf `Jetzt installieren`. |
    |Wählt einen Sicherheitsschlüssel |Wählt einen [sicheren, individuellen Sicherheitsschlüssel](https://gofoss.net/de/passwords/). Dieser wird bei jedem Neustart Eures Computers zur Entschlüsselung der Festplatte benötigt. Klickt anschließend auf `Weiter`. <br><br>*Vorsicht*: Bei Verlust des Sicherheitsschlüssels gehen alle Daten verloren. Bewahrt ihn daher sicher auf! |
    |Wo befinden Sie sich? |Wählt eine Zeitzone und einen Ort. Klickt anschließend auf `Weiter`. |
    |Wer sind Sie? |Gebt Anmeldeinformationen an, z. B. einen Benutzernamen und ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/). Wählt `Passwort zum Anmelden abfragen` und klickt auf `Weiter`. |
    |Startet den Rechner neu & meldet Euch an |Das war's. Wartet, bis die Installation abgeschlossen ist, entfernt den USB-Stick und klickt auf `Jetzt neu starten`, sobald Ihr dazu aufgefordert werdet. Meldet Euch nach dem Neustart bei Ubuntu mit Eurem Sicherheitsschlüssel und Passwort an. |

    </center>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252dab-255f-42c7-885e-711d9a24da85" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Ubuntu System Aktualisierungen" width="150px"></img> </center>

## System-Aktualisierungen

Ihr solltet Ubuntu stets auf dem neuesten Stand halten und regelmäßig Sicherheitskorrekturen, Fehlerbehebungen und Programmaktualisierungen aufspielen. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt anschließend den folgenden Befehl aus:

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    Der erste Teil des Befehls `sudo apt update` prüft, ob neue Programmversionen verfügbar sind. Der zweite Teil des Befehls `sudo apt upgrade` installiert die neuesten Updates. Das `-y` am Ende des Befehls gestattet die Installation neuer Pakete.

    Falls Ihr ohne Terminal arbeiten möchtet, könnt Ihr in der oberen Menüleiste auf `Aktivitäten` klicken und nach dem `Software-Updater` suchen. Dieser prüft, ob Aktualisierungen bereitstehen und schlägt vor, diese zu installieren.

    Sobald das System auf den neuesten Stand gebracht wurde, könnt Ihr mit den folgenden Terminalbefehlen das System bereinigen und unnötige Pakete entfernen:

    ```bash
    sudo apt autoremove
    sudo apt autoclean
    sudo apt clean
    ```

<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Ubuntu-Fehlerberichte" width="150px"></img> </center>

## Fehlerberichte

[Apport](https://wiki.ubuntu.com/Apport/) ist Ubuntus Fehlermeldesystem. Es erfasst Programmabstürze und speichert Fehlerberichte. Aus Datenschutzgründen solltet Ihr Apport jedoch abschalten. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt dann folgende Befehle aus, um die Fehlerberichtsfunktion vollständig zu entfernen:

    ```bash
    sudo apt purge apport
    sudo rm /etc/cron.daily/apport
    ```

    Solltet Ihr es vorziehen, ohne Terminal zu arbeiten, könnt Ihr das Ubuntu Software Centre öffnen, nach `Apport` suchen und auf `Entfernen` klicken. Wollt Ihr hingegen lediglich den automatischen Fehlerbericht deaktivieren, ohne die Funktionalität komplett zu entfernen, müsst Ihr das Terminal aufrufen und den Befehl `sudo systemctl disable apport.service` eingeben. Öffnen anschließend die Konfigurationsdatei mit dem Befehl `sudo gedit /etc/default/apport` und setzt den Wert `enabled` auf Null, d.h. `enabled=0`.


<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Ubuntu-Codecs" width="150px"></img> </center>

## Codecs

Codecs legen fest, wie Eure Rechner Video- oder Audiodateien lesen sollen. Aus rechtlichen und ethischen Gründen enthalten einige Linux-Distributionen nicht alle Multimedia-Codecs. Um bestimmte Dateiformate richtig anzuzeigen ist es daher manchmal notwendig, zusätzliche Codecs zu installieren. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt die folgenden Befehle aus, um Euer Multimedia-Erlebnis in vollen Zügen zu genießen:

    ```bash
    sudo add-apt-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -cs) partner"
    sudo apt update
    sudo apt install ubuntu-restricted-extras adobe-flashplugin browser-plugin-freshplayer-pepperflash libavcodec-extra libdvd-pkg
    ```

<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Ubuntu Software-Center" width="150px"></img> </center>

## Software-Center

Ab Ubuntu 20.04 wurde das traditionelle Software-Center durch Snap ersetzt. Bei Snap handelt es sich um eine neue Technologie, die Anwendungen gebündelt in einer einzigen Datei bereitstellt. Falls Ihr es vorziehen solltet, wieder zum Software-Center zurückzukehren, könnt Ihr den nachstehenden Anweisungen folgen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt dann folgende Befehle aus:

    ```bash
    sudo apt remove snap-store
    sudo apt install gnome-software
    ```

    Wenn Ihr wollt, dass das Software-Center sowohl Snap- als auch Flatpak-Pakete unterstützt, könnt Ihr folgende Befehle ausführen:

    ```bash
    sudo apt install gnome-software-plugin-snap
    sudo apt install gnome-software-plugin-flatpak
    ```

<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Ubuntu Gnome Tweaks" width="150px"></img> </center>

## Erscheinung & Handhabung

Mit Ubuntu könnt Ihr fast alle visuellen Aspekte des Betriebssystems anpassen: Schriftarten, Fensterstile, Animationen, Themen, Symbole und so weiter. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt den folgenden Befehl aus, um [Gnome Tweaks](https://wiki.gnome.org/Apps/Tweaks/) zu installieren und die volle Kontrolle über Ubuntus Erscheinung zu erhalten:

    ```bash
    sudo apt install gnome-tweaks
    ```

    Alternativ könnt Ihr auch auf `Aktivitäten` in der oberen Menüleiste klicken und nach dem Eintrag `Software` suchen. Sucht anschließend nach `Gnome Tweaks` und klickt auf `Installieren`.


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Ubuntu-Grafiktreiber" width="150px"></img> </center>

## Ubuntu-Grafiktreiber

Die große Mehrheit der Linux-Distributionen wird mit Open-Source-Grafiktreibern ausgeliefert. Diese reichen meist zur Nutzung von Standardanwendungen aus, sind aber oft ungeeignet für Spiele. Ihr könnt zusätzlich proprietäre Grafiktreiber installieren, indem Ihr auf `Aktivitäten` in der oberen Menüleiste klickt und `Zusätzliche Treiber` eingebt. Wählt anschließend einen Grafiktreiber aus (die Standardoption ist meist geeignet) und startet Euren Rechner neu.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr in [Ubuntus Dokumentation](https://help.ubuntu.com/), [Ubuntus Tutorials](https://ubuntu.com/tutorials) oder [Ubuntus Wiki](https://wiki.ubuntu.com/). Ihr könnt auch gerne die [beginnerfreundliche Ubuntu-Gemeinschaft](https://ubuntuusers.de/) um Unterstützung bitten.

<div align="center">
<img src="https://imgs.xkcd.com/comics/sandwich.png" alt="Ubuntu Desktop"></img>
</div>

<br>
