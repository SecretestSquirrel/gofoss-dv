---
template: main.html
title: How to encrypt your emails
description: Privacy respecting email providers. Is Protonmail safer than Gmail? Protonmail vs. Gmail? Tutanota vs. Protonmail? Is Tutanota safe?
---


# Encrypt your emails

!!! level "For beginners. No tech skills required."

<div align="center">
<img src="../../assets/img/https5.png" alt="Email encryption" width="250px"></img>
</div>

Choose a trustworthy email provider. Carefully assess aspects such as available features, encryption technologies, or server locations — privacy legislation changes from country to country. This chapter provides an (incomplete) overview of popular, privacy respecting email providers.


<br>


<center> <img src="../../assets/img/separator_protonmail.svg" alt="Protonmail" width="150px"></img> </center>

## Protonmail

[Protonmail](https://protonmail.com/) claims to be the world's largest secure email service, protected by Swiss privacy laws. It's amongst others funded by US investors (Charles River Ventures) and the European Union. The source code is [available here](https://github.com/ProtonMail/).

At the time of writing, the basic single user account offered 500 MB storage for free. For 4 to 24 EUR/month, you get access to more users and storage, as well as a plethora of features: calendar, contact and email imports, bitcoin payments, VPN, and more.

??? warning "Some words of advice on encryption"

    <center>

    | Emails | Encryption |
    | ------ | ------ |
    | **Sent between Protonmail users** |Message body and attachments are end-to-end encrypted. Subject lines and recipient/sender addresses are not. |
    | **Sent from Protonmail users to other providers** |Message body and attachments are only end-to-end encrypted if the user selects the `Encrypt for Outside` option. Otherwise, only TLS encryption is applied if the receiving mail server supports it (which also means that the receiving provider can read the message). In any case, subject lines and recipient/sender addresses are not end-to-end encrypted.|
    | **Received by Protonmail users from other providers** | Message body and attachments are only encrypted with TLS, if the sender's mail server supports it. Subject lines and recipient/sender addresses are not end-to-end encrypted. |

    </center>


### Protonmail clients

Besides [webmail](https://account.protonmail.com/login/) access, Protonmail offers mobile apps for Android and iOS. On desktop environments, Protonmail works with [Thunderbird](https://www.thunderbird.net/en-GB/) via a so-called Bridge application. This feature is however only available to paid accounts. Alternatively, [ElectronMail](https://github.com/vladimiry/ElectronMail) is a free and open source desktop client for Protonmail. Mind however that ElectronMail is an unofficial app. More detailed instructions below.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

         Simply download the Protonmail app from [Google's Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=en&gl=US) or [Aurora Store](https://auroraoss.com/). It [contains 0 trackers and requires 14 permissions](https://reports.exodus-privacy.eu.org/en/reports/ch.protonmail.android/latest/). By comparison: for Gmail it's 1 tracker and 55 permissions; for Outlook it's 13 trackers and 49 permissions; and for Hotmail it's 4 trackers and 31 permissions.


=== "iOS"

    ??? tip "Show me the step-by-step guide iOS"

         Simply download the Protonmail app from the [App Store](https://apps.apple.com/us/app/protonmail-encrypted-email/id979659905/).


=== "Windows"

    ??? tip "Show me the step-by-step guide for ElectronMail on Windows (no paid account needed)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download ElectronMail |Download and run the [ElectronMail installer for Windows](https://github.com/vladimiry/ElectronMail/releases). |
        | Create a master password |Open ElectronMail and provide a [strong, unique master password](https://gofoss.net/passwords/) to protect your emails. |
        | Login |Provide your Protonmail credentials, including two-factor authentication if activated. |
        | Domain |Choose a domain from the list. There is even an `Onion` option to use Tor. Then click on `Close`. |

        </center>


    ??? tip "Show me the step-by-step guide for Thunderbird on Windows (paid accounts only)"

        ### Install Thunderbird on Windows

        Navigate to [Thunderbird's download page](https://www.thunderbird.net/en-US/) and click on the "Free Download" button. Once the installer is downloaded, click on the "Run" button and follow the installation wizard.

        ### Install Protonmail Bridge on Windows

        Thunderbird integrates nicely with Protonmail, making sure emails stay encrypted when they enter and leave your computer. This is handled by the so-called Bridge application, available to paid users only. [Download Protonmail Bridge for Windows](https://protonmail.com/bridge/download). Once the installer is downloaded, click on the "Run" button and follow the installation wizard.

        ### Configure Protonmail Bridge on Windows

        Open the freshly installed Protonmail Bridge application and follow the setup wizard:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Log into your Protonmail account. |
        | 2 | Click on your account name and then the `Mailbox configuration` button. |
        | 3 | A window with the title `Protonmail Bridge Mailbox Configuration` should pop up. It displays IMAP and SMTP settings, including a password, needed later on to configure Thunderbird. |

        </center>

        ### Configure Thunderbird on Windows

        Now launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Email Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Your name | Enter the name you want others to see. |
        | Email address | Enter your Protonmail email address. |
        | Password | Copy and paste the password from the `Protonmail Bridge Mailbox Configuration` window (do not enter your Protonmail password, it won't work). |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Manual config | Click on the `Manual config` button, and fill out the IMAP and SMTP settings provided in the `Protonmail Bridge Mailbox Configuration` window (for Authentication, select `Normal password`). |
        | Re-test | Click on the `Re-test` button to verify your connection settings. |
        | Advanced config | Click on the `Advanced config` button. A new window appears. Just click on the `OK` button, do not modify any settings in this window. |
        | Add Security Exception | Click on the `Confirm Security Exception` button in the pop-up window. This confirms that your computer (127.0.0.1) can run the Bridge app. You might have to confirm a second security exception later on, once you send your first email. |

        </center>

    ??? tip "Show me the 3-minute summary video for Thunderbird (paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Courtesy of Protonmail. Instructions should similarly apply to macOS or Linux.*

        </center>


=== "macOS"

    ??? tip "Show me the step-by-step guide for ElectronMail on macOS (no paid account needed)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download ElectronMail |Download the [ElectronMail disk image](https://github.com/vladimiry/ElectronMail/releases), open it and drag the ElectronMail icon on top of the Application folder. For easy access, open the Applications folder and drag the ElectronMail icon to your dock. |
        | Create a master password |Open ElectronMail and provide a [strong, unique master password](https://gofoss.net/passwords/) to protect your emails. |
        | Login |Provide your Protonmail credentials, including two-factor authentication if activated. |
        | Domain |Choose a domain from the list. There is even an `Onion` option to use Tor. Then click on `Close`. |

        </center>


    ??? tip "Show me the step-by-step guide for Thunderbird on macOS (paid accounts only)"

        ### Install Thunderbird on macOS

        Navigate to [Thunderbird's download page](https://www.thunderbird.net/en-US/) and click on the `Free Download` button. Once the installer is downloaded, it should open by itself and mount a new volume containing the Thunderbird application. If not, open the downloaded Thunderbird .dmg file and drag the appearing Thunderbird icon on top of the Application folder. For easy access, open the Applications folder and drag the Thunderbird icon to your dock.

        ### Install Protonmail Bridge on macOS

        Thunderbird integrates nicely with Protonmail, making sure emails stay encrypted when they enter and leave your computer. This is handled by the so-called Bridge application, available to paid users only. [Download Protonmail Bridge for macOS](https://protonmail.com/bridge/download). Once the installer is downloaded, it should start by itself and mount a new volume containing the Protonmail application. If not, open the downloaded Protonmail Bridge `.dmg` file and drag the Protonmail icon on top of the Application folder. For easy access, open the Applications folder and drag the Protonmail Bridge icon to your dock.

        ### Configure Protonmail Bridge on macOS

        Open the freshly installed Protonmail Bridge application and follow the setup wizard:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Log into your Protonmail account. |
        | 2 | Click on your account name and then the `Mailbox configuration` button. |
        | 3 | A window with the title `Protonmail Bridge Mailbox Configuration` should pop up. It displays IMAP and SMTP settings, including a password, needed later on to configure Thunderbird. |

        </center>

        ### Configure Thunderbird on macOS

        Now launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Email Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Your name | Enter the name you want others to see. |
        | Email address | Enter your Protonmail email address. |
        | Password | Copy and paste the password from the `Protonmail Bridge Mailbox Configuration` window (do not enter your Protonmail password, it won't work). |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Manual config | Click on the `Manual config` button, and fill out the IMAP and SMTP settings provided in the `Protonmail Bridge Mailbox Configuration` window (for Authentication, select `Normal password`). |
        | Re-test | Click on the `Re-test` button to verify your connection settings. |
        | Advanced config | Click on the `Advanced config` button. A new window appears. Just click on the `OK` button, do not modify any settings in this window. |
        | Add Security Exception | Click on the `Confirm Security Exception` button in the pop-up window. This confirms that your computer (127.0.0.1) can run the Bridge app. You might have to confirm a second security exception later on, once you send your first email. |

        </center>

    ??? tip "Show me the 3-minute summary video for Thunderbird (paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Courtesy of Protonmail. Instructions should similarly apply to macOS or Linux.*

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for ElectronMail on Ubuntu Linux (no paid account needed)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download ElectronMail |Download the latest [ElectronMail .deb package](https://github.com/vladimiry/ElectronMail/releases). The file should be named something like `electron-mail-X-XX-X-linux-amd64.deb`. For the purpose of this tutorial, let's suppose the file was downloaded to the folder `/home/gofoss/Downloads`. Make sure to adjust these file paths according to your own setup. Now open the terminal with the `Ctrl+Alt+T` shortcut or click on the `Applications` button on the top left and search for `Terminal`. Finally, run the following commands:<br><br> `cd /home/gofoss/Downloads` <br>`sudo dpkg -i electron-mail-X-XX-X-linux-amd64.deb` |
        | Create a master password |Open ElectronMail and provide a [strong, unique master password](https://gofoss.net/passwords/) to protect your emails. |
        | Login |Provide your Protonmail credentials, including two-factor authentication if activated. |
        | Domain |Choose a domain from the list. There is even an `Onion` option to use Tor. Then click on `Close`. |

        </center>


    ??? tip "Show me the step-by-step guide for Thunderbird on Ubuntu Linux (paid accounts only)"

        ### Install Thunderbird on Linux

        Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. Run the following command to install Thunderbird:

        ```bash
        sudo apt install thunderbird
        ```

        ### Install Protonmail Bridge on Linux

        Thunderbird integrates nicely with Protonmail, making sure emails stay encrypted when they enter and leave your computer. This is handled by the so-called Bridge application, available to paid users only. [Download Protonmail Bridge for Linux](https://protonmail.com/bridge/download). The file should be called something similar to `protonmail-bridge_X.X.X-X_amd64.deb`. Let's assume it has been downloaded to the folder `/home/gofoss/Downloads`. Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. Then run the following commands (don't forget to adjust the filename and download folder path accordingly):

        ```bash
        sudo apt install gdebi
        cd /home/gofoss/Downloads
        sudo gdebi protonmail-bridge_X.X.X-X_amd64.deb
        ```

        ### Configure Protonmail Bridge on Linux

        Open the Bridge application with the terminal command `protonmail-bridge`, or click on the `Applications` button on the top left, and search for `ProtonMail Bridge`. Follow the setup wizard:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Log into your Protonmail account. |
        | 2 | Click on your account name and then the `Mailbox configuration` button. |
        | 3 | A window with the title `Protonmail Bridge Mailbox Configuration` should pop up. It displays IMAP and SMTP settings, including a password, needed later on to configure Thunderbird. |

        </center>

        ### Configure Thunderbird on Linux

        Now launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Email Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Your name | Enter the name you want others to see. |
        | Email address | Enter your Protonmail email address. |
        | Password | Copy and paste the password from the `Protonmail Bridge Mailbox Configuration` window (do not enter your Protonmail password, it won't work). |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Manual config | Click on the `Manual config` button, and fill out the IMAP and SMTP settings provided in the `Protonmail Bridge Mailbox Configuration` window (for Authentication, select `Normal password`). |
        | Re-test | Click on the `Re-test` button to verify your connection settings. |
        | Advanced config | Click on the `Advanced config` button. A new window appears. Just click on the `OK` button, do not modify any settings in this window. |
        | Add Security Exception | Click on the `Confirm Security Exception` button in the pop-up window. This confirms that your computer (127.0.0.1) can run the Bridge app. You might have to confirm a second security exception later on, once you send your first email. |

        </center>


    ??? tip "Show me the 3-minute summary video for Thunderbird (paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Courtesy of Protonmail. Instructions should similarly apply to macOS or Linux.*

        </center>


<br>


<center> <img src="../../assets/img/separator_tutanota.svg" alt="Tutanota" width="150px"></img> </center>

## Tutanota

[Tutanota](https://tutanota.com/) is a freemium hosted secure email service, registered in Germany. Everything is end-to-end encrypted and [open-source](https://github.com/tutao/tutanota/). Tutanota uses its own encryption standard, and does not support PGP.

At the time of writing, the basic account offered 1 GB storage for free. For approximately 1 to 6 EUR/month, you get access to more users and storage, as well as a plethora of features: custom domains, unlimited search, multiple calendars, inbox rules, whitelabel, calendar sharing, etc. Email imports and anonymous payment are currently not supported.


### Tutanota clients

Besides [webmail](https://mail.tutanota.com/?r=/login/) access, Tutanota offers mobile apps for Android and iOS. For desktop environments, Tutanota developed its own dedicated client. More detailed instructions below.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

         Simply download the Tutanota app from [Google's Play Store](https://play.google.com/store/apps/details?id=de.tutao.tutanota&hl=en&gl=US) or [Aurora Store](https://auroraoss.com/). Tutanota is also available on [F-Droid](https://f-droid.org/en/packages/de.tutao.tutanota/). Alternatively, visit Tutanota's [download page](https://tutanota.com/#download) or [Github repository](https://github.com/tutao/tutanota/releases/) to download and install the `.apk` file. The app [contains 0 trackers and requires 9 permissions](https://reports.exodus-privacy.eu.org/en/reports/de.tutao.tutanota/latest/). By comparison: for Gmail it's 1 tracker and 55 permissions; for Outlook it's 13 trackers and 49 permissions; and for Hotmail it's 4 trackers and 31 permissions.

=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

         Simply download the Tutanota app from the [App Store](https://apps.apple.com/us/app/tutanota/id922429609).


=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        Simply [download the installer](https://mail.tutanota.com/desktop/tutanota-desktop-win.exe), then click on the `Run` button and follow the installation wizard.


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        Simply [download the installer](https://mail.tutanota.com/desktop/tutanota-desktop-mac.dmg), which should open by itself and mount a new volume containing the Tutanota application. If not, open the downloaded Tutanota `.dmg` file and drag the appearing Tutanota icon on top of the Application folder. For easy access, open the Applications folder and drag the Tutanota icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Simply [download the installer](https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage), which should be called something like `tutanota-desktop-linux.AppImage`. Let's assume it was downloaded to the folder `/home/gofoss/Downloads`. Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Applications` button on the top left and search for `Terminal`. Then run the following commands (don't forget to adjust the filename and download folder path accordingly):

        ```bash
        cd /home/gofoss/Downloads
        chmod +x tutanota-desktop-linux.AppImage
        ```

    ??? tip "Show me how to pin Tutanota to the Ubuntu dock"

        It's not straight forward, but Tutanota's launcher can be added to Ubuntu's application menu and pinned to the dock. Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Applications` button on the top left and search for `Terminal`. Run the following command:

        ```bash
        sudo gedit /usr/share/applications/tutanota.desktop
        ```

        Paste the following content into the newly created file. Make sure to point the `Exec` path towards the folder containing the downloaded AppImage:

        ```
        #!/usr/bin/env xdg-open
        [Desktop Entry]
        Version=1.0
        Type=Application
        Terminal=false
        Exec=/home/gofoss/Downloads/tutanota-desktop-linux.AppImage
        Name=Tutanota
        ```

        Make the file executable:

        ```bash
        sudo chmod +x /usr/share/applications/tutanota.desktop
        ```

        Log off and back into your Ubuntu session. You should now be able to launch Tutanota from the application menu, and pin it to the dock.


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Email providers" width="150px"></img> </center>

## Other providers

=== "Disroot"

    | Info | Description |
    | :-----: | ----- |
    |Website |[disroot. org](https://disroot.org/en/services/email) |
    |Pricing | Basic account is free (1 GB storage); extra storage for 0.15 EUR per GB per month. |
    |Features |Platform providing online services based on principles of freedom, privacy, federation and decentralization. Located in the Netherlands. Accepts bitcoin and faircoin. Full disk encryption & email encryption. Mobile app. |
    |Anti-features| Can potentially decrypt user data, as emails are reportedly stored in plain text. |


=== "Mailbox"

    | Info | Description |
    | :-----: | ----- |
    |Website |[mailbox. org](https://mailbox.org/en/) |
    |Pricing | 1 EUR/month, 2 GB storage. |
    |Features |German open source email provider, with servers located in Berlin. Offers security features such as encryption at rest, PGP, DANE, SPF and DKIM, as well as two-factor authentication, full text search, calendars, address books and task lists, CalDAV and CardDAV synchronisation. |
    |Anti-features| No mobile client, need for third party clients. |


=== "Posteo"

    | Info | Description |
    | :-----: | ----- |
    |Website |[posteo.de](https://posteo.de/en/) |
    |Pricing | 1 EUR/month, 2 GB storage. |
    |Features |German open source email provider, self-financed, encryption at rest, two-factor authentication, calendars and address books, CalDAV and CardDAV synchronisation. |
    |Anti-features| No spam folder, no trial or free version. |


=== "Kolab Now"

    | Info | Description |
    | :-----: | ----- |
    |Website |[kolabnow.com](https://kolabnow.com/) |
    |Pricing | 5 USD/month, 2 GB storage. |
    |Features |Swiss open source email provider, text search and tagging, filters, address books, calendars, CalDAV and CardDAV synchronisation. |
    |Anti-features| No built-in end-to-end encryption, not encryption at rest. |


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="Email forwarding" width="150px"></img> </center>

## Transition period

=== "Forward emails"

    Transitioning to a new email account can take some time. You'll probably want to keep your old accounts alive for a while to make sure you don't miss out on anything. Just forward any incoming message to the new account. For more instructions on how to forward emails refer to the documentation pages of [Gmail](https://support.google.com/mail/answer/10957?hl=en/), [Outlook](https://support.microsoft.com/en-us/office/create-reply-to-or-forward-email-messages-in-outlook-on-the-web-ecafbc06-e812-4b9e-a7af-5074a9c7abd0?redirectsourcepath=%252fen-us%252farticle%252fforward-email-from-office-365-to-another-email-account-1ed4ee1e-74f8-4f53-a174-86b748ff6a0e%252f), [iCloud](https://www.idownloadblog.com/2019/01/28/set-up-icloud-email-forwarding/), [Yahoo](https://www.wikihow.com/Forward-Yahoo-Mail) and so on.


=== "Update subscriptions"

    Use the transition period to scan your old email accounts for any active subscriptions and update your new email address!


=== "Inform contacts"

    Don't forget to communicate the new email address to your personal and professional contacts, bank, insurance, tax office, and so on. You might also want to set up an auto-reply message on your old account to keep folks informed about the change of address.


=== "Terminate old account"

    Over time, less and less emails will land in your old inbox. Eventually, it will become inactive. That's when you should consider terminating your old email account.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Protonmail & Tutanota support" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [Protonmail's support](https://protonmail.com/support/) or [Thunderbird's documentation](https://support.mozilla.org/en-US/products/thunderbird). Also feel free to ask the [Protonmail](https://teddit.net/r/ProtonMail/) or [Thunderbird](https://support.mozilla.org/en-US/questions/new/thunderbird) communities for help.

* [Tutanota's documentation](https://tutanota.com/howto/#install-desktop/) or [ask the Tutanota community](https://teddit.net/r/tutanota/).

<br>

<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/email_reply.png" width="450px" alt="Secure Emails"></img>
</div>

<br>