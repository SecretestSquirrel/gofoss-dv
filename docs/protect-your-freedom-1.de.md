---
template: main.html
title: FOSS schützt Eure Daten vor dem Überwachungskapitalismus
description: Was ist FOSS? Was ist ein Bedrohungsmodell? Was ist der Unterschied zwischen Datenschutz, Anonymität und Sicherheit?
---

# Was geht mich das an?

<div align="center">
<img src="../../assets/img/closed_ecosystem.png" alt="Walled garden" width="500px"></img>
</div>

GoFOSS möchte freie, quelloffene und datenschutzfreundliche Software zugänglicher machen. Entscheidet selbst, ob sich das Weiterlesen lohnt:

* Besitzt Ihr ein iPhone? Ein Android-Smartphone? Einen Computer auf dem Windows, macOS oder ChromeOS läuft?
* Nutzt Ihr Chrome, Safari oder Edge, um im Internet zu surfen?
* Seit Ihr auf Facebook, Twitter, WhatsApp, Tiktok oder Snapchat aktiv?

Wahrscheinlich habt Ihr zumindest eine der Fragen bejaht. Ist Euch dabei bewusst, dass all diese Dienste von Unternehmen entwickelt werden, die Teil des sogenannten [Überwachungskapitalismus](https://de.wikipedia.org/wiki/%C3%9Cberwachungskapitalismus) sind? Dieser Begriff beschreibt ein Wirtschaftssystem, in dem Technologiemonopole persönliche Nutzerdaten zur Gewinnmaximierung ausbeuten.

Euch ist unklar, wo dabei das Problem liegt? Nun ja, der Überwachungskapitalismus bedroht den Kern unserer Gesellschaftsordnung. Er führt zu Massenüberwachung, polarisiert die politische Debatte, mischt sich in Wahlen ein, vereinheitlicht Denkstrukturen, begünstigt die Zensur und fördert geplante Obsoleszenz.

Die Bewältigung dieser Probleme erfordert umfassende Änderungen unserer Rechtssysteme und gesellschaftlichen Konventionen. Bis dahin könnt Ihr bereits eine Menge tun, um Eure Daten zu schützen und Euer Recht auf Reparatur einzufordern. Das fängt z.B. mit der Nutzung freier und quelloffener Software an, die oft auch [FOSS](https://fsfe.org/freesoftware/freesoftware.de.html) genannt wird. Das erlaubt Euch online zu surfen, zu kommunizieren und zu arbeiten, ohne dass Eure Daten erfasst, zu Geld gemacht oder zensiert werden. Oder alle zwei Jahre neue Geräte kaufen zu müssen.

<br>