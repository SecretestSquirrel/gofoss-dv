---
template: main.html
title: Déclaration de confidentialité, avis juridique et licence
description: Déclaration de confidentialité. Les données que nous collectons. Vos droits. Avis juridique. Résumé de la licence. GDPR. Ce que nous ne ferons jamais avec vos données.
---

# Déclaration de confidentialité, avis juridique et licence

## Notre déclaration de confidentialité

<center> <img src="../../assets/img/disclaimer.png" alt="protection et respect de la vie privée sur internet" width="700px"></img> </center>

v1.1 - Octobre 2021

gofoss.net s'engage à protéger la vie privée de ses utilisatrices et utilisateurs. Comme tout ce que nous faisons, cette déclaration de confidentialité a été conçue pour être simple et accessible à tou·t·es.

Ce document a été initialement rédigé en anglais ; c'est l'unique version pour laquelle gofoss.net peut être tenu responsable.


### Définitions

* *RGPD*: Règlement général sur la protection des données, [EU 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/?qid=1580499932731&uri=CELEX%3A32016R0679)
* *Données* : Selon le RGPD, les données sont toutes les informations qui peuvent être utilisées pour identifier une personne, soit directement (nom réel, numéro de téléphone, adresse IP, etc.), soit indirectement (toute combinaison des éléments susmentionnés plus les empreintes digitales des appareils, les cookies, etc.). Dans le contexte spécifique de l'utilisation de notre site web, il s'agit des informations minimales requises pour le bon fonctionnement du site web.
* *Services* : l'ensemble des différents logiciels, protocoles et normes utilisés pour échanger des données entre applications web.
* *Utilisateur* ou *vous* : toute personne ou tiers qui accède à gofoss.net.
* *gofoss.net* ou *nous* : https://gofoss.net, https://gofoss.today
* *Hébergeur* : ce site est hébergé par [Netlify](https://www.netlify.com/).


### Champ d'application

Cette déclaration de confidentialité s'applique à gofoss.net et à ses sous-domaines. Elle ne s'étend pas aux sites web ou aux services web accessibles à partir de notre site, y compris, mais sans s'y limiter, les services fédérés ou les sites web de médias sociaux. Ces services utilisent des protocoles qui partagent ou transfèrent nécessairement des données entre différents fournisseurs et, par conséquent, ces interactions ne relèvent pas de la présente déclaration de confidentialité.


### Consentement

En accédant à notre site web, vous acceptez notre politique de confidentialité et nos conditions d'utilisation.


### Quelles sont les données que nous recueillons ?

* Nous n'utilisons pas de cookies.
* Nous ne vous suivons pas à travers des scripts.
* Nous n'implémentons pas de traqueurs sur notre site web pour analyser votre comportement.
* Nous ne recueillons pas d'informations permettant de vous identifier personnellement, par le biais de formulaires ou d'autres méthodes.
* Nous avons choisi un hébergeur connu pour [respecter la vie privée des utilisatrices et utilisateurs](https://www.netlify.com/gdpr-ccpa/), qui s'est associé à des experts juridiques en Europe et aux États-Unis pour s'assurer que ses produits et ses engagements contractuels sont conformes à la réglementation RGPD.
* L'hébergeur collecte les journaux d'accès, y compris les adresses IP des visiteurs du site, stockés pendant moins de 30 jours.


### Ce que nous ne ferons jamais avec vos données

* Nous ne cherchons pas à tirer profit de vos données.
* Nous ne collectons pas d'autres données que celles qui sont nécessaires au fonctionnement du site web.
* Nous ne traitons, ni n'analysons en aucune façon votre comportement ou vos caractéristiques personnelles pour créer des profils vous concernant.
* Nous n'affichons pas de publicité et n'avons pas de relations commerciales avec des annonceurs.
* Nous ne vendons pas vos données à des tiers.
* Nous ne demandons aucune information supplémentaire qui ne soit pas essentielle au fonctionnement du site web (nous ne demandons pas de numéros de téléphone, de données personnelles privées, d'adresse personnelle, etc.)


### Vos droits

En vertu du *RGPD*, vous disposez d'un certain nombre de droits en ce qui concerne vos données personnelles :

* *Droit d'accès* - Le droit de demander (I) des copies de vos données personnelles ou (II) l'accès aux informations que vous avez soumises et que nous détenons à tout moment.
* *Droit de rectification* - Le droit de faire rectifier vos données si elles sont inexactes ou incomplètes.
* *Droit à l'oubli* - Le droit de demander la suppression ou le retrait de vos données de notre site web.
* *Droit de restreindre l'utilisation de vos données* - Le droit de restreindre le traitement ou de limiter la manière dont nous utilisons vos données.
* *Droit à la portabilité des données* - Le droit de déplacer, copier ou transférer vos données.
* *Droit d'opposition* - Le droit de s'opposer à notre utilisation de vos données.


### Modifications de la présente déclaration de confidentialité

Toutes les modifications apportées à la présente *déclaration de confidentialité* seront accessibles au public. Nous vous recommandons de vérifier régulièrement si des modifications ont été apportées à la présente déclaration.


### Nous contacter

Si vous avez des questions concernant la présente politique de confidentialité ou les pratiques de ce site web, veuillez nous contacter en envoyant un courriel à [gofoss@protonmail.com](mailto:gofoss@protonmail.com).


<br>

## Mentions légales

Toutes les informations sur [https://gofoss.net](https://gofoss.net/) sont publiées de bonne foi et à des fins d'information générale uniquement. [https://gofoss.net](https://gofoss.net/) ne donne aucune garantie quant à l'exhaustivité, la fiabilité et l'exactitude de ces informations.

Aucune garantie n'est donnée ou suggérée. Toute action que vous entreprenez sur la base des informations que vous trouvez sur [https://gofoss.net](https://gofoss.net/), est strictement à vos propres risques. L'utilisatrice ou l'utilisateur assume tous les risques de l'ensemble des dommages qui pourraient survenir, y compris, mais sans s'y limiter, la perte de données, les dommages au matériel ou la perte de gains commerciaux. [https://gofoss.net](https://gofoss.net/) ne sera pas responsable des pertes et/ou dommages liés à l'utilisation de notre site web. Notez que, sauf si la garantie de votre appareil l'autorise explicitement, il faut partir du principe que toute garantie accompagnant votre appareil sera annulée si vous modifiez le logiciel ou le matériel.

À partir de notre site web, vous pouvez visiter d'autres sites web en suivant les hyperliens vers ces sites externes. Bien que nous nous efforcions de ne fournir que des liens de qualité vers des sites utiles et éthiques, nous n'avons aucun contrôle sur le contenu et la nature de ces sites. Ces liens vers d'autres sites web n'impliquent donc pas une recommandation pour l'ensemble des contenus pouvant se trouver sur ces sites. Les propriétaires des sites et leur contenu peuvent changer sans préavis, et cela peut se produire avant que nous ayons la possibilité de supprimer un lien qui aurait évolué négativement. Sachez également que lorsque vous quittez notre site web, d'autres sites peuvent avoir des politiques de confidentialité et des conditions différentes qui échappent à notre contrôle. Veillez à vérifier les politiques de confidentialité de ces sites ainsi que leurs conditions de service avant de vous engager dans une activité commerciale ou de télécharger des informations.

En utilisant notre site web, vous consentez par la présente à notre clause de non-responsabilité et en acceptez les termes. Si nous mettons à jour, modifions ou apportons des changements à ce document, ces changements seront affichés ici de manière visible.


<br>

## Licence (résumé)

Si vous utilisez ce site, n'oubliez pas de mentionner le nom de [https://gofoss.net](https://gofoss.net/) et de produire un lien vers la licence.

Copyright (c) 2020-2022 Georg Jerska <gofoss@protonmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A full copy of the GNU Affero General Public License version 3 is available at <https://www.gnu.org/licenses/agpl-3.0.txt>.

This program incorporates work covered by the following copyright and permission notice:

> Copyright (c) 2016-2022 Martin Donath <martin.donath@squidfunk.com>
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
