---
template: main.html
title: How to self-host cloud services
description: Fediverse. Alternative cloud providers. Self-host. Ubuntu server. LAMP stack. Server hardening. SSH. Firewall. NTP. Antivirus. Let’s Encrypt. OpenVPN. Pi-Hole.
---

# Join the Fediverse, choose your cloud providers and self-host

<div align="center">
<img src="../../assets/img/server.png" alt="Free your cloud" width="400px"></img>
</div>

Congrats if you've read this far! Maybe you've already changed browser and email provider. Backed up and encrypted your data. Moved to Linux, CalyxOS or LineageOS. And installed FOSS apps. Those are significant steps towards more online privacy!

But that is not the end of it. Your data will continue to be harvested as long as you use cloud services from Google, Apple, Facebook, Twitter, Amazon or Microsoft. To regain full control over your data, you are ultimately facing three options.

<br>

<center> <img src="../../assets/img/separator_mastodon.svg" alt="Fediverse" width="150px"></img> </center>

**Option 1** — join the Fediverse: in this chapter, we'll dive into the Fediverse, a decentralized and FOSS alternative to commercial social media. Join one of the various communities to connect with people, communicate, discuss or share pictures, videos & music.

<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Cloud providers" width="150px"></img> </center>

**Option 2** — choose trustworthy cloud providers: in this chapter, we'll also recommend some privacy respecting cloud services to synchronise and share files, collaborate on documents, schedule meetings, organise chats, stream videos, and so on. Whether you follow these recommendations or your own preferences, make sure your cloud providers apply strict privacy policies and rely on free and open source software.

<br>

<center> <img src="../../assets/img/separator_duckdns.svg" alt="Self hosting" width="150px"></img> </center>

**Option 3** — self-host cloud services: in this chapter, we'll also explain how to set up a server at home and *reasonably* protect it from unauthorised access or malicious attacks. We'll furthermore explain how to host your own cloud services, such as file sync, photo galleries, contact/calendar/task management or media streaming.

<br>
