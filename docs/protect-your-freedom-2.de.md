---
template: main.html
title: FOSS schützt Eure Daten um sicher im Netz zu surfen
description: Datenschutzverletzungen vermeiden. Datenschutz- und Sicherheitseinstellungen konfigurieren. Verschlüsselung verwenden.

---

# Was springt für mich dabei raus?

<center><img src="../../assets/img/foss_ecosystem.png" alt="Freie und quelloffene Software" width="500px"></img></center>

Es gibt keine Patentlösung: der Wechsel zu freier und quelloffener Software ist ein Lernprozess. Wir wollen Euch auf dieser Webseite einige Tipps geben, die Eure digitale Freiheit begünstigen:

<center>

| Vorteile | Schritte |
| :------: | ------ |
|[Surft sicher im Netz](https://gofoss.net/de/intro-browse-freely/) | Wechselt zu Firefox. Blockiert Tracker, Cookies und Werbung. Verwendet datenschutzfreundliche Suchmaschinen. Verschlüsselt Euren Datenverkehr gegebenenfalls mit Tor oder VPN. |
|[Führt vertrauliche Gespräche](https://gofoss.net/de/intro-speak-freely/) |Nutzt durchgängige Verschlüsselung, um Eure E-Mails, Nachrichten und Anrufe zu sichern. |
|[Schützt Eure digitalen Daten](https://gofoss.net/de/intro-store-safely/) |Verwendet sichere und individuelle Passwörter. Wählt für jedes Eurer Konten und Geräte ein eigenes Kennwort. Bewahrt sie sicher in einem verschlüsselten Passwort-Manager auf. Erwägt die Verwendung der Zwei-Faktor-Authentifizierung. Erstellt regelmäßige Sicherungskopien. Und verschlüsselt empfindliche Daten. |
|[Erschließt das volle Potenzial Eurer Computer](https://gofoss.net/de/intro-free-your-computer/) |Wechselt zu GNU/Linux und bevorzugt freie und quelloffene Anwendungen. Wählt je nach Bedürfnis eine einsteigerfreundliche Distro wie Linux Mint oder Ubuntu, oder veteranentaugliche Distros wie Debian, Manjaro, openSUSE, Fedora oder Gentoo Linux. Datenschutz-Liebhaber sollten sich auch Qubes OS, Whonix oder Tails ansehen. |
|[Bleibt mobil und frei](https://gofoss.net/de/intro-free-your-phone/) |Wechselt zu einem mobilen Betriebssystem wie LineageOS, CalyxOS, GrapheneOS oder /e/. Bevorzugt dabei trackerfreie und quelloffene Apps aus gemeinschaftlich gepflegten App-Stores. |
|[Erobert Eure Cloud zurück](https://gofoss.net/de/intro-free-your-cloud/) |Dateien, Bilder, Videos, Kontakte, Kalender und Aufgabenlisten gehören Euch, nicht den Datenkraken. Lasst kommerzielle soziale Medien links liegen und entdeckt das Fediverse, einen Zusammenschluss freier und quelloffener Online-Dienste. Wählt datenschutzfreundliche Cloud-Anbieter. Und richtet gegebenenfalls Euren eigenen Server ein und betreibt Eure Cloud-Dienste selbst. |
|[Lernt, netzwerkt und macht mit](https://gofoss.net/de/nothing-to-hide/) |Man lernt nie aus. Über Online-Datenschutz, Datenausbeutung, Filterblasen, Überwachung, Zensur und geplante Obsoleszenz gibt es viel zu sagen. Erfahrt mehr über unser Projekt, macht mit und teilt Eure Wissen mit anderen. |

</center>

Diese Webseite erlaubt es Euch einen Blick unter die (Rechner-)Haube zu werfen, um selbst zu entscheiden, welche Software Ihr übernehmen, verwerfen oder anpassen wollt. Ihr solltet Euren Geräten vertrauen können, die Kontrolle behaltet und verstehen, welche Software Ihr verwendet. Einige Grundsätze, auf die unsere Webseite Wert legt:

<center>

| Grundsatz | Beschreibung |
| :------: | ------ |
| #1 |Wir gehen schrittweise vor. |
| #2 |Wir beginnen mit den Grundlagen, bevor fortgeschrittene Themen behandelt werden. |
| #3 |Wir veranschaulichen jeden Schritt mit konkreten, praktischen Beispielen. |
| #4 |Wir bevorzugen freie und quelloffene Software. |
| #5 |Wir legen für bestimmte Aufgaben geeignete Qualitätssoftware nahe, statt Mehrzweck-Blähware. |
| #6 |Wir bieten anpassbare Lösungen an, die den BenutzerInnen die Kontrolle überlassen. |
| #7 |Wir bleiben nah am Quellcode und fern von Ein-Klick- oder Containerlösungen. |

</center>

<br>