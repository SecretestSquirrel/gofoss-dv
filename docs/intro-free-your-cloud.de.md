---
template: main.html
title: So hostet Ihr Eure Cloud-Dienste selbst
description: Das Fediverse. Alternative Cloud-Anbieter. Selbst hosten. Ubuntu-Server. LAMP-Stack. Server-Härtung. SSH. Firewall. NTP. Antivirus. Let's Encrypt. OpenVPN. Pi-Hole.
---

# Tretet dem Fediverse bei. Wechselt Cloud-Anbieter. Hostet Eure Cloud-Dienste selbst.

<div align="center">
<img src="../../assets/img/server.png" alt="Befreit Eure Cloud" width="400px"></img>
</div>

Herzlichen Glückwunsch, wenn Ihr es bis hierher geschafft habt! Vielleicht habt Ihr bereits Browser und E-Mail-Anbieter gewechselt; Eure Daten gesichert und verschlüsselt; Linux, CalyxOS oder LineageOS bevorzugt; und FOSS-Apps installiert. Das sind wichtige Schritte in Richtung Datenschutz!

Doch das ist noch nicht das Ende der Fahnenstange. Ihre Daten werden weiterhin zweckentfremdet solange Sie Cloud-Dienste von Google, Apple, Facebook, Twitter, Amazon oder Microsoft nutzen. Um die volle Kontrolle über Ihre Daten wiederzuerlangen, stehen Sie letztlich vor drei Optionen.

<br>

<center> <img src="../../assets/img/separator_mastodon.svg" alt="Fediverse" width="150px"></img> </center>

**Option 1** - Tretet dem Fediverse bei: In diesem Kapitel tauchen wir ins Fediverse ein. Es handelt sich um eine frei, quelloffene und dezentrale Alternative zu kommerziellen sozialen Medien. Tretet einer der verschiedenen Gemeinschaften bei, um Euch mit Menschen zu vernetzen, zu kommunizieren, zu diskutieren oder Bilder, Videos und Musik zu teilen.

<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Cloud-Anbieter" width="150px"></img> </center>

**Option 2** - Wählt datenschutzfreundliche Cloud-Anbieter: In diesem Kapitel empfehlen wir einige datenschutzfreundliche Cloud-Dienste, um Dateien zu synchronisieren und zu teilen, gemeinsam an Dokumenten zu arbeiten, Termine zu planen, Online-Gespräche zu organisieren, Videos zu übertragen und so weiter. Egal, ob Ihr diesen Empfehlungen oder Euren eigenen Vorlieben folgt: Ihr solltet sicherstellen, dass Eure Cloud-Anbieter strengen Datenschutzrichtlinien folgen und auf freie und quelloffene Software-Lösungen setzen.

<br>

<center> <img src="../../assets/img/separator_duckdns.svg" alt="Selbst hosten" width="150px"></img> </center>

**Option 3** - Hostet Eure Cloud-Dienste selbst: In diesem Kapitel erklären wir auch, wie Ihr Euren eigenen Server zu Hause einrichten und ihn *vernünftig* vor unbefugtem Zugriff oder schädlichen Angriffen schützen könnt. Außerdem erklären wir, wie Ihr Eure eigenen Cloud-Dienste hosten könnt, z. B. Dateisynchronisation, Fotogalerien, Kontakt-, Kalender- und Aufgabenverwaltung oder Medien-Übertragung.

<br>
