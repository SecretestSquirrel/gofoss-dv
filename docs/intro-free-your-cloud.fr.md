---
template: main.html
title: Comment auto-héberger des services en nuage
description: Fediverse. Fournisseurs alternatifs de services en nuage. Auto-héberger. Serveur Ubuntu. Pile LAMP. Sécuriser votre serveur. SSH. Pare-feu. NTP. Antivirus. Let's Encrypt. OpenVPN. Pi-Hole.
---

# Rejoignez le Fediverse, changez de fournisseur de services en nuage & auto-hébergez

<div align="center">
<img src="../../assets/img/server.png" alt="Libérez votre nuage" width="400px"></img>
</div>

Bravo si vous avez êtes arrivés jusqu'ici ! Peut-être que vous avez déjà changé de navigateur et de fournisseur d'e-mail. Sauvegardé et chiffré vos données. Changé pour Linux, CalyxOS ou bien LineageOS. Et installé des applications FOSS. Ce sont des étapes significatives vers plus de vie privée en ligne !

Mais ce n'est pas fini. Vos données continueront d'être récoltées tant que vous utiliserez des services en nuage de Google, Apple, Facebook, Twitter, Amazon ou Microsoft. Pour reprendre un contrôle total sur vos données, vous n'avez en définitive que trois options.


<br>

<center> <img src="../../assets/img/separator_mastodon.svg" alt="Fediverse" width="150px"></img> </center>

**Option 1** — rejoignez le Fediverse: dans ce chapitre, nous allons nous plonger dans le Fediverse, une alternative libre et décentralisée aux médias sociaux commerciaux. Rejoignez l'une des différentes communautés pour entrer en contact avec des gens, communiquer, discuter, ou partager vos photos, vidéos et musiques.

<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Fournisseurs de services en nuage" width="150px"></img> </center>

**Option 2** — changez de fournisseur de service en nuage dignes de confiance : dans ce chapitre, nous allons recommander également des services en nuage respectueux de la vie privée pour synchroniser et partager des fichiers, collaborer sur des documents, planifier des réunions, organiser des discussions en ligne, diffuser des vidéos, et ainsi de suite. Que vous suiviez ces recommandations ou vos propres préférences, assurez-vous que vos fournisseurs de solutions en nuage appliquent des politiques strictes de respect de la vie privée, et qu'ils reposent sur des logiciels libres et *open-source*.

<br>

<center> <img src="../../assets/img/separator_duckdns.svg" alt="Auto-hébergement" width="150px"></img> </center>

**Option 3** — auto-hébergez vos services en nuage : dans ce chapitre, nous allons vous expliquer aussi comment configurer un serveur chez vous et le protéger raisonnablement d'accès non autorisés ou d'attaques malveillantes. Nous allons expliquer en outre comment héberger vos propres services en nuage, comme de la synchronisation de fichiers, des galeries de photos, de la gestion de contacts/calendriers/tâches ou de la diffusion de media.


<br>
