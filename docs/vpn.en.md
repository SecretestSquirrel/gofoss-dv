---
template: main.html
title: How to use VPN
description: How does VPN work? How to use VPN? Is using VPN illegal? Can VPN be traced? Is VPN free? VPN for Ubuntu. Free VPN. VPN meaning.
---

# Encrypt your traffic with VPN

!!! level "For intermediate users. Some tech skills required."

<div align="center">
<img src="../../assets/img/vpn_connection.png" alt="VPN" width="500px"></img>
</div>

The term [Virtual Private Network](https://en.wikipedia.org/wiki/Virtual_private_network) (VPN) refers to a form of transport-layer encryption. When connecting to a VPN, traffic sent out or received by your device travels through a sort of encrypted tunnel. If someone eavesdrops, for example your Internet Service Provider (ISP) or an attacker, they can only tell you are transmitting data. They won't know its destination or content. However, the online providers you interact with — such as search engines, banks or email providers — are able to inspect, store and modify your data. So does your VPN provider.

Choose a trustworthy VPN provider to protect your online activity. You'll find many online benchmarks and guides, for support refer to [Reddit's VPN community](https://teddit.net/r/VPN/). The following providers have a good reputation in terms of location, logging, payment, security level, availability, pricing and ethics.

??? warning "VPNs have their limits"

    Using a VPN doesn't necessarily mean being anonymous or secure. Your device still can be exposed to browser fingerprinting, man in the middle attacks, correlation attacks or other exploits. Also note that [in a later chapter, we'll explain how to set up OpenVPN to communicate with your private server](https://gofoss.net/secure-domain/). Such a setup is not meant to protect your online identity.

<br>

<center> <img src="../../assets/img/separator_protonvpn.svg" alt="Proton VPN" width="150px"></img> </center>

## ProtonVPN

[ProtonVPN](https://protonvpn.com/) claims to be the only free VPN with no ads and no speed limits. It offers a large range of features, such as a "kill switch" and DNS leak protection, split tunneling or traffic encryption over two locations.

ProtonVPN is based in Switzerland, outside the [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) countries. The company claims to be a [no-logs VPN provider](https://protonvpn.com/privacy-policy), except for keeping a timestamp of the last successful login attempt, to protect user accounts from password brute force attacks.

The [client software is open source](https://github.com/ProtonVPN/), anyone can review it. The Android app [contains no trackers and requires 7 permissions](https://reports.exodus-privacy.eu.org/en/reports/ch.protonvpn.android/latest/). ProtonVPN reportedly uses [dedicated servers](https://protonvpn.com/vpn-servers/).

There is a free version, however with some limitations: 1 VPN connection, severs in only 3 countries and medium speed. Access to additional features requires a paid account, which started at around €4/month at the time of writing. ProtonVPN accepts anonymous payments.


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Mullvad Mozilla VPN" width="150px"></img> </center>

## Mozilla / Mullvad

[Mullvad](https://mullvad.net/en/) claims to be a fast, trustworthy and easy-to-use VPN. Mullvad also powers [Mozilla's VPN solution](https://www.mozilla.org/en-US/products/vpn/).

The company is based in Sweden, which is part of the [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) countries. Mullvad claims to be a [no-logs VPN provider](https://mullvad.net/en/help/no-logging-data-policy/).

The [client software is open source](https://github.com/mullvad/), anyone can review it. The Android app [contains no trackers and requires 4 permissions](https://reports.exodus-privacy.eu.org/en/reports/net.mullvad.mullvadvpn/latest/). Mullvad [operates servers all over the world](https://mullvad.net/en/servers/), most of them are however rented.

At the time of writing, subscriptions started at around €5/month. Mullvad accepts anonymous payments and requires no email to sign up.


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="RiseupVPN" width="150px"></img> </center>

## RiseupVPN

[RiseupVPN](https://riseup.net/en/vpn) claims to offer censorship circumvention, location anonymization and traffic encryption.

RiseUp is a volunteer-run collective, created in 1999. The Seattle based organisation is located in a [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) country. RiseUp claims to be a [no-logs VPN provider](https://riseup.net/en/privacy-policy).

RiseupVPN’s clients are based on the open source software [Bitmask](https://bitmask.net/). The Android app [contains no trackers and requires 8 permissions](https://reports.exodus-privacy.eu.org/en/reports/se.leap.riseupvpn/latest/).

At the time of writing, RiseupVPN was free of charge. The service is entirely funded through donations. 


<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="CalyxVPN" width="150px"></img> </center>

## CalyxVPN

[CalyxVPN](https://calyx.net/) is made available by the Calyx Institute, a non-profit organisation founded in 2010. It is located in a [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) country.

The [privacy policy](https://calyxinstitute.org/legal/privacy-policy/) of the Calyx Institute claims that IP addresses may be logged, but deleted on a regular basis.

CalyxVPN's clients are based on the open source software [Bitmask](https://bitmask.net/). The Android app [contains no trackers and requires 8 permissions](https://reports.exodus-privacy.eu.org/en/reports/org.calyxinstitute.vpn/latest/).

At the time of writing, CalyxVPN was free of charge.


<br>
