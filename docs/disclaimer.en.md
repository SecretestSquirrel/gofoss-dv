---
template: main.html
title: Privacy Statement, Legal Disclaimer & License
description: Privacy Statement. What data we collect. Your rights. Legal Disclaimer. License summary. GDPR. What we'll never do with your data.
---

# Privacy Statement, Legal Disclaimer & License

## Our Privacy Statement

<center> <img src="../../assets/img/disclaimer.png" alt="privacy protection and privacy on the internet" width="700px"></img> </center>

v1.1 - October 2021

gofoss.net is committed to protecting the privacy of its users. Like everything we do, we've designed this policy to be simple and accessible to all.

This document has been originally written in English and is the only version for which gofoss.net can be held accountable.

### Definitions

* *GDPR*: General Data Protection Regulation, [EU 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1580499932731&uri=CELEX%3A32016R0679)
* *Data*: According to the GDPR, data is any information that can be used to identify a person, either directly (real name, phone number, IP address, etc.) or indirectly (any combination of the aforementioned plus device fingerprints, cookies, etc.). In the specific context of the use of our website, it is the minimum information required for the proper operation of the website.
* *Services*: the set of different software, protocols and standards used to exchange data between web applications.
* *User* or *you*: any person or third party that access gofoss.net.
* *gofoss.net*, *we* or *us*: https://gofoss.net, https://gofoss.today
* *Host*: this website is hosted by [Netlify](https://www.netlify.com/).

### Scope

This Privacy Statement applies to gofoss.net and its sub-domains. It does not extend to any websites or web services that can be accessed from our website including, but not limited to, any federated services or social media websites. These services use protocols that necessarily share or transfer data between different providers and therefore such interactions are outside the scope of this Privacy Statement.

### Consent

By accessing our website, you accept our Privacy Policy and Terms of Use.

### What data do we collect?

* We do not utilise cookies.
* We don't track you through scripts.
* We do not implement trackers on our website to analyse your behavior.
* We do not collect personally identifiable information from you, via form submission or other methods.
* We chose a host known to [respect user privacy](https://www.netlify.com/gdpr-ccpa/), which has partnered with legal experts in Europe and the US to ensure that its products and contractual commitments are in line with GDPR regulations.
* The host collects access logs including the IP addresses of site visitors, stored for less than 30 days.

### What we'll never do with your data

* We don't have anything to gain from your data.
* We do not collect any other data than what is needed to operate the website.
* We do not, in any way, process, analyse your behavior or personal characteristics to create profiles about you.
* We have no advertisements or business relationships with advertisers.
* We do not sell your data to any third party.
* We do not require any additional information that is not crucial for the operation of the website (we do not ask for phone numbers, private personal data, home address and so on).

### Your rights

Under the *GDPR* you have a number of rights with regard to your personal data:

* *Right to access* - The right to request (I) copies of your personal Data or (II) access to the information you submitted and we hold at any time.
* *Right to correct* - The right to have your Data rectified if it is inaccurate or incomplete.
* *Right to erase* - The right to request delete or remove your Data from our website.
* *Right to restrict the use of your Data* - The right to restrict processing or limit the way we use your Data.
* *Right to Data portability* - The right to move, copy or transfer your Data.
* *Right to object* - The right to object to our use of your Data.

### Changes to this Privacy Statement

Any and all changes to this *Privacy Statement* will be publicly available. We recommend that you regularly check for any changes to this Statement.

### Contact us

If you have any questions regarding this Privacy Policy or the practices of this website, please contact us by sending an email to [gofoss@protonmail.com](mailto:gofoss@protonmail.com).

<br>

## Legal Disclaimer

All the information on [https://gofoss.net](https://gofoss.net/) is published in good faith and for general information purpose only. [https://gofoss.net](https://gofoss.net/) does not make any warranties about the completeness, reliability and accuracy of this information.

No guarantees or warranties are given or implied. Any action you take upon the information you find on [https://gofoss.net](https://gofoss.net/), is strictly at your own risk. The user assumes all risks of any damages that may occur, including but not limited to loss of data, damages to hardware, or loss of business profits. [https://gofoss.net](https://gofoss.net/) will not be liable for any losses and/or damages in connection with the use of our website. Note that unless explicitly allowed by the warranty covering your device, it should be assumed that any warranty accompanying your device will be voided if you tamper with either the system software or the hardware.

From our website, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone 'bad'. Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information.

By using our website, you hereby consent to our disclaimer and agree to its terms. Should we update, amend or make any changes to this document, those changes will be prominently posted here.

<br>

## License (Summary)

If you use this site, please remember to give proper credit to [https://gofoss.net](https://gofoss.net/), and link to the license.

Copyright (c) 2020-2022 Georg Jerska <gofoss@protonmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A full copy of the GNU Affero General Public License version 3 is available at <https://www.gnu.org/licenses/agpl-3.0.txt>.

This program incorporates work covered by the following copyright and permission notice:

> Copyright (c) 2016-2022 Martin Donath <martin.donath@squidfunk.com>
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
