---
template: main.html
title: GoFOSS. Digitale Freiheit für alle
description: Digitale Freiheit für alle. Der ultimative, freie und quelloffene Leitfaden zu Online-Datenschutz, Dateneigentum und nachhaltiger Technologie.
---

# Wir glauben an Datenschutz, Privatsphäre und nachhaltige Technologie

<center> <img src="../../assets/img/about.png" alt="GoFOSS" width="400px"></img> </center>

[GoFOSS](https://gofoss.net/de/) ist ein freier und quelloffener Leitfaden den Themen Online-Datenschutz, Dateneigentum und nachhaltige Technologie. Das Projekt wurde im Jahr 2020 von einem kleinen Team von Freiwilligen ins Leben gerufen und ist zu 100% gemeinnützig – keine Reklame, kein Tracking, keine gesponsorten Inhalte, keine Werbepartner.

Wir bedauern, dass viele Technologieunternehmen ihre Gewinne auf Kosten der Privatsphäre von InternetnutzerInnen erzielen. Wir sind zunehmend besorgt über die wiederholten Eingriffe von Regierungen in die Privatsphäre der Bürger. Und wir sehen dringenden Handlungsbedarf, um technologiebedingte Umweltbelastungen einzuschränken.

Freiheit und Privatsphäre sind grundlegende Menschenrechte. Jeder sollte in der Lage sein, seine digitalen Geräte auf sichere und datenschutzfreundliche Weise zu nutzen. Im Internet surfen, soziale Netzwerke nutzen, Mediendateien teilen oder kollaborativ arbeiten; all dies sollte möglich sein, ohne dass jemand Daten ausspioniert, zu Geld macht oder zensiert.

<br>