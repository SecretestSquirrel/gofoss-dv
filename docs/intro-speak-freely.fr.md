---
template: main.html
title: Comment chiffrer sa communication
description: Qu'est ce qu'une couche de transport chiffrée ? Qu'est ce que le chiffrage de bout en bout ? Algorithmes de chiffrements ? Clé privée vs clé publique ?
---

# Parlez librement, chiffrez vos conversations

Chiffrez vos communications. Cela les rend illisibles pendant qu'elles transitent d'un appareil à un autre. Seule la personne qui reçoit sera en mesure de lire ce que vous avez à dire.

Le **Chiffrement de la couche de transport (TLS)** s'assure que votre communication ne peut être décryptée lorsqu'elle transite sur internet ou entre des antennes relais. Cependant, des intermédiaires peuvent accéder à vos données lorsqu'elles transitent chez eux et les traiter. Cela inclut beaucoup de services de messageries, de réseaux sociaux, de moteurs de recherches, de banques et bien plus encore.

<div align="center">
<img src="../../assets/img/tls.png" alt="Chiffrement de la couche de transport" width="550px"></img>
</div>

Le **Chiffrement de bout en bout** protège vos communications pendant tout le trajet. Personne à l'exception de votre destinataire final ne peut la décrypter. Même pas les intermédiaires qui font transiter les données, comme les applications de messagerie chiffrée ou bien votre fournisseur de courriel.

<div align="center">
<img src="../../assets/img/e2ee.png" alt="Chiffrement de bout en bout" width="550px"></img>
</div>

??? tip "Comment le cryptage peut aider à éviter la surveillance en ligne ?"

    <center>

    <iframe src="https://archive.org/embed/tips-to-help-avoid-online-surveillance" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Publié par la [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


## Courriels chiffrés

Peut-être avez vous déjà entendu dire que la plupart des fournisseurs de courriels:

* [vendent vos conversations privées aux publicitaires](https://www.wsj.com/articles/yahoo-bucking-industry-scans-emails-for-data-to-sell-advertisers-1535466959/)
* [participent à des programmes de surveillance globale](https://www.nytimes.com/2016/10/06/technology/yahoo-email-tech-companies-government-investigations.html/)
* [souffrent de fuites de données sévères](https://fr.wikipedia.org/wiki/Yahoo!#Vol_de_donn%C3%A9es)
* [tracent le comportement de leurs utilisateurs et utilisatrices](https://thenextweb.com/news/google-tracks-nearly-all-your-online-and-some-of-your-offline-purchases-see-it-for-yourself-here)

Néanmoins, la vaste majorité des personnes continue de placer la sécurité de leur communication entre les mains de ces fournisseurs de courriels. La raison principale ? Changer de fournisseur est intimidant. Après tout, beaucoup argumentent que leur adresse actuelle est "gratuite", connue par tout le monde et liée à tous leurs comptes et souscriptions. Heureusement, changer de fournisseur de courriel n'est pas aussi compliqué qu'il ne le semble. Dans ce chapitre nous allons discuter de comment migrer progressivement ses courriels vers un fournisseur plus respectueux de la vie privée.

<center>
<html>
<embed src="../../assets/echarts/email_stats.html" style="width: 100%; height:420px">
</html>
</center>


## Messageries chiffrées

Dans ce chapitre, nous allons aussi parler de comment chiffrer vos messages et appels. De nos jours, plusieurs applications mobile fournissent le chiffrage de bout en bout pour protéger vos communications. Les messages émis ou reçus par de telles applications ne peuvent pas être lus par des intermédiaires.

<br>