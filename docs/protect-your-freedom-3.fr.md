---
template: main.html
title: Comment les FOSS protègent votre vie privée
description: Que sont les FOSS ? Quels sont les avantages à utiliser des logiciels libres et open source ? Qu'est-ce qu'un modèle de menace ? Quelles sont les différences entre la vie privée, l'anonymat et la sécurité ?
---

# Est-ce le bon guide pour vous ?

Ce guide s'adresse à tout le monde. Il se veut sympa, pédagogique, personnalisable et accessible. Et il est 100% gratuit et open source - pas de publicité, pas de suivi, pas de contenu sponsorisé. Sur chaque page, nous indiquerons quel public est le plus susceptible d'être intéressé. Vive les logiciels libres !

<center>

| Public | Description |
| :------: | ------ |
|Les utilisateurs standards (ou « débutants »)<br><img src="../../assets/img/users.png" alt="débutants" width="30px"></img> |Ces chapitres sont destinés à celles et ceux qui n'ont pas de compétences techniques particulières, qui ont peu de temps ou pas très envie de bidouiller. |
|Les utilisatrices intermédiaires (ou « bidouilleurs »)<br><img src="../../assets/img/geeks.png" alt="geeks" width="30px"></img> |Ces chapitres sont destinés aux personnes qui ont un petit bagage technique et aiment en savoir plus sur la vie privée et les technologies. |
|Les personnes les plus avancées (ou « techies »)<br><img src="../../assets/img/techies.png" alt="techies" width="75px"></img> |Ces chapitres sont destinés aux utilisatrices qui ont de solides compétences techniques et connaissent bien les astuces et les pratiques de ce domaine. |

</center>

<br>