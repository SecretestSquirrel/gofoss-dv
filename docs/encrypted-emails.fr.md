---
template: main.html
title: Comment chiffrer vos emails ?
description: Des fournisseurs d'email respectueux de la vie privée. Est-ce que Protonmail est plus sûr que Gmail ? Protonmail vs Gmail ? Tutanota vs Protomail ? Est-ce que Tutanota est sécurisé ?
---


# Chiffrez vos courriels

!!! level "Destiné aux débutants. Aucune compétence technique particulière n'est requise."

<div align="center">
<img src="../../assets/img/https5.png" alt="Chiffrement de courriels" width="250px"></img>
</div>

Choisissez un fournisseur de courriel de confiance. Considérez avec attention des aspects comme les fonctionnalités, les technologies de chiffrement, l'emplacement des serveurs — les lois sur la vie privée changent d'un pays à l'autre. Ce chapitre fournit un aperçu (incomplet) de fournisseurs populaires et respectueux de la vie privée.


<br>


<center> <img src="../../assets/img/separator_protonmail.svg" alt="Protonmail" width="150px"></img> </center>

## Protonmail

[Protonmail](https://protonmail.com/fr/) affirme être le plus grand service de courriels sécurisés au monde, protégé par les lois suisses sur la vie privée. L'entreprise a entre autre a été financée par des investissements états-uniens (Charles River Ventures) et par l'union européenne. Le code source est [disponible ici](https://github.com/ProtonMail/).

Au moment d'écrire ces lignes, un compte utilisateur simple offre gratuitement 500 Mo de stockage. Pour 4 à 24 Euro/mois, vous aurez accès à plus d'utilisateurs et plus de stockage, ainsi qu'à pléthore de fonctionnalités: calendrier, contacts, import de courriels, paiement en bitcoins, VPN et plus encore.

??? warning "Quelques conseils sur le chiffrement"

    <center>

    | Courriels | Chiffrement|
    | ------ | ------ |
    | **Envoyés entre utilisateurs de Protonmail** | Le corps du message et les attachements sont chiffrés de bout en bout. Le sujet et les adresses des émetteur/récepteur ne le sont pas. |
    | **Envoyés de Protonmail vers d'autres fournisseurs** | Le corps du message et les attachements ne sont chiffrés que si l'utilisateur sélectionne l'option `Chiffrez vers l'extérieur`. Faute de quoi seul le chiffrement TLS est appliqué, si le fournisseur de mail du destinataire le permet (ce qui implique également que le fournisseur destinataire peut lire le mail). Dans tous les cas, le sujet et les adresses des émetteur/récepteur ne sont pas chiffrés de bout en bout.|
    | **Reçus par les utilisateurs de Protonmail d'un autre fournisseur** | Le corps du message ainsi que les attachements sont uniquement chiffrés via TLS, si le serveur de l'émetteur le permet. Le sujet et les adresses des émetteurs/récepteur ne sont pas chiffrés de bout en bout. |

    </center>


### Les clients Protonmail

Au delà de l'accès à un [webmail](https://account.protonmail.com/login/), Protonmail offre une application mobile pour Android et iOS. Sur les environnements de bureau, Protonmail s'intègre avec [Thunderbird](https://www.thunderbird.net/fr/) à travers ce qu'on appelle une application Bridge. Cela est néanmoins limité aux comptes payants. Une alternative consiste à utiliser [ElectronMail](https://github.com/vladimiry/ElectronMail), qui est un client libre et open-source pour Protonmail. Gardez néanmoins en tête que ce n'est pas une application officielle. Vous trouverez des informations détaillées ci-dessous.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

         Téléchargez l'application Protonmail depuis le [Google Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=fr&gl=FR) ou [Aurora Store](https://auroraoss.com/). Elle [contient 0 traqueurs et requiert 14 permissions](https://reports.exodus-privacy.eu.org/fr/reports/ch.protonmail.android/latest/). À titre de comparaison: pour Gmail, c'est 1 traqueur et 55 permissions ; pour Outlook, c'est 13 traqueurs et 49 permissions ; et pour Hotmail, c'est 4 traqueurs et 31 permissions.


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

         Téléchargez l'application Protonmail depuis l'[App Store](https://apps.apple.com/fr/app/protonmail-encrypted-email/id979659905/).


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour ElectronMail sous Windows (un compte payant n'est pas nécessaire)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Télécharger ElectronMail | Téléchargez et lancez l'[installateur ElectronMail pour Windows](https://github.com/vladimiry/ElectronMail/releases). |
        | Créer un mot de passe principal | Ouvrez ElectronMail et saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour protéger vos courriels. |
        | Login | Fournissez vos identifiants Protonmail, y compris l'authentification à deux facteurs si celle-ci est active. |
        | Domaine | Choisissez un domaine dans la liste. Il y a même une option `Onion` pour utiliser Tor. Puis cliquez sur `Fermer`. |

        </center>


    ??? tip "Montrez-moi le guide étape par étape pour Thunderbird sous Windows (pour comptes payants uniquement)"

        ### Installer Thunderbird sous Windows

        Allez sur [la page de téléchargement de Thunderbird](https://www.thunderbird.net/fr/) et cliquez sur le bouton "Téléchargement gratuit". Une fois le programme d'installation téléchargé, cliquez sur le bouton "Exécuter" et suivez l'assistant d'installation.

        ### Installer Protonmail Bridge sous Windows

        Thunderbird s'intègre parfaitement à Protonmail, et veille à ce que vos courriels restent cryptés lorsqu'ils entrent et sortent de votre ordinateur. Cette tâche est confiée à l'application Bridge, disponible uniquement pour les utilisateurs payants. [Téléchargez Protonmail Bridge pour Windows](https://protonmail.com/bridge/download). Une fois le programme d'installation téléchargé, cliquez sur le bouton "Exécuter" et suivez l'assistant d'installation.

        ### Configurer Protonmail Bridge sous Windows

        Ouvrez l'application Protonmail Bridge tout juste installée et suivez l'assistant de configuration :

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez-vous à votre compte Protonmail. |
        | 2 | Cliquez sur le nom de votre compte, puis sur le bouton `Mailbox configuration` (en anglais). |
        | 3 | Une fenêtre avec le titre `Protonmail Bridge Mailbox Configuration` (en anglais) devrait apparaître. Elle affiche les paramètres IMAP et SMTP, y compris un mot de passe, nécessaire plus tard pour configurer Thunderbird. |

        </center>

        ### Configurer Thunderbird sous Windows

        Finalement, lancez Thunderbird, naviguez vers `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant de configuration :

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Votre nom complet | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse Protonmail. |
        | Mot de passe | Copiez et collez le mot de passe depuis la fenêtre `Protonmail Bridge Mailbox Configuration` (n'entrez pas votre mot de passe Protonmail, cela ne fonctionnera pas). |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configurer manuellement | Cliquez sur le bouton `Configuration manuelle`, et saisissez les paramètres IMAP et SMTP fournis dans la fenêtre `Protonmail Bridge Mailbox Configuration` (pour l'authentification, sélectionnez `Mot de passe normal`). |
        | Re-tester | Cliquez sur le bouton `Re-tester` pour vérifier vos paramètres de connexion. |
        | Configuration avancée | Cliquez sur le bouton `Configuration avancée`. Une nouvelle fenêtre apparaît. Cliquez simplement sur le bouton `OK`, ne modifiez aucun paramètre dans cette fenêtre. |
        | Ajouter une exception de sécurité | Cliquez sur le bouton `Confirmer l'exception de sécurité` dans la fenêtre qui s'ouvre. Cela confirme que votre ordinateur (127.0.0.1) peut exécuter l'application Bridge. Vous devrez peut-être confirmer une deuxième exception de sécurité plus tard, lorsque vous envoyez votre premier courriel. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative pour Thunderbird (3min, pour comptes payants uniquement)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Publié par Protonmail. Les instructions devraient s'appliquer de la même manière sous macOS ou Linux.*

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour ElectronMail sous macOS (un compte payant n'est pas nécessaire)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Télécharger ElectronMail | Téléchargez l'[image disque d'ElectronMail pour macOS](https://github.com/vladimiry/ElectronMail/releases), ouvrez-la et faites glisser l'icône d'ElectronMail au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône ElectronMail vers le dock. |
        | Créer un mot de passe principal | Ouvrez ElectronMail et saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour protéger vos courriels. |
        | Login | Fournissez vos identifiants Protonmail, y compris l'authentification à deux facteurs si celle-ci est active. |
        | Domaine | Choisissez un domaine dans la liste. Il y a même une option `Onion` pour utiliser Tor. Puis cliquez sur `Fermer`.  |

        </center>


    ??? tip "Montrez-moi le guide étape par étape pour Thunderbird sous macOS (pour comptes payants uniquement)"

        ### Installer Thunderbird sous macOS

        Allez sur [la page de téléchargement de Thunderbird](https://www.thunderbird.net/fr/) et cliquez sur le bouton "Téléchargement gratuit". Une fois le programme d'installation téléchargé, il devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Thunderbird. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Thunderbird au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Thunderbird vers le dock.

        ### Installer Protonmail Bridge sous macOS

        Thunderbird s'intègre parfaitement à Protonmail, et veille à ce que vos courriels restent cryptés lorsqu'ils entrent et sortent de votre ordinateur. Cette tâche est confiée à l'application Bridge, disponible uniquement pour les utilisateurs payants. [Téléchargez Protonmail Bridge pour macOS](https://protonmail.com/bridge/download). Une fois le programme d'installation téléchargé, il devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Protonmail Bridge. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Protonmail Bridge au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Protonmail Bridge vers le dock.

        ### Configurer Protonmail Bridge sous macOS

        Ouvrez l'application Protonmail Bridge tout juste installée et suivez l'assistant de configuration :

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez-vous à votre compte Protonmail. |
        | 2 | Cliquez sur le nom de votre compte, puis sur le bouton `Mailbox configuration` (en anglais). |
        | 3 | Une fenêtre avec le titre `Protonmail Bridge Mailbox Configuration` (en anglais) devrait apparaître. Elle affiche les paramètres IMAP et SMTP, y compris un mot de passe, nécessaire plus tard pour configurer Thunderbird. |

        </center>

        ### Configurer Thunderbird sous macOS

        Finalement, lancez Thunderbird, naviguez vers `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant de configuration :

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Votre nom complet | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse Protonmail. |
        | Mot de passe | Copiez et collez le mot de passe depuis la fenêtre `Protonmail Bridge Mailbox Configuration` (n'entrez pas votre mot de passe Protonmail, cela ne fonctionnera pas). |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configurer manuellement | Cliquez sur le bouton `Configuration manuelle`, et saisissez les paramètres IMAP et SMTP fournis dans la fenêtre `Protonmail Bridge Mailbox Configuration` (pour l'authentification, sélectionnez `Mot de passe normal`). |
        | Re-tester | Cliquez sur le bouton `Re-tester` pour vérifier vos paramètres de connexion. |
        | Configuration avancée | Cliquez sur le bouton `Configuration avancée`. Une nouvelle fenêtre apparaît. Cliquez simplement sur le bouton `OK`, ne modifiez aucun paramètre dans cette fenêtre. |
        | Ajouter une exception de sécurité | Cliquez sur le bouton `Confirmer l'exception de sécurité` dans la fenêtre qui s'ouvre. Cela confirme que votre ordinateur (127.0.0.1) peut exécuter l'application Bridge. Vous devrez peut-être confirmer une deuxième exception de sécurité plus tard, lorsque vous envoyez votre premier courriel. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative pour Thunderbird (3min, pour comptes payants uniquement)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Publié par Protonmail. Les instructions devraient s'appliquer de la même manière sous macOS ou Linux.*

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour ElectronMail sous Ubuntu Linux (un compte payant n'est pas nécessaire)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Télécharger ElectronMail | Téléchargez le dernier [paquet `.deb` d'ElectronMail](https://github.com/vladimiry/ElectronMail/releases). Le fichier devrait s'appeler `electron-mail-X-XX-X-linux-amd64.deb`. Supposons qu'il ait été téléchargé dans le dossier `/home/gofoss/Downloads`. Assurez-vous d'ajuster ces chemins de fichiers en fonction de votre propre configuration. Maintenant ouvrez le terminal avec le raccourci `Ctrl+Alt+T` ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Enfin, exécutez les commandes suivantes:<br><br> `cd /home/gofoss/Downloads` <br>`sudo dpkg -i electron-mail-X-XX-X-linux-amd64.deb` |
        | Créer un mot de passe principal | Ouvrez ElectronMail et saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour protéger vos courriels. |
        | Login | Fournissez vos identifiants Protonmail, y compris l'authentification à deux facteurs si celle-ci est active. |
        | Domaine | Choisissez un domaine dans la liste. Il y a même une option `Onion` pour utiliser Tor. Puis cliquez sur `Fermer`.  |

        </center>


    ??? tip "Montrez-moi le guide étape par étape pour Thunderbird sous Ubuntu Linux (pour comptes payants uniquement)"

        ### Installer Thunderbird sous Linux

        Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez la commande suivante pour installer Thunderbird :

        ```bash
        sudo apt install thunderbird
        ```

        ### Installer Protonmail Bridge sous Linux

        Thunderbird s'intègre parfaitement à Protonmail, et veille à ce que vos courriels restent cryptés lorsqu'ils entrent et sortent de votre ordinateur. Cette tâche est confiée à l'application Bridge, disponible uniquement pour les utilisateurs payants. [Téléchargez Protonmail Bridge pour Linux](https://protonmail.com/bridge/download). Le fichier devrait s'appeler `protonmail-bridge_X.X.X-X_amd64.deb`. Supposons qu'il ait été téléchargé dans le dossier `/home/gofoss/Downloads`. Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes (n'oubliez pas d'ajuster le nom du fichier et le chemin du dossier de téléchargement en conséquence) :

        ```bash
        sudo apt install gdebi
        cd /home/gofoss/Downloads
        sudo gdebi protonmail-bridge_X.X.X-X_amd64.deb
        ```

        ### Configurer Protonmail Bridge sous Linux

        Ouvrez l'application Bridge avec la commande `protonmail-bridge`, ou cliquez sur le bouton `Applications` en haut à gauche, et recherchez `ProtonMail Bridge`. Suivez ensuite l'assistant d'installation :

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez-vous à votre compte Protonmail. |
        | 2 | Cliquez sur le nom de votre compte, puis sur le bouton `Mailbox configuration` (en anglais). |
        | 3 | Une fenêtre avec le titre `Protonmail Bridge Mailbox Configuration` (en anglais) devrait apparaître. Elle affiche les paramètres IMAP et SMTP, y compris un mot de passe, nécessaire plus tard pour configurer Thunderbird. |

        </center>

        ### Configurer Thunderbird sous Linux

        Finalement, lancez Thunderbird, naviguez vers `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant de configuration :

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Votre nom complet | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse Protonmail. |
        | Mot de passe | Copiez et collez le mot de passe depuis la fenêtre `Protonmail Bridge Mailbox Configuration` (n'entrez pas votre mot de passe Protonmail, cela ne fonctionnera pas). |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configurer manuellement | Cliquez sur le bouton `Configuration manuelle`, et saisissez les paramètres IMAP et SMTP fournis dans la fenêtre `Protonmail Bridge Mailbox Configuration` (pour l'authentification, sélectionnez `Mot de passe normal`). |
        | Re-tester | Cliquez sur le bouton `Re-tester` pour vérifier vos paramètres de connexion. |
        | Configuration avancée | Cliquez sur le bouton `Configuration avancée`. Une nouvelle fenêtre apparaît. Cliquez simplement sur le bouton `OK`, ne modifiez aucun paramètre dans cette fenêtre. |
        | Ajouter une exception de sécurité | Cliquez sur le bouton `Confirmer l'exception de sécurité` dans la fenêtre qui s'ouvre. Cela confirme que votre ordinateur (127.0.0.1) peut exécuter l'application Bridge. Vous devrez peut-être confirmer une deuxième exception de sécurité plus tard, lorsque vous envoyez votre premier courriel. |

        </center>


    ??? tip "Montrez-moi une vidéo récapitulative pour Thunderbird (3min, paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Publié par Protonmail. Les instructions devraient s'appliquer de la même manière sous macOS ou Linux.*

        </center>


<br>


<center> <img src="../../assets/img/separator_tutanota.svg" alt="Tutanota" width="150px"></img> </center>

## Tutanota

[Tutanota](https://tutanota.com/fr/) est un service de messagerie électronique sécurisé et "freemium", enregistré en Allemagne. Tout est crypté de bout en bout et [open-source](https://github.com/tutao/tutanota/). Tutanota utilise sa propre norme de chiffrage et ne prend pas en charge PGP.

Au moment d'écrire ces lignes, le compte de base offre gratuitement 1 Go de stockage. Pour environ 1 à 6 Euros/mois, vous avez accès à plus d'utilisateurs et de stockage, ainsi qu'à une pléthore de fonctionnalités : domaines personnalisés, recherche illimitée, calendriers multiples, règles de boîte de réception, label blanc, partage de calendrier, etc. L'importation de courriels et le paiement anonyme ne sont actuellement pas supportés.

### Clients Tutanota

Outre l'accès au [webmail](https://mail.tutanota.com/?r=/login/), Tutanota propose des applications mobiles pour Android et iOS. Pour les environnements de bureau, Tutanota a développé son propre client. Des instructions plus détaillées sont fournies ci-dessous.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

         Téléchargez l'application Tutanota sur [Google Play Store](https://play.google.com/store/apps/details?id=de.tutao.tutanota&hl=fr&gl=FR) ou [Aurora Store](https://auroraoss.com/). Tutanota est également disponible sur [F-Droid](https://f-droid.org/fr/packages/de.tutao.tutanota/). Vous pouvez également visiter la [page de téléchargement](https://tutanota.com/fr/#download) ou le [dépôt Github](https://github.com/tutao/tutanota/releases/) de Tutanota pour télécharger et installer le fichier `.apk`. L'application [contient 0 traqueurs et nécessite 9 permissions](https://reports.exodus-privacy.eu.org/fr/reports/de.tutao.tutanota/latest/). À titre de comparaison : pour Gmail, il y a 1 traqueur et 55 permissions ; pour Outlook, il y a 13 traqueurs et 49 permissions ; et pour Hotmail, il y a 4 traqueurs et 31 permissions.


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

         Téléchargez l'application Tutanota sur l'[App Store](https://apps.apple.com/fr/app/tutanota/id922429609).


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        Téléchargez [le programme d'installation](https://mail.tutanota.com/desktop/tutanota-desktop-win.exe), puis cliquez sur le bouton `Exécuter` pour suivre l'assistant d'installation.


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        [Téléchargez le programme d'installation](https://mail.tutanota.com/desktop/tutanota-desktop-mac.dmg), qui devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Tutanota. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Tutanota au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Tutanota vers le dock.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        [Télécharger le programme d'installation](https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage), qui devrait s'appeler `tutanota-desktop-linux.AppImage`. Supposons qu'il ait été téléchargé dans le dossier `/home/gofoss/Downloads`. Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes (n'oubliez pas d'ajuster le nom du fichier et le chemin du dossier de téléchargement en conséquence) :

        ```bash
        cd /home/gofoss/Downloads
        chmod +x tutanota-desktop-linux.AppImage
        ```

    ??? tip "Montrez-moi comment épingler Tutanota au dock Ubuntu"

        Ce n'est pas évident, mais le lanceur de Tutanota peut être ajouté au menu des applications d'Ubuntu et épinglé au dock. Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez la commande suivante :

        ```bash
        sudo gedit /usr/share/applications/tutanota.desktop
        ```

        Insérez le contenu suivant dans le fichier nouvellement créé. Assurez-vous de faire pointer le chemin `Exec` vers le dossier contenant l'AppImage téléchargé :

        ```
        #!/usr/bin/env xdg-open
        [Desktop Entry]
        Version=1.0
        Type=Application
        Terminal=false
        Exec=/home/gofoss/Downloads/tutanota-desktop-linux.AppImage
        Name=Tutanota
        ```

        Rendez le fichier exécutable :

        ```bash
        sudo chmod +x /usr/share/applications/tutanota.desktop
        ```

        Déconnectez-vous, puis reconnectez-vous à votre session Ubuntu. Vous devriez maintenant être en mesure de lancer Tutanota à partir du menu des applications et de l'épingler au dock.


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Fournisseurs de courriel" width="150px"></img> </center>

## Autres fournisseurs

=== "Disroot"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[disroot. org](https://disroot.org/fr/services/email) |
    |Prix | Le compte de base est gratuit (1 Go de stockage) ; le stockage supplémentaire coûte 0,15 Euros par Go et par mois. |
    |Fonctionnalités |Plateforme fournissant des services en ligne basés sur les principes de liberté, de confidentialité, de fédération et de décentralisation. Située aux Pays-Bas. Accepte le *bitcoin* et le *faircoin*. Cryptage complet du disque et du courrier électronique. Application mobile. |
    |Faiblesses | Peut potentiellement décrypter les données de ses utilisateurs, car les courriels sont apparemment stockés en texte clair. |


=== "Mailbox"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[mailbox. org](https://mailbox.org/en/) |
    |Prix | 1 Euro/mois, 2 Go de stockage. |
    |Fonctionnalités |Fournisseur de courriel allemand et open source, dont les serveurs sont situés à Berlin. Offre des fonctions de sécurité telles que le cryptage au repos, PGP, DANE, SPF et DKIM, ainsi que l'authentification à deux facteurs, la recherche plein texte, les calendriers, les carnets d'adresses et les listes de tâches, ainsi que la synchronisation CalDAV et CardDAV. |
    |Faiblesses| Pas de client mobile, nécessite des clients tiers. |


=== "Posteo"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[posteo.de](https://posteo.de/fr/) |
    |Prix | 1 Euro/mois, 2 Go de stockage. |
    |Fonctionnalités |Fournisseur de courriel allemand et open source. Autofinancé, cryptage au repos, authentification à deux facteurs, calendriers et carnets d'adresses, synchronisation CalDAV et CardDAV. |
    |Faiblesses| Pas de dossier spam, pas de version d'essai ou gratuite. |


=== "Kolab Now"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[kolabnow.com](https://kolabnow.com/) |
    |Prix | 5 Euros/mois, 2 Go de stockage. |
    |Fonctionnalités |Fournisseur de courriel suisse et open source. Recherche de texte, étiquetage, filtres, carnets d'adresses, calendriers, synchronisation CalDAV et CardDAV. |
    |Faiblesses| Pas de cryptage de bout en bout, pas de cryptage au repos. |


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="Transfert d'e-mail" width="150px"></img> </center>

## Période de transition

=== "Transférez vos courriels"

    La transition vers un nouveau compte de messagerie peut prendre un certain temps. Il est préférable de garder vos anciens comptes en vie pendant un certain temps pour être sûr de ne rien rater. Il suffit de transférer tout message entrant vers le nouveau compte. Pour plus d'information sur le transfert de courriel, consultez les pages de documentation de [Gmail](https://support.google.com/mail/answer/10957?hl=fr/), [Outlook](https://support.microsoft.com/fr-fr/office/cr%C3%A9er-et-transf%C3%A9rer-des-messages-%C3%A9lectroniques-et-y-r%C3%A9pondre-dans-outlook-sur-le-web-ecafbc06-e812-4b9e-a7af-5074a9c7abd0?redirectsourcepath=%252fen-us%252farticle%252fforward-email-from-office-365-to-another-email-account-1ed4ee1e-74f8-4f53-a174-86b748ff6a0e%252f), [iCloud](https://www.idownloadblog.com/2019/01/28/set-up-icloud-email-forwarding/), [Yahoo](https://www.wikihow.com/Forward-Yahoo-Mail), etc.


=== "Mettez à jour vos abonnements"

    Profitez de cette période de transition pour repérer tout abonnement actif dans vos anciens comptes de messagerie, afin de mettre à jour votre nouvelle adresse électronique !


=== "Informez vos contacts"

    N'oubliez pas de communiquer votre nouvelle adresse électronique à vos contacts personnels et professionnels, à vos banques, à vos assurances, à l'administration fiscale, etc. Vous pouvez également configurer un message de réponse automatique sur votre ancien compte pour informer les gens de votre changement d'adresse.


=== "Résilier vos anciens comptes"

    Au fil du temps, de moins en moins de courriels atterriront dans votre ancienne boîte de réception. Elle finira par devenir inactive. C'est à ce moment-là que vous pourriez envisager de résilier votre ancien compte de messagerie.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Protonmail et Tutanota" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions :

* référez-vous à la [documentation de Protonmail](https://protonmail.com/support/) ou à la [documentation de Thunderbird](https://support.mozilla.org/fr/products/thunderbird). Vous pouvez également demander de l'aide à la [communauté Protonmail](https://teddit.net/r/ProtonMail/) ou à la [communauté Thunderbird](https://support.mozilla.org/fr/questions/new/thunderbird).

* référez-vous à la [documentation de Tutanota](https://tutanota.com/howto/#install-desktop/) ou demandez de l'aide à la [communauté Tutanota](https://teddit.net/r/tutanota/).

<br>

<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/email_reply.png" width="450px" alt="Courriels sécurisés"></img>
</div>

<br>