<!--
Thanks for taking the time to submit a merge request.

Before working on a merge request, make sure to:

1. search for similar issues in the Issue Tracker: https://gitlab.com/curlycrixus/gofoss/-/issues

2. get in contact with the development team via [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today), [Email](mailto:gofoss@protonmail.com), [Reddit](https://teddit.net/u/gofosstoday/) or [Matrix](https://matrix.to/#/#gofoss-community:matrix.org/)
-->

# Merge request

:twisted_rightwards_arrows: Create a merge request to help us improve!

## Checklist

<!--
Please make sure that you verify each checkbox below.
-->

- [ ] This merge request is not a duplicate, I've searched the Issue Tracker
- [ ] This merge request has been discussed with the development team
- [ ] This is a merge request, and not a question, a content request, a feature request, or anything other
- [ ] This merge request uses the correct target branch, i.e. the `development` branch
- [ ] The option `Delete source branch when merge request is accepted` has been selected

## What is the proposed change?

<!--
Provide a clear and concise description of the implemented changes. Add any relevant screenshots, if available.

For example, "I've added a new section covering the Session app".

Or, "I've changed the dark theme to enhance accessibility".
-->

## What type of change?

<!--
Please make sure to select the relevant checkboxes below.
-->

- [ ] Provides new content (`content` label)
- [ ] Adjusts missing or incorrect content (`content` label)
- [ ] Applies spelling, grammar or other readability improvements (`content` label)
- [ ] Fixes broken links (`content` label)
- [ ] Improves website navigation (`feature` label)
- [ ] Enhances website layout (`feature` label)
- [ ] Adds new illustrations (`feature` label)
- [ ] Adds new website features (`feature` label)
- [ ] Fixes a bug on the website (`bug` label)
- [ ] Other changes

## Link to related Issues or Merge Requests?

<!--
Please provide links to all related Issues or Merge Requests.
-->

## Testing

<!--
Please make sure to select the relevant checkboxes below.
-->

This merge request has been tested in the following browser(s):

- [ ] Firefox
- [ ] Chrome
- [ ] Safari
- [ ] Edge
- [ ] Android Firefox
- [ ] Android Chrome
- [ ] iOS Safari
- [ ] iOS Chrome


## Do you have additional comments?

<!--
Provide any other comments you might have.
-->

