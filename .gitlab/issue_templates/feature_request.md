<!--
Thanks for taking the time to submit an issue.

Before opening a new issue, make sure to search for similar issues in the Issue Tracker:

https://gitlab.com/curlycrixus/gofoss/-/issues

There's a good chance somebody else had the same issue or feature proposal.

If nothing similar shows up, please fill out each section below.
-->

# Feature request

:rocket: Suggest ideas for new website features!

## Checklist

<!--
Please make sure that you verify each checkbox below.
-->

- [ ] This issue is not a duplicate, I've searched the Issue Tracker
- [ ] This issue is not a question, I'm aware that I can submit questions via [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today), [Email](mailto:gofoss@protonmail.com), [Reddit](https://teddit.net/u/gofosstoday/) or [Matrix](https://matrix.to/#/#gofoss-community:matrix.org/)
- [ ] This issue is a feature request, and not a bug report, content request, or anything other

## What are you missing or what could be improved?

<!--
Provide a clear and concise description of the missing or problematic feature.

For example, "I have difficulties to read the current website".
-->

## What is the solution you'd like to see?

<!--
Provide a clear and concise description of what you want to happen.

Ideally, also provide other solutions you've tried or researched.

For example, "I'd suggest to implement a dark mode. Similar to the one in MkDocs Material: https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/".
-->

## Do you have additional comments?

<!--
Provide any other comments you might have.

You might also want to link to related issues here
-->

/label ~feature
