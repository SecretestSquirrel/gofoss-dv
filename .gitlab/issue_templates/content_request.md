<!--
Thanks for taking the time to submit an issue.

Before opening a new issue, make sure to search for similar issues in the Issue Tracker:

https://gitlab.com/curlycrixus/gofoss/-/issues

There's a good chance somebody else had the same issue or feature proposal.

If nothing similar shows up, please fill out each section below.
-->

# Content request

:pencil2: Suggest ideas for new content!


## Checklist

<!--
Please make sure that you verify each checkbox below.
-->

- [ ] This issue is not a duplicate, I've searched the Issue Tracker
- [ ] This issue is not a question, I'm aware that I can submit questions via [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today), [Email](mailto:gofoss@protonmail.com), [Reddit](https://teddit.net/u/gofosstoday/) or [Matrix](https://matrix.to/#/#gofoss-community:matrix.org/)
- [ ] This issue is a content request, and not a bug report, feature request, or anything other

## What are you missing or what could be improved?

<!--
Provide a clear and concise description of the missing or problematic content.

For example, "I would love to see other Linux distributions recommended like Pop!_OS, Mint and Fedora."

Or for example, "The second paragraph in section Secure Domain is not clear enough. There is also a spelling mistake in the last paragraph".
-->

## What is the solution you'd like to see?

<!--
Provide a clear and concise description of what you want to happen.

Ideally, also provide specific wording to add new content, increase readability or fix mistakes.

For example, "I'd suggest to add a dedicated section for other Linux distributions. It could be called <Other beginner friendly Linux distributions>".

Or for example, "The sentence should read: "The easiest way to discover Ubuntu is to run it alongside your current operating system", rather than "The simplest possibility to test Ubuntu is to have it installed with Windows on the same machine."
-->

## Do you have additional comments?

<!--
Provide any other comments you might have.

You might also want to link to related issues here.
-->

/label ~content
