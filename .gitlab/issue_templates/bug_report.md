<!--
Thanks for taking the time to submit an issue.

Before opening a new issue, make sure to search for similar issues in the Issue Tracker:

https://gitlab.com/curlycrixus/gofoss/-/issues

There's a good chance somebody else had the same issue or feature proposal.

If nothing similar shows up, please fill out each section below.
-->

# Bug report

:beetle: Create a report to help us improve!

## Checklist

<!--
Please make sure that you verify each checkbox below.
-->

- [ ] This issue is not a duplicate, I've searched the Issue Tracker
- [ ] This issue is not a question, I'm aware that I can submit questions via [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today), [Email](mailto:gofoss@protonmail.com), [Reddit](https://teddit.net/u/gofosstoday/) or [Matrix](https://matrix.to/#/#gofoss-community:matrix.org/)
- [ ] This issue is a bug report, and not a content request, feature request, or anything other

## What is the bug behavior?

<!--
Provide a clear and concise description of the bug encountered. Add any relevant screenshots, if available.

For example, "Whenever I browse to the GoFOSS website, I'm redirected to a 404 page. See also screenshot below".
-->

## What is the expected correct behavior?

<!--
Provide a clear and concise description of what you should see instead.

For example, "When I browse to the GoFOSS website, I expect to see the project's landing page".
-->

## What are the steps to reproduce the bug?

<!--
Provide a clear and concise description of how one can reproduce the bug. Please specify which device(s) you're using, and provide a step-by-step reproduction scenario.

For example,

1. I open the browser on my iPhone 13 Pro Max
2. I navigate to https://gofoss.today
3. I see a 404 page
-->

## What are possible fixes?

<!--
If you can, provide a suggestion to what might be responsible for the problem.

For example, "I think that Netlify is currently down".
-->


## Do you have additional comments?

<!--
Provide any other comments you might have.

You might also want to link to related issues here.
-->

/label ~bug
