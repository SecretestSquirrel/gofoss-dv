################
# PROJECT INFO #
################

site_name: GoFOSS
site_url: https://gofoss.net
site_author: Georg Jerska
site_description:
  GoFOSS - Digital freedom for all. The ultimate, free and open source guide to online privacy, data ownership and durable technology.
copyright: © 2020 - 2022, Georg Jerska. Released with ❤️ under <a href="https://www.gnu.org/licenses/agpl-3.0.txt">aGPLv3</a>.
edit_uri: ""


################
#    THEME     #
################
# Load material theme, with theme extension located in `material/overrides` & with light/dark mode switch
# Load custom scheme incl. colors/layout & local fonts, as defined in 'docs/assets/css/layout.css'
# Disable Google Fonts CDN
# Configure favicon & logo, stored in 'docs/assets/img'
# Enable instant loading, back-to-top button & integrated table of contents

theme:
  name: material
  custom_dir: overrides
  palette:
    - scheme: gofoss
      toggle:
        icon: material/toggle-switch-off-outline
        name: Switch to dark mode
    - scheme: slate
      toggle:
        icon: material/toggle-switch
        name: Switch to light mode
  font: false
  favicon: assets/img/gofoss_favicon.png
  logo: assets/img/gofoss_favicon.png
  features:
    - navigation.top


#####################
#    NAVIGATION     #
#####################

nav:
  - Home: index.md
  - Get started: 
    - Why bother?: protect-your-freedom-1.md
    - What's in it for you?: protect-your-freedom-2.md
    - Is this guide right for you?: protect-your-freedom-3.md
  - Browse privately:
    - Free your browser: intro-browse-freely.md
    - Firefox: firefox.md
    - Tor Browser: tor.md
    - VPN: vpn.md
  - Speak freely:
    - Keep conversations private: intro-speak-freely.md
    - Encrypted emails: encrypted-emails.md
    - Encrypted messages: encrypted-messages.md
  - Store safely:
    - Secure your data: intro-store-safely.md
    - Safe passwords: passwords.md
    - Backups: backups.md
    - Encrypted files: encrypted-files.md
  - Stay mobile & free:
    - Free your phone: intro-free-your-phone.md
    - FOSS apps: foss-apps.md
    - CalyxOS: calyxos.md
    - LineageOS for microG: lineageos.md
  - Unlock your computer:
    - Free your computer: intro-free-your-computer.md
    - Ubuntu: ubuntu.md
    - Ubuntu apps: ubuntu-apps.md
  - Own your cloud:
    - Free your cloud: intro-free-your-cloud.md
    - Fediverse: fediverse.md
    - Alternative cloud providers: cloud-providers.md
    - Server hosting: ubuntu-server.md
    - Basic server security: server-hardening-basics.md
    - Advanced server security: server-hardening-advanced.md
    - Secure domain: secure-domain.md
    - Cloud storage: cloud-storage.md
    - Photo gallery: photo-gallery.md
    - Contacts, calendars & tasks: contacts-calendars-tasks.md
    - Media streaming: media-streaming.md
    - Server backups: server-backups.md
  - About:
    - Why digital freedom matters: nothing-to-hide.md
    - The project: about.md
    - The team: team.md
    - Thanks: thanks.md
    - Contributing: contributing.md
    - Roadmap: roadmap.md
    - Disclaimer: disclaimer.md


#################
# CUSTOMISATION #
#################
# Define color schemes, titles, links, fonts, content area width & admonitions
# Enable social links in footer
# Enable multi-language selector

extra_css:
  - assets/css/layout.css

extra:
  social:
  - icon: fontawesome/brands/gitlab
    link: https://gitlab.com/curlycrixus/gofoss
    name: Get source code
  - icon: fontawesome/solid/donate
    link: https://liberapay.com/GeorgJerska/donate
    name: Donate with Liberapay
  - icon: fontawesome/brands/mastodon
    link: https://hostux.social/@don_atoms
    name: Mastodon
  - icon: fontawesome/brands/twitter
    link: https://nitter.net/gofoss_today
    name: Twitter
  - icon: fontawesome/brands/reddit
    link: https://teddit.net/u/gofosstoday/
    name: Reddit
  - icon: fontawesome/solid/users
    link: https://lemmy.ml/u/gofoss
    name: Lemmy
  - icon: material/matrix
    link: https://matrix.to/#/#gofoss-community:matrix.org
    name: Matrix
  - icon: fontawesome/brands/youtube
    link: https://libre.video/video-channels/gofoss.today
    name: PeerTube
  - icon: fontawesome/solid/envelope
    link: mailto:gofoss@protonmail.com
    name: Email
  alternate:
    # Switch to English
    - name: English
      link: /
      lang: en
    # Switch to French
    - name: Français
      link: /fr/
      lang: fr  
    # Switch to German
    - name: Deutsch
      link: /de/
      lang: de    


################
#   PLUGINS    #
################
# Enable multi-language support

plugins:
  - minify:
      minify_html: true
  - search:
      lang:
        - en
        - fr
        - de
  - i18n:
      default_language: en
      languages:
        fr: français
        de: deutsch
      nav_translations:
        fr:
          Home: Accueil
          Get started: Introduction
          Why bother?: Pourquoi s'inquiéter ?
          What's in it for you?: Qu'est-ce que vous y gagnez ?
          Is this guide right for you?: Est-ce le bon guide pour vous ?
          Browse privately: Navigez en mode privé
          Free your browser: Libérez votre navigateur
          Firefox: Firefox
          Tor Browser: Tor
          VPN: VPN
          Speak freely: Parlez librement
          Keep conversations private: Conversations confidentielles
          Encrypted emails: Courriels chiffrés
          Encrypted messages: Messages chiffrés
          Fediverse: Fediverse
          Store safely: Stockez en toute sécurité
          Secure your data: Données sécurisées
          Safe passwords: Mots de passes sécurisés
          Backups: Sauvegardes
          Encrypted files: Fichiers chiffrés
          Stay mobile & free: Restez mobile & libre
          Free your phone: Libérez votre téléphone
          FOSS apps: Logiciels libres
          CalyxOS: CalyxOS
          LineageOS for microG: LineageOS avec microG
          Unlock your computer: Déchainez votre ordinateur
          Free your computer: Libérez votre ordinateur
          Ubuntu: Ubuntu
          Ubuntu apps: Logiciels libres
          Own your cloud: Contrôlez votre cloud
          Free your cloud: Libérez votre cloud
          Alternative cloud providers: Fournisseurs alternatifs
          Server hosting: Hébergement de serveur
          Basic server security: Sécurisation basique du serveur
          Advanced server security: Sécurisation avancée du serveur
          Secure domain: Domaine sécurisé
          Cloud storage: Stockage en ligne
          Photo gallery: Photothèque
          Contacts, calendars & tasks: Contacts, agendas & liste de tâches
          Media streaming: Diffusion de médias
          Server backups: Sauvegarde du serveur
          About: Sur nous
          Why digital freedom matters: L'importance de la liberté numérique
          The project: Le projet
          The team: L'équipe
          Thanks: Remerciements
          Contributing: Participer
          Roadmap: Feuille de route
          Disclaimer: Mentions légales
        de:
          Home: Startseite
          Get started: Erste Schritte
          Why bother?: Was geht mich das an?
          What's in it for you?: Was springt für mich dabei raus?
          Is this guide right for you?: Ist dieser Leitfaden der richtige?
          Browse privately: Surft sicher im Netz
          Free your browser: Befreit Euren Browser
          Firefox: Firefox
          Tor Browser: Tor
          VPN: VPN
          Speak freely: Führt vertrauliche Gespräche
          Keep conversations private: Private Unterhaltungen
          Encrypted emails: Verschlüsselte E-Mails
          Encrypted messages: Verschlüsselte Nachrichten
          Fediverse: Fediverse
          Store safely: Sichert Eure Daten
          Secure your data: Sichere Datenspeicherung
          Safe passwords: Sichere Passwörter
          Backups: Datensicherung
          Encrypted files: Verschlüsselte Dateien
          Stay mobile & free: Bleibt frei und mobil
          Free your phone: Befreit Euer Smartphone
          FOSS apps: Freie Apps
          CalyxOS: CalyxOS
          LineageOS for microG: LineageOS für microG
          Unlock your computer: Entfesselt Eure Rechner
          Free your computer: Befreit Eure Rechner
          Ubuntu: Ubuntu
          Ubuntu apps: Linux Software
          Own your cloud: Erobert die Cloud
          Free your cloud: Befreit Eure Cloud
          Alternative cloud providers: Alternative Cloud Anbieter
          Server hosting: Server-Hosting
          Basic server security: Server-Sicherheit (Teil 1)
          Advanced server security: Server-Sicherheit (Teil 2)
          Secure domain: Sichere Domäne
          Cloud storage: Cloud-Speicher
          Photo gallery: Fotogalerie
          Contacts, calendars & tasks: Kontakte, Kalender & Aufgaben
          Media streaming: Media-Streaming
          Server backups: Server-Datensicherung
          About: Über uns
          Why digital freedom matters: Datenschutz geht uns alle an
          The project: Das Projekt
          The team: Das Team
          Thanks: Danke!
          Contributing: Beitragen
          Roadmap: Roadmap
          Disclaimer: Haftungsausschluss


################
#  EXTENSIONS  #
################
# Allow icon integration in `svg` format
# Enable anchor links, limit table of content depth to 2

markdown_extensions:
  - admonition
  - abbr
  - attr_list
  - def_list
  - footnotes
  - meta
  - md_in_html
  - pymdownx.caret
  - pymdownx.mark
  - pymdownx.tilde
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.highlight
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.smartsymbols
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.snippets:
      check_paths: true
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tasklist:
      custom_checkbox: true
  - toc:
      permalink: true
      toc_depth: 2
